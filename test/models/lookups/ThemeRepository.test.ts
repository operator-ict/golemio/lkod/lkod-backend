import { expect } from "chai";
import "mocha";
import { postgresConnector } from "../../../src/core/database";
import { ThemeRepository } from "../../../src/models/lookups/ThemeRepository";

describe("ThemeRepository", () => {
    const loggerMock = {
        log: () => {},
        error: () => {},
        info: () => {},
        warn: () => {},
    } as any;
    before(async () => {
        await postgresConnector.connect();
    });

    it("has table name and validator", async () => {
        const repository = new ThemeRepository(postgresConnector, loggerMock);
        expect(repository["tableName"]).to.be.not.undefined;
        expect(repository["validator"]).to.be.not.undefined;
    });

    it("should save and load test row", async () => {
        const repository = new ThemeRepository(postgresConnector, loggerMock);
        await repository.saveAll([
            {
                iri: "test",
                id: "test",
                label: "test",
            },
        ]);
        const result = (await repository.getAll()).filter((el) => el.iri === "test");
        expect(result.length).to.be.equal(1);
    });
});
