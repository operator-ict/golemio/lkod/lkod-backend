import { expect } from "chai";
import * as nodemailer from "nodemailer";
import "mocha";
import * as sinon from "sinon";
import { config } from "../../../src/core/config";
import { log } from "../../../src/core/helpers";
import { Mailer, MailerTansportType } from "../../../src/core/mailer/Mailer";

describe("Mailer", () => {
    let sandbox: sinon.SinonSandbox;
    let mailer: Mailer;
    let logVerboseSpy: sinon.SinonSpy;
    let logDebugSpy: sinon.SinonSpy;

    describe("Dummy (default) transport", () => {
        before(() => {
            mailer = new Mailer();
        });

        beforeEach(() => {
            sandbox = sinon.createSandbox();
            logVerboseSpy = sandbox.spy(log, "verbose");
            logDebugSpy = sandbox.spy(log, "debug");
        });

        afterEach(() => {
            sandbox.restore();
        });

        it("should sends email with password reset link", async () => {
            try {
                await mailer.sendPasswordReset("test@test.com", "token123456");
                sandbox.assert.calledOnce(logVerboseSpy);
                sandbox.assert.calledWith(logDebugSpy.firstCall, { from: "lkod@golemio.cz", to: ["test@test.com"] });
                expect(logDebugSpy.secondCall.firstArg).matches(/token123456/);
            } catch (err) {
                expect(err).to.be.null;
            }
        });
    });

    describe("SMTP transport", () => {
        let sendMailStub: sinon.SinonStub;

        before(() => {
            sandbox.stub(config, "mailerType").value(MailerTansportType.SMTP);
            sandbox.stub(config, "smtp").value({
                pass: "pwd",
                user: "test@test.cz",
                host: "mail.test.cz",
                port: 465,
                secure: true,
                rejectUnauthorized: true,
            });
        });

        after(() => {
            sandbox.stub(config, "mailerType").value(MailerTansportType.DUMMY);
        });

        beforeEach(() => {
            sandbox = sinon.createSandbox();
            logVerboseSpy = sandbox.spy(log, "verbose");
            sendMailStub = sinon.stub().resolves({ messageId: "123456" });
            sandbox.stub(nodemailer, "createTransport").returns({
                sendMail: sendMailStub,
            } as any);
            mailer = new Mailer();
        });

        afterEach(() => {
            sandbox.restore();
        });

        it("should sends email with password reset link", async () => {
            try {
                await mailer.sendPasswordReset("test@test.com", "token123456");
                sandbox.assert.calledOnce(sendMailStub);
                sandbox.assert.calledOnceWithExactly(logVerboseSpy, "[Mailer] Message sent: 123456");
            } catch (err) {
                expect(err).to.be.null;
            }
        });
    });
});
