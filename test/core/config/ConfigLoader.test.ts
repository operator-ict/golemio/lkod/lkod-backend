import * as chai from "chai";
import { expect } from "chai";
import * as chaiAsPromised from "chai-as-promised";
import "mocha";
import { config } from "../../../src/core/config";

chai.use(chaiAsPromised);

describe("ConfigLoader", () => {
    it("should properly load all configurations", () => {
        // env variables
        expect(config.database.host).is.not.null;
        expect(config.database.port).is.a("number");
    });
});
