import { expect } from "chai";
import "mocha";
import * as sinon from "sinon";
import CachedLookups, { LookupFilter } from "../../../../src/core/helpers/LookupQuery/CachedLookups";
import { init } from "../../../resources/00_init.test";

describe("CachedLookups", () => {
    let sandbox: sinon.SinonSandbox;
    before(async () => {
        await init();
    });
    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should get avaiable keywords", async () => {
        const result = await CachedLookups.getInstance().getLookup(LookupFilter.keywords);
        expect(result).to.deep.equal([
            { label: "Silniční doprava", count: 1 },
            { label: "Doprava/Transportation", count: 1 },
        ]);
    });
});
