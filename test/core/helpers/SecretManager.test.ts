import { expect } from "chai";
import "mocha";
import { secretManager } from "../../../src/core/helpers";

describe("secretManager", () => {
    it("should has verifyAndDecodeToken method", async () => {
        expect(secretManager.hashSecret).not.to.be.undefined;
    });

    it("should has generateToken method", async () => {
        expect(secretManager.checkSecret).not.to.be.undefined;
    });

    it("should hash plain secret and return it as string", async () => {
        const hashed = await secretManager.hashSecret("test");
        expect(hashed).to.be.string;
    });

    it("should hash plain secret and validate it", async () => {
        const hashed = await secretManager.hashSecret("test");
        expect(await secretManager.checkSecret("test", hashed)).to.be.true;
    });

    it("should throw if plain secret is not valid", async () => {
        expect(await secretManager.checkSecret("test", "hashed")).to.be.false;
    });
});
