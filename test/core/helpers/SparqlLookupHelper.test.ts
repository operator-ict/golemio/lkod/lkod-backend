import * as sinon from "sinon";
import { expect } from "chai";
import "mocha";
import { SparqlLookupRepository } from "../../../src/core/helpers/LookupQuery/SparqlLookupRepository";

describe("SparqlLookupHelper", () => {
    let sandbox: sinon.SinonSandbox;
    let instance: SparqlLookupRepository;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        instance = new SparqlLookupRepository();
        sandbox.stub(instance["repository"], "query").returns(Promise.resolve([]));
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should has getFormats method", async () => {
        expect(await instance.getFormats()).deep.equal([]);
    });

    it("should has getKeywords method", async () => {
        expect(await instance.getKeywords()).deep.equal([]);
    });

    it("should has getPublishers method", async () => {
        expect(await instance.getPublishers()).deep.equal([]);
    });

    it("should has getThemes method", async () => {
        expect(await instance.getThemes()).deep.equal([]);
    });
});
