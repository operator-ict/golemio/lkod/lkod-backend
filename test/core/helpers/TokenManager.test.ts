import { CustomError } from "@golemio/core/dist/shared/golemio-errors";
import { expect } from "chai";
import "mocha";
import { redisConnector } from "../../../src/core/database";
import { tokenManager } from "../../../src/core/helpers";

describe("tokenManager", () => {
    before(async () => {
        await redisConnector.connect();
    });

    it("should has generateToken method", async () => {
        expect(tokenManager.generateToken).not.to.be.undefined;
    });

    it("should has verifyAndDecodeToken method", async () => {
        expect(tokenManager.verifyAndDecodeToken).not.to.be.undefined;
    });

    it("should generate token and return it as string", async () => {
        const token = await tokenManager.generateToken({ test: "test" });
        expect(token).to.be.string;
    });

    it("should generate token and verify it", async () => {
        const token = await tokenManager.generateToken({ test: "test" });
        try {
            const decoded = await tokenManager.verifyAndDecodeToken(token);
            expect(decoded).to.have.property("test", "test");
        } catch (err) {
            expect(err).to.be.null;
        }
    });

    it("should throw if token is not valid", async () => {
        try {
            await tokenManager.verifyAndDecodeToken("testtest");
        } catch (err) {
            expect(err).to.be.instanceOf(CustomError);
        }
    });

    it("should set token to blacklist", async () => {
        const token = await tokenManager.generateToken({ test: "test" }, 300);
        try {
            const decoded = (await tokenManager.verifyAndDecodeToken(token)) as any;
            await tokenManager.setTokenToBlacklist(decoded.jti, decoded.exp);
            expect(await tokenManager.isTokenBlacklisted(token));
        } catch (err) {
            expect(err).to.be.null;
        }
    });
});
