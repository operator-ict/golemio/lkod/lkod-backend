import { expect } from "chai";
import * as fs from "fs";
import "mocha";
import * as path from "path";
import * as sinon from "sinon";
import { ThemeDatasource } from "../../../src/core/datasources/ThemeDatasource";

describe("ThemeDatasource", () => {
    let datasource: any;
    let sandbox: sinon.SinonSandbox;
    let testQueryData: any;
    let expectedTransformedData: any;

    beforeEach(() => {
        datasource = new ThemeDatasource();
        sandbox = sinon.createSandbox();
        testQueryData = JSON.parse(fs.readFileSync(path.join(__dirname, "data/themeDatasource-input.json"), "utf8"));
        expectedTransformedData = JSON.parse(fs.readFileSync(path.join(__dirname, "data/themeDatasource-expected.json"), "utf8"));
        sandbox.stub(datasource, "query").returns(testQueryData);
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("has command and validation scheme", async () => {
        expect(datasource["command"]).to.be.not.undefined;
        expect(datasource["getJsonSchema"]).to.be.not.undefined;
    });

    it("should query, validate and return data", async () => {
        const result = await datasource.getThemes();

        expect(result).to.deep.equal(expectedTransformedData);
    });
});
