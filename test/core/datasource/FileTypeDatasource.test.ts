import { expect } from "chai";
import * as fs from "fs";
import "mocha";
import * as path from "path";
import * as sinon from "sinon";
import { SinonSandbox } from "sinon";
import { FileTypeDatasource } from "../../../src/core/datasources/FileTypeDatasource";

describe("FileTypeDatasource", () => {
    let datasource: any;
    let sandbox: SinonSandbox;
    let testQueryData: any;
    let expectedTransformedData: any;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        datasource = new FileTypeDatasource();
        testQueryData = JSON.parse(fs.readFileSync(path.join(__dirname, "data/fileTypeDatasource-input.json"), "utf8"));
        expectedTransformedData = JSON.parse(
            fs.readFileSync(path.join(__dirname, "data/fileTypeDatasource-expected.json"), "utf8")
        );
        sandbox.stub(datasource, "query").returns(testQueryData);
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("has command and validation scheme", async () => {
        expect(datasource["command"]).to.be.not.undefined;
        expect(datasource["getJsonSchema"]).to.be.not.undefined;
    });

    it("should query, validate and return data", async () => {
        const result = await datasource.getFileTypes();

        expect(result).to.deep.equal(expectedTransformedData);
    });
});
