import { expect } from "chai";
import * as fs from "fs";
import "mocha";
import * as path from "path";
import * as sinon from "sinon";
import { FrequencyDatasource } from "../../../src/core/datasources/FrequencyDatasource";

describe("FrequencyTypeDatasource", () => {
    let datasource: any;
    let sandbox: sinon.SinonSandbox;
    let testQueryData: any;
    let expectedTransformedData: any;

    beforeEach(() => {
        datasource = new FrequencyDatasource();
        sandbox = sinon.createSandbox();
        testQueryData = JSON.parse(fs.readFileSync(path.join(__dirname, "data/frequencyDatasource-input.json"), "utf8"));
        expectedTransformedData = JSON.parse(
            fs.readFileSync(path.join(__dirname, "data/frequencyDatasource-expected.json"), "utf8")
        );
        sandbox.stub(datasource, "query").returns(testQueryData);
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("has command and validation scheme", async () => {
        expect(datasource["command"]).to.be.not.undefined;
        expect(datasource["getJsonSchema"]).to.be.not.undefined;
    });

    it("should query, validate and return data", async () => {
        const result = await datasource.getFrequencies();

        expect(result).to.deep.equal(expectedTransformedData);
    });
});
