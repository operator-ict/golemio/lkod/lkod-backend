import { expect } from "chai";
import "mocha";
import { log } from "../../../src/core/helpers";
import { OrganizationRepository } from "../../../src/models/OrganizationRepository";
import RefreshArcgisDatasetsTask from "../../../src/registers/tasks/RefreshArcgisDatasetsTask";

describe("RefreshArcgisDatasetsTask", () => {
    let task: RefreshArcgisDatasetsTask;

    beforeEach(async () => {
        task = new RefreshArcgisDatasetsTask(new OrganizationRepository(), log);
    });

    it("has name, cron and process function", async () => {
        expect(task["name"]).to.be.not.undefined;
        expect(task["cronTime"]).to.be.not.undefined;
        expect(task.process).to.be.not.undefined;
    });
});
