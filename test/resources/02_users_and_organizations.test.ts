import { CustomError, HTTPErrorHandler, ICustomErrorObject } from "@golemio/core/dist/shared/golemio-errors";
import * as chai from "chai";
import * as express from "express";
import { NextFunction, Request, Response } from "express";
import "mocha";
import * as sinon from "sinon";
import * as supertest from "supertest";
import { config } from "../../src/core/config";
import { log } from "../../src/core/helpers";
import { AuthRouter } from "../../src/resources/auth";
import { OrganizationsRouter } from "../../src/resources/organizations";
import { init } from "./00_init.test";
import { OrganizationRepository } from "../../src/models/OrganizationRepository";
import { UsersRouter } from "../../src/resources/users";

describe("Users and Organizations", () => {
    let sandbox: sinon.SinonSandbox;
    // Create clean express instance
    const app = express();
    let authRouter: AuthRouter;
    let usersRouter: UsersRouter;
    let organizationsRouter: any; // :any for access to private properties
    let accessToken: string;
    let adminAccessToken: string;
    let superadminAccessToken: string;
    let orgRepo: OrganizationRepository | null;

    before(async () => {
        // mainly database initialization
        await init();

        authRouter = new AuthRouter();
        usersRouter = new UsersRouter();
        organizationsRouter = new OrganizationsRouter();

        app.use(express.json());
        app.use(express.urlencoded({ extended: true }));
        // Mount the tested router to the express instance
        app.use("/auth", authRouter.router);
        app.use("/users", usersRouter.router);
        app.use("/organizations", organizationsRouter.router);
        app.use((err: any, req: Request, res: Response, next: NextFunction) => {
            // hadle body parser errors
            if (err instanceof SyntaxError) {
                err = new CustomError(err.message, true, "App", (err as any)?.status || 500, err.message);
            }

            const warnCodes = [400, 401, 403, 404, 422];
            const errObject: ICustomErrorObject = HTTPErrorHandler.handle(
                err,
                log,
                warnCodes.includes(err.code) ? "warn" : "error"
            );
            log.silly("Error caught by the router error handler.");
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(errObject.error_status || 500).send(errObject);
        });
    });

    before((done) => {
        // get access token
        supertest(app)
            .post("/auth/login")
            .send({
                email: "test@golemio.cz",
                password: "pass",
            })
            .end((err: Error | CustomError, res: supertest.Response) => {
                if (err || res.statusCode !== 200) {
                    done(err);
                }
                accessToken = res.body.accessToken;
                done();
            });
    });
    before((done) => {
        // get access token
        supertest(app)
            .post("/auth/login")
            .send({
                email: "admin@golemio.cz",
                password: "pass",
            })
            .end((err: Error | CustomError, res: supertest.Response) => {
                if (err || res.statusCode !== 200) {
                    done(err);
                }
                adminAccessToken = res.body.accessToken;
                done();
            });
    });

    before((done) => {
        // get access token
        supertest(app)
            .post("/auth/login")
            .send({
                email: "superadmin@golemio.cz",
                password: "pass",
            })
            .end((err: Error | CustomError, res: supertest.Response) => {
                if (err || res.statusCode !== 200) {
                    done(err);
                }
                superadminAccessToken = res.body.accessToken;
                done();
            });
    });

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        orgRepo = new OrganizationRepository();
    });

    afterEach(() => {
        sandbox.restore();
    });

    describe("users", () => {
        it("should let superadmin retrieve all users at GET /users", (done) => {
            supertest(app)
                .get("/users")
                .set("Authorization", `Bearer ${superadminAccessToken}`)
                .expect(200)
                .end((err: Error | CustomError, res: supertest.Response) => {
                    if (err) {
                        done(err);
                    }
                    chai.expect(res.body).to.be.deep.equal([
                        {
                            id: 666,
                            email: "deleteme@golemio.cz",
                            name: "delete me",
                            role: "user",
                            hasDefaultPassword: false,
                        },
                        {
                            id: 4,
                            email: "new@golemio.cz",
                            name: "new user",
                            role: "user",
                            hasDefaultPassword: true,
                        },
                        {
                            id: 3,
                            email: "admin@golemio.cz",
                            name: "test admin",
                            role: "admin",
                            hasDefaultPassword: false,
                        },
                        {
                            id: 5,
                            email: "superadmin@golemio.cz",
                            name: "test superadmin",
                            role: "superadmin",
                            hasDefaultPassword: false,
                        },
                        {
                            id: 6,
                            email: "superadmin2@golemio.cz",
                            name: "test superadmin 2",
                            role: "superadmin",
                            hasDefaultPassword: false,
                        },
                        {
                            id: 2,
                            email: "test@golemio.cz",
                            name: "test user",
                            role: "user",
                            hasDefaultPassword: false,
                        },
                    ]);
                    chai.expect(err).to.be.null;
                    done();
                });
        });

        it("should return 403 for admin at GET /users", (done) => {
            supertest(app).get("/users").set("Authorization", `Bearer ${adminAccessToken}`).expect(403, done);
        });

        it("should return 403 for user at GET /users", (done) => {
            supertest(app).get("/users").set("Authorization", `Bearer ${accessToken}`).expect(403, done);
        });

        it("should return 401 for anonymous at GET /users", (done) => {
            supertest(app).get("/users").expect(401, done);
        });

        it("should let superadmin retrieve themselves at GET /users/me", (done) => {
            supertest(app)
                .get("/users/me")
                .set("Authorization", `Bearer ${superadminAccessToken}`)
                .expect(200)
                .end((err: Error | CustomError, res: supertest.Response) => {
                    if (err) {
                        done(err);
                    }
                    chai.expect(res.body).to.be.deep.equal({
                        id: 5,
                        email: "superadmin@golemio.cz",
                        name: "test superadmin",
                        role: "superadmin",
                        hasDefaultPassword: false,
                    });
                    chai.expect(err).to.be.null;
                    done();
                });
        });

        it("should return 403 for admin at GET /users/me", (done) => {
            supertest(app).get("/users/me").set("Authorization", `Bearer ${adminAccessToken}`).expect(403, done);
        });

        it("should return 403 for user at GET /users/me", (done) => {
            supertest(app).get("/users/me").set("Authorization", `Bearer ${accessToken}`).expect(403, done);
        });

        it("should return 401 for anonymous at GET /users/me", (done) => {
            supertest(app).get("/users/me").expect(401, done);
        });

        const userDetails = {
            email: "xyz@golemio.cz",
            password: "abc def",
            name: "Xyz User",
            role: "admin",
        };

        it("should not let user create user at POST /users", (done) => {
            supertest(app)
                .post("/users")
                .set("Authorization", `Bearer ${accessToken}`)
                .send(userDetails)
                .type("json")
                .expect(403, done);
        });

        it("should not let admin create user at POST /users", (done) => {
            supertest(app)
                .post("/users")
                .set("Authorization", `Bearer ${adminAccessToken}`)
                .send(userDetails)
                .type("json")
                .expect(403, done);
        });

        let newUserId: number;

        it("should let superadmin create user at POST /users", (done) => {
            const { ["password"]: ignored, ...expected } = userDetails;
            supertest(app)
                .post("/users")
                .set("Authorization", `Bearer ${superadminAccessToken}`)
                .send(userDetails)
                .type("json")
                .expect((res) => {
                    chai.expect(res.body.id).to.be.a("number");
                    chai.expect(res.body).to.contain(expected);
                    newUserId = res.body.id;
                })
                .expect(201, done);
        });

        it("should not create duplicate email user at POST /users", (done) => {
            supertest(app)
                .post("/users")
                .set("Authorization", `Bearer ${superadminAccessToken}`)
                .send(userDetails)
                .type("json")
                .expect(409, done);
        });

        const userDiff = {
            email: "changed@email.cz",
            name: "Changed Name",
            role: "superadmin",
        };

        it("should let superadmin update user at PATCH /users/{id}", (done) => {
            supertest(app)
                .patch(`/users/${newUserId}`)
                .set("Authorization", `Bearer ${superadminAccessToken}`)
                .send(userDiff)
                .type("json")
                .expect((res) => {
                    chai.expect(res.body.id).to.be.a("number");
                    chai.expect(res.body).to.deep.equals({
                        id: newUserId,
                        email: "changed@email.cz",
                        name: "Changed Name",
                        role: "superadmin",
                        hasDefaultPassword: true,
                    });
                    newUserId = res.body.id;
                })
                .expect(200, done);
        });

        it("should not let superadmin update themselves at PATCH /users/{id}", (done) => {
            supertest(app)
                .patch(`/users/5`)
                .set("Authorization", `Bearer ${superadminAccessToken}`)
                .send(userDiff)
                .type("json")
                .expect(400, done);
        });

        it("should not let admin update user at PATCH /users/{id}", (done) => {
            supertest(app)
                .patch(`/users/${newUserId}`)
                .set("Authorization", `Bearer ${adminAccessToken}`)
                .send(userDiff)
                .type("json")
                .expect(403, done);
        });

        it("should not let user update user at PATCH /users/{id}", (done) => {
            supertest(app)
                .patch(`/users/${newUserId}`)
                .set("Authorization", `Bearer ${accessToken}`)
                .send(userDiff)
                .type("json")
                .expect(403, done);
        });

        it("should not let user update self at PATCH /users/{id}", (done) => {
            supertest(app)
                .patch(`/users/2`)
                .set("Authorization", `Bearer ${accessToken}`)
                .send(userDiff)
                .type("json")
                .expect(403, done);
        });

        it("should not allow email collision at PATCH /users/{id}", (done) => {
            supertest(app)
                .patch(`/users/${newUserId}`)
                .set("Authorization", `Bearer ${superadminAccessToken}`)
                .send({ ...userDiff, email: "admin@golemio.cz" })
                .type("json")
                .expect(409, done);
        });

        it("should return 404 on bad user ID at DELETE /users/{id}", (done) => {
            supertest(app)
                .delete(`/users/999999999999999999`)
                .set("Authorization", `Bearer ${superadminAccessToken}`)
                .expect(404, done);
        });

        it("should not let anonymous delete user at DELETE /users/{id}", (done) => {
            supertest(app).delete(`/users/666`).expect(401, done);
        });

        it("should not let user delete user at DELETE /users/{id}", (done) => {
            supertest(app).delete(`/users/666`).set("Authorization", `Bearer ${accessToken}`).expect(403, done);
        });

        it("should not let admin delete user at DELETE /users/{id}", (done) => {
            supertest(app).delete(`/users/666`).set("Authorization", `Bearer ${adminAccessToken}`).expect(403, done);
        });

        it("should let superadmin delete user at DELETE /users/{id}", (done) => {
            supertest(app).delete(`/users/666`).set("Authorization", `Bearer ${superadminAccessToken}`).expect(204, done);
        });

        it("should not let superadmin delete themselves at DELETE /users/{id}", (done) => {
            supertest(app).delete(`/users/5`).set("Authorization", `Bearer ${superadminAccessToken}`).expect(400, done);
        });
    });

    describe("organizations", () => {
        let superadminAccessToken2: string;

        before((done) => {
            // get access token
            supertest(app)
                .post("/auth/login")
                .send({
                    email: "superadmin2@golemio.cz",
                    password: "pass",
                })
                .end((err: Error | CustomError, res: supertest.Response) => {
                    if (err || res.statusCode !== 200) {
                        done(err);
                    }
                    superadminAccessToken2 = res.body.accessToken;
                    done();
                });
        });

        it("should returns own organizations for user at GET /organizations", (done) => {
            const url = "/organizations";
            supertest(app)
                .get(url)
                .set("Authorization", `Bearer ${accessToken}`)
                .expect(200)
                .end((err: Error | CustomError, res: supertest.Response) => {
                    if (err) {
                        done(err);
                    }
                    chai.expect(res.body).to.be.deep.equal([
                        {
                            id: 1,
                            identificationNumber: "02795281",
                            name: "test org",
                            slug: "test-org",
                            logo: "https://placehold.co/100x100",
                            hasArcGisFeed: false,
                            description: "Lorem ipsum dolor sit amet.",
                        },
                        {
                            hasArcGisFeed: true,
                            id: 3,
                            name: "arcgis test org",
                            identificationNumber: "02795282",
                            slug: "arcgis-test-org",
                            logo: null,
                            description: null,
                        },
                    ]);
                    chai.expect(err).to.be.null;
                    done();
                });
        });

        it("should returns own organizations for admin at GET /organizations", (done) => {
            const url = "/organizations";
            supertest(app)
                .get(url)
                .set("Authorization", `Bearer ${adminAccessToken}`)
                .expect(200)
                .end((err: Error | CustomError, res: supertest.Response) => {
                    if (err) {
                        done(err);
                    }
                    chai.expect(res.body).to.be.deep.equal([
                        {
                            hasArcGisFeed: true,
                            id: 3,
                            name: "arcgis test org",
                            identificationNumber: "02795282",
                            slug: "arcgis-test-org",
                            logo: null,
                            description: null,
                        },
                    ]);
                    chai.expect(err).to.be.null;
                    done();
                });
        });

        it("should returns all organizations for superadmin at GET /organizations", (done) => {
            const url = "/organizations";
            supertest(app)
                .get(url)
                .set("Authorization", `Bearer ${superadminAccessToken}`)
                .expect(200)
                .end((err: Error | CustomError, res: supertest.Response) => {
                    if (err) {
                        done(err);
                    }
                    chai.expect(res.body).to.be.deep.equal([
                        {
                            id: 1,
                            identificationNumber: "02795281",
                            name: "test org",
                            slug: "test-org",
                            logo: "https://placehold.co/100x100",
                            arcGisFeed: null,
                            hasArcGisFeed: false,
                            description: "Lorem ipsum dolor sit amet.",
                        },
                        {
                            id: 2,
                            identificationNumber: "02795281",
                            name: "second test org",
                            slug: "second-test-org",
                            logo: null,
                            hasArcGisFeed: false,
                            arcGisFeed: null,
                            description: null,
                        },
                        {
                            id: 3,
                            name: "arcgis test org",
                            identificationNumber: "02795282",
                            slug: "arcgis-test-org",
                            logo: null,
                            hasArcGisFeed: true,
                            arcGisFeed: "https://test-data-brno-cz-mestobrno.hub.arcgis.com/api/feed/dcat-us/1.1.json",
                            description: null,
                        },
                    ]);
                    chai.expect(err).to.be.null;
                    done();
                });
        });

        it("should return Organizations as RDF", async () => {
            const res = await orgRepo?.getOrganizationsAsJsonLD();
            chai.expect(res).to.be.deep.equal({
                "@graph": [
                    {
                        "@id": `${config.linked_data.poskytovatel_url_prefix}test-org`,
                        "@type": "schema:Organization",
                        alternateName: "test-org",
                        sameAs: "https://linked.opendata.cz/zdroj/ekonomický-subjekt/02795281",
                        legalName: "test org",
                        name: { "@language": "cs", "@value": "test org" },
                        description: {
                            "@language": "cs",
                            "@value": "Lorem ipsum dolor sit amet.",
                        },
                        logo: "https://placehold.co/100x100",
                    },
                    {
                        "@id": `${config.linked_data.poskytovatel_url_prefix}second-test-org`,
                        "@type": "schema:Organization",
                        alternateName: "second-test-org",
                        sameAs: "https://linked.opendata.cz/zdroj/ekonomický-subjekt/02795281",
                        legalName: "second test org",
                        name: { "@language": "cs", "@value": "second test org" },
                    },
                    {
                        "@id": `${config.linked_data.poskytovatel_url_prefix}arcgis-test-org`,
                        "@type": "schema:Organization",
                        alternateName: "arcgis-test-org",
                        legalName: "arcgis test org",
                        name: {
                            "@language": "cs",
                            "@value": "arcgis test org",
                        },
                        sameAs: "https://linked.opendata.cz/zdroj/ekonomický-subjekt/02795282",
                    },
                ],
                "@context": {
                    alternateName: "http://schema.org/alternateName",
                    description: "http://schema.org/description",
                    logo: { "@id": "http://schema.org/logo", "@type": "@id" },
                    name: "http://xmlns.com/foaf/0.1/name",
                    legalName: "http://schema.org/legalName",
                    sameAs: { "@id": "http://www.w3.org/2002/07/owl#sameAs", "@type": "@id" },
                    schema: "http://schema.org/",
                    rdf: "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
                    owl: "http://www.w3.org/2002/07/owl#",
                    rdfs: "http://www.w3.org/2000/01/rdf-schema#",
                    foaf: "http://xmlns.com/foaf/0.1/",
                },
            });
        });

        const organizationDetails = {
            name: "Other Organization",
            identificationNumber: "02795283",
            slug: "other-test-org",
            logo: "https://placehold.co/100x100",
            description: "Lorem ipsum dolor sit amet other.",
            arcGisFeed: "https://valid.url.com/",
        };

        let testOrganizationId: string;

        it("should let superadmin create organization at POST /organizations", (done) => {
            supertest(app)
                .post("/organizations")
                .set("Authorization", `Bearer ${superadminAccessToken}`)
                .send(organizationDetails)
                .type("json")
                .expect((res) => {
                    chai.expect(res.body.id).to.be.a("number");
                    chai.expect(res.body).to.contain(organizationDetails);
                    testOrganizationId = res.body.id;
                })
                .expect(201, done);
        });

        it("should not let admin create organization at POST /organizations", (done) => {
            supertest(app)
                .post("/organizations")
                .set("Authorization", `Bearer ${adminAccessToken}`)
                .send(organizationDetails)
                .type("json")
                .expect(403, done);
        });

        it("should not let user create organization at POST /organizations", (done) => {
            supertest(app)
                .post("/organizations")
                .set("Authorization", `Bearer ${accessToken}`)
                .send(organizationDetails)
                .type("json")
                .expect(403, done);
        });

        it("should not let anonymous create organization at POST /organizations", (done) => {
            supertest(app).post("/organizations").send(organizationDetails).type("json").expect(401, done);
        });

        it("should not create duplicate slug organization at POST /organizations", (done) => {
            const dupOrgDetails = { ...organizationDetails, slug: "dup-org-slug" };
            supertest(app)
                .post("/organizations")
                .set("Authorization", `Bearer ${superadminAccessToken}`)
                .send(dupOrgDetails)
                .type("json")
                .expect(201)
                .end((err) => {
                    if (err) {
                        done(err);
                    }
                    supertest(app)
                        .post("/organizations")
                        .set("Authorization", `Bearer ${superadminAccessToken}`)
                        .send(dupOrgDetails)
                        .type("json")
                        .expect(409, done);
                });
        });

        const organizationDiff = {
            name: "Modified Name",
            arcGisFeed: "https://modified.url.com/",
        };

        it("should let superadmin update self-created organization at PATCH /organization/{id}", (done) => {
            supertest(app)
                .patch(`/organizations/${testOrganizationId}`)
                .set("Authorization", `Bearer ${superadminAccessToken}`)
                .send(organizationDiff)
                .type("json")
                .expect(200, done);
        });

        it("should let superadmin update non-self-created organization at PATCH /organization/{id}", (done) => {
            supertest(app)
                .patch(`/organizations/${testOrganizationId}`)
                .set("Authorization", `Bearer ${superadminAccessToken2}`)
                .send(organizationDiff)
                .type("json")
                .expect(200, done);
        });

        it("should let admin update own organization at PATCH /organization/{id}", (done) => {
            supertest(app)
                .patch(`/organizations/3`) // Assigned in 00_init.test.ts
                .set("Authorization", `Bearer ${adminAccessToken}`)
                .send(organizationDiff)
                .type("json")
                .expect(200, done);
        });

        it("should let user update own organization at PATCH /organization/{id}", (done) => {
            supertest(app)
                .patch(`/organizations/1`) // Assigned in 00_init.test.ts
                .set("Authorization", `Bearer ${accessToken}`)
                .send(organizationDiff)
                .type("json")
                .expect(200, done);
        });

        it("should not let admin update foreign organization at PATCH /organization/{id}", (done) => {
            supertest(app)
                .patch(`/organizations/${testOrganizationId}`)
                .set("Authorization", `Bearer ${adminAccessToken}`)
                .send(organizationDiff)
                .type("json")
                .expect(403, done);
        });

        it("should not let user update foreign organization at PATCH /organization/{id}", (done) => {
            supertest(app)
                .patch(`/organizations/${testOrganizationId}`)
                .set("Authorization", `Bearer ${accessToken}`)
                .send(organizationDiff)
                .type("json")
                .expect(403, done);
        });

        it("should not let anonymous update organization at PATCH /organization/{id}", (done) => {
            supertest(app).patch(`/organizations/${testOrganizationId}`).send(organizationDiff).type("json").expect(401, done);
        });

        it("should update exactly the provided fields at PATCH /organization/{id}", (done) => {
            const organization = { ...organizationDetails, slug: "slug-for-update-data-check" };
            supertest(app)
                .post("/organizations")
                .set("Authorization", `Bearer ${superadminAccessToken}`)
                .send(organization)
                .type("json")
                .expect(201)
                .end((err, createRes) => {
                    if (err) {
                        done(err);
                    }
                    supertest(app)
                        .patch(`/organizations/${createRes.body.id}`)
                        .set("Authorization", `Bearer ${superadminAccessToken}`)
                        .send(organizationDiff)
                        .type("json")
                        .expect((res) => {
                            chai.expect(res.body.id).to.eq(createRes.body.id);
                            chai.expect(res.body).to.deep.equal({
                                ...organization,
                                ...organizationDiff,
                                id: createRes.body.id,
                            });
                        })
                        .expect(200, done);
                });
        });

        it("should not allow slug collision at PATCH /organization/{id}", (done) => {
            supertest(app)
                .patch(`/organizations/${testOrganizationId}`)
                .set("Authorization", `Bearer ${superadminAccessToken}`)
                .send({ slug: "test-org" }) // Assigned in 00_init.test.ts
                .type("json")
                .expect(409, done);
        });

        it("should not let anonymous delete organization at DELETE /organizations/{id}", (done) => {
            supertest(app).delete(`/organizations/${testOrganizationId}`).expect(401, done);
        });

        it("should not let user delete organization at DELETE /organizations/{id}", (done) => {
            supertest(app)
                .delete(`/organizations/${testOrganizationId}`)
                .set("Authorization", `Bearer ${accessToken}`)
                .expect(403, done);
        });

        it("should not let admin delete organization at DELETE /organizations/{id}", (done) => {
            supertest(app)
                .delete(`/organizations/${testOrganizationId}`)
                .set("Authorization", `Bearer ${adminAccessToken}`)
                .expect(403, done);
        });

        it("should let superadmin delete organization at DELETE /organizations/{id}", (done) => {
            supertest(app)
                .post("/organizations")
                .set("Authorization", `Bearer ${superadminAccessToken}`)
                .send({ ...organizationDetails, slug: "delete-me" })
                .type("json")
                .expect(201)
                .end((err, res) => {
                    if (err) {
                        done(err);
                        return;
                    }
                    supertest(app)
                        .delete(`/organizations/${res.body.id}`)
                        .set("Authorization", `Bearer ${superadminAccessToken}`)
                        .expect(204, done);
                });
        });

        describe("members", () => {
            it("should let superadmin add user to organization at POST /organizations/{id}/members/{id}", (done) => {
                supertest(app)
                    .post(`/organizations/${testOrganizationId}/members/1`)
                    .set("Authorization", `Bearer ${superadminAccessToken}`)
                    .expect(204, done);
            });

            it("should not let admin add user to organization at POST /organizations/{id}/members/{id}", (done) => {
                supertest(app)
                    .post(`/organizations/${testOrganizationId}/members/1`)
                    .set("Authorization", `Bearer ${adminAccessToken}`)
                    .expect(403, done);
            });

            it("should not let user add user to organization at POST /organizations/{id}/members/{id}", (done) => {
                supertest(app)
                    .post(`/organizations/${testOrganizationId}/members/1`)
                    .set("Authorization", `Bearer ${accessToken}`)
                    .expect(403, done);
            });

            it("should not let anonymous add user to organization at POST /organizations/{id}/members/{id}", (done) => {
                supertest(app).post(`/organizations/${testOrganizationId}/members/1`).expect(401, done);
            });

            it("should return 404 on bad organization ID at POST /organizations/{id}/members/{id}", (done) => {
                supertest(app)
                    .post(`/organizations/999999999999/members/1`)
                    .set("Authorization", `Bearer ${superadminAccessToken}`)
                    .expect(404, done);
            });

            it("should return 404 on bad user ID at POST /organizations/{id}/members/{id}", (done) => {
                supertest(app)
                    .post(`/organizations/${testOrganizationId}/members/99999999999999`)
                    .set("Authorization", `Bearer ${superadminAccessToken}`)
                    .expect(404, done);
            });

            it("should return 404 on bad user ID at DELETE /organizations/{id}/members/{id}", (done) => {
                supertest(app)
                    .delete(`/organizations/${testOrganizationId}/members/999999999999999999`)
                    .set("Authorization", `Bearer ${superadminAccessToken}`)
                    .expect(404, done);
            });

            it("should return 404 on bad organization ID at POST /organizations/{id}/members/{id}", (done) => {
                supertest(app)
                    .delete(`/organizations/99999999999/members/1`)
                    .set("Authorization", `Bearer ${superadminAccessToken}`)
                    .expect(404, done);
            });

            it("should not let anonymous remove user from organization at DELETE /organizations/{id}/members/{id}", (done) => {
                supertest(app).delete(`/organizations/${testOrganizationId}/members/1`).expect(401, done);
            });

            it("should not let user remove user from organization at DELETE /organizations/{id}/members/{id}", (done) => {
                supertest(app)
                    .delete(`/organizations/${testOrganizationId}/members/1`)
                    .set("Authorization", `Bearer ${accessToken}`)
                    .expect(403, done);
            });

            it("should not let admin remove user from organization at DELETE /organizations/{id}/members/{id}", (done) => {
                supertest(app)
                    .delete(`/organizations/${testOrganizationId}/members/1`)
                    .set("Authorization", `Bearer ${adminAccessToken}`)
                    .expect(403, done);
            });

            it("should let superadmin remove user from organization at DELETE /organizations/{id}/members/{id}", (done) => {
                supertest(app)
                    .delete(`/organizations/${testOrganizationId}/members/1`)
                    .set("Authorization", `Bearer ${superadminAccessToken}`)
                    .expect(204, done);
            });
        });
    });

    describe.skip("users", () => {
        //
    });
});
