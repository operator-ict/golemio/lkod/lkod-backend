import { CustomError, HTTPErrorHandler, ICustomErrorObject } from "@golemio/core/dist/shared/golemio-errors";
import * as chai from "chai";
import * as express from "express";
import { NextFunction, Request, Response } from "express";
import "mocha";
import * as sinon from "sinon";
import * as supertest from "supertest";
import { config } from "../../src/core/config";
import { postgresConnector } from "../../src/core/database";
import { log } from "../../src/core/helpers";
import { DatasetsRouter } from "../../src/resources/datasets";
import { FormDataRouter } from "../../src/resources/form-data";
import { AuthRouter } from "../../src/resources/auth";
import { SessionsRouter } from "../../src/resources/sessions";
import { init } from "./00_init.test";

describe("Form data", () => {
    let sandbox: sinon.SinonSandbox;
    // Create clean express instance
    const app = express();
    let authRouter: AuthRouter;
    let datasetsRouter: any; // :any for access to private properties
    let formDataRouter: any; // :any for access to private properties
    let sessionsRouter: any; // :any for access to private properties
    let accessToken: string;
    let datasetId: string;
    let newsession: any;

    before(async () => {
        // mainly database initialization
        await init();

        authRouter = new AuthRouter();
        datasetsRouter = new DatasetsRouter();
        formDataRouter = new FormDataRouter();
        sessionsRouter = new SessionsRouter();

        app.use(express.json());
        app.use(express.urlencoded({ extended: true }));
        // Mount the tested router to the express instance
        app.use("/auth", authRouter.router);
        app.use("/datasets", datasetsRouter.router);
        app.use("/form-data", formDataRouter.router);
        app.use("/sessions", sessionsRouter.router);
        app.use(`/form-data`, (err: any, req: Request, res: Response, next: NextFunction) => {
            res.redirect(config.frontend_url + `?e=${encodeURIComponent(JSON.stringify(err.toObject ? err.toObject() : err))}`);
        });
        app.use((err: any, req: Request, res: Response, next: NextFunction) => {
            // hadle body parser errors
            if (err instanceof SyntaxError) {
                err = new CustomError(err.message, true, "App", (err as any)?.status || 500, err.message);
            }

            const warnCodes = [400, 401, 403, 404, 422];
            const errObject: ICustomErrorObject = HTTPErrorHandler.handle(
                err,
                log,
                warnCodes.includes(err.code) ? "warn" : "error"
            );
            log.silly("Error caught by the router error handler.");
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(errObject.error_status || 500).send(errObject);
        });
    });

    before((done) => {
        // get access token
        supertest(app)
            .post("/auth/login")
            .send({
                email: "test@golemio.cz",
                password: "pass",
            })
            .end((err: Error | CustomError, res: supertest.Response) => {
                if (err || res.statusCode !== 200) {
                    done(err);
                }
                accessToken = res.body.accessToken;
                done();
            });
    });

    before((done) => {
        // get dataset id
        supertest(app)
            .post("/datasets")
            .send({
                organizationId: 1,
            })
            .set("Authorization", `Bearer ${accessToken}`)
            .end((err: Error | CustomError, res: supertest.Response) => {
                if (err || res.statusCode !== 201) {
                    done(err);
                }
                datasetId = res.body.id;
                done();
            });
    });

    before((done) => {
        supertest(app)
            .post("/sessions")
            .send({
                datasetId,
            })
            .set("Authorization", `Bearer ${accessToken}`)
            .end((err: Error | CustomError, res: supertest.Response) => {
                if (err || res.statusCode !== 201) {
                    done(err);
                }
                newsession = res.body;
                done();
            });
    });

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should throws error on bad input data at POST /form-data (1)", (done) => {
        const url = "/form-data";
        supertest(app)
            .post(url)
            .send({
                formData: JSON.stringify({ any: "data" }),
            })
            .set("Authorization", `Bearer ${accessToken}`)
            .expect(302)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                chai.expect(res.headers.location).to.equal(
                    `${config.frontend_url}?e=${encodeURIComponent(
                        JSON.stringify({
                            error_message: "Bad Request",
                            error_class_name: "FormDataRouter",
                            error_status: 400,
                            error_info:
                                '[{"msg":"Invalid value","param":"userData","location":"body"},' +
                                '{"msg":"Invalid value","param":"userData","location":"body"}]',
                        })
                    )}`
                );
                done();
            });
    });

    it("should propagate error on bad input data at POST /form-data (2)", (done) => {
        const url = "/form-data";
        supertest(app)
            .post(url)
            .send({
                formData: JSON.stringify({ any: "data" }),
                userData: JSON.stringify({
                    accessToken,
                    datasetId,
                    sessionId: "b3bef705-e1ac-4ecd-aed2-fafa0ca6e2f6",
                }),
            })
            .set("Authorization", `Bearer ${accessToken}`)
            .expect(302)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                chai.expect(res.headers.location).to.equal(
                    `${config.frontend_url}?e=${encodeURIComponent(
                        JSON.stringify({
                            error_message: "Error while userData validation",
                            error_class_name: "FormDataController",
                            error_status: 400,
                            error_info: "Session not exists or is expired",
                        })
                    )}`
                );
                done();
            });
    });

    it("should propagate error on bad input data at POST /form-data (3)", (done) => {
        const url = "/form-data";
        supertest(app)
            .post(url)
            .send({
                formData: JSON.stringify({ any: "data" }),
                userData: JSON.stringify({
                    datasetId,
                    sessionId: newsession.id,
                }),
            })
            .set("Authorization", `Bearer ${accessToken}`)
            .expect(302)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                chai.expect(res.headers.location).to.equal(
                    `${config.frontend_url}?e=${encodeURIComponent(
                        JSON.stringify({
                            error_message: "Error while JWT token validation",
                            error_class_name: "TokenManager",
                            error_status: 401,
                            error_info: "jwt must be provided",
                        })
                    )}`
                );
                done();
            });
    });

    it("should properly receives form data at POST /form-data", (done) => {
        const url = "/form-data";
        supertest(app)
            .post(url)
            .send({
                formData: JSON.stringify({
                    any: "data",
                    distribuce: [
                        {
                            přístupové_url: "https://testtest.cz/test",
                        },
                        {
                            přístupové_url: "https://testtest.cz/test2",
                            formát: "http://publications.europa.eu/resource/authority/file-type/CSV",
                        },
                        {
                            přístupové_url: "https://testtest.cz/test2",
                            formát: "http://publications.europa.eu/resource/authority/file-type/CSV",
                        },
                        {
                            přístupové_url: "https://testtest.cz/test3service",
                            přístupová_služba: {
                                typ: "Datová služba",
                                přístupový_bod: "https://testtest.cz/test3service",
                                název: {
                                    cs: "test service",
                                },
                            },
                        },
                        {
                            přístupové_url: "",
                        },
                        {
                            typ: "Distribuce",
                            podmínky_užití: {
                                typ: "Specifikace podmínek užití",
                                autorské_dílo: "https://data.gov.cz/podmínky-užití/obsahuje-více-autorských-děl/",
                                databáze_jako_autorské_dílo:
                                    "https://data.gov.cz/podmínky-užití/není-autorskoprávně-chráněnou-databází/",
                                databáze_chráněná_zvláštními_právy:
                                    "https://data.gov.cz/podmínky-užití/není-chráněna-zvláštním-právem-pořizovatele-databáze/",
                                osobní_údaje: "https://data.gov.cz/podmínky-užití/neobsahuje-osobní-údaje/",
                            },
                            přístupové_url: "./",
                        },
                    ],
                }),
                userData: JSON.stringify({
                    accessToken,
                    datasetId,
                    sessionId: newsession.id,
                }),
            })
            .expect(302)
            .end((err: Error | CustomError, res: supertest.Response) => {
                if (err) {
                    done(err);
                }
                chai.expect(err).to.be.null;
                postgresConnector
                    .getConnection()
                    .query(`SELECT * FROM "session"`, { raw: true })
                    .then((results: any[]) => {
                        chai.expect(results[0]).to.be.an("array").that.is.empty;

                        supertest(app)
                            .get(`/datasets/${datasetId}`)
                            .set("Authorization", `Bearer ${accessToken}`)
                            .expect(200)
                            .end((err2: Error | CustomError, res2: supertest.Response) => {
                                if (err2) {
                                    done(err2);
                                }
                                chai.expect(err2).to.be.null;
                                chai.expect(res2.body.validationResult.status).to.be.equal("invalid");
                                chai.expect(res2.body.validationResult.messages).to.be.an("array");
                                chai.expect(res2.body.data).to.be.deep.equal({
                                    any: "data",
                                    distribuce: [
                                        {
                                            přístupové_url: "https://testtest.cz/test",
                                            iri:
                                                `${config.backend_url}${config.pathPrefix}/lod/catalog/${datasetId}` +
                                                `/distributions/url-ps__testtest.cz_test`,
                                        },
                                        {
                                            přístupové_url: "https://testtest.cz/test2",
                                            formát: "http://publications.europa.eu/resource/authority/file-type/CSV",
                                            iri:
                                                `${config.backend_url}${config.pathPrefix}/lod/catalog/${datasetId}` +
                                                `/distributions/csv-s__testtest.cz_test2`,
                                        },
                                        {
                                            přístupové_url: "https://testtest.cz/test2",
                                            formát: "http://publications.europa.eu/resource/authority/file-type/CSV",
                                            iri:
                                                `${config.backend_url}${config.pathPrefix}/lod/catalog/${datasetId}` +
                                                `/distributions/csv-s__testtest.cz_test2_1`,
                                        },
                                        {
                                            přístupové_url: "https://testtest.cz/test3service",
                                            přístupová_služba: {
                                                typ: "Datová služba",
                                                přístupový_bod: "https://testtest.cz/test3service",
                                                název: {
                                                    cs: "test service",
                                                },
                                                iri:
                                                    `${config.backend_url}${config.pathPrefix}/lod/catalog/${datasetId}` +
                                                    `/distributions/url-test.cz_test3service/DataService`,
                                            },
                                            iri:
                                                `${config.backend_url}${config.pathPrefix}/lod/catalog/${datasetId}` +
                                                `/distributions/url-test.cz_test3service`,
                                        },
                                        {
                                            přístupové_url: "",
                                        },
                                        {
                                            typ: "Distribuce",
                                            podmínky_užití: {
                                                typ: "Specifikace podmínek užití",
                                                autorské_dílo: "https://data.gov.cz/podmínky-užití/obsahuje-více-autorských-děl/",
                                                databáze_jako_autorské_dílo:
                                                    "https://data.gov.cz/podmínky-užití/není-autorskoprávně-chráněnou-databází/",
                                                databáze_chráněná_zvláštními_právy:
                                                    "https://data.gov.cz/podmínky-užití/není-chráněna-zvláštním-právem-pořizovatele-databáze/",
                                                osobní_údaje: "https://data.gov.cz/podmínky-užití/neobsahuje-osobní-údaje/",
                                            },
                                            přístupové_url: "./",
                                        },
                                    ],
                                    iri: `${config.backend_url}${config.pathPrefix}/lod/catalog/${datasetId}`,
                                    poskytovatel:
                                        `${config.linked_data.poskytovatel_url_prefix}` + `${res2.body.organization.slug}`,
                                });
                                done();
                            });
                    });
            });
    });
});
