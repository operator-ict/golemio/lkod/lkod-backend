import { CustomError, HTTPErrorHandler, ICustomErrorObject } from "@golemio/core/dist/shared/golemio-errors";
import * as chai from "chai";
import * as express from "express";
import { NextFunction, Request, Response } from "express";
import "mocha";
import * as sinon from "sinon";
import * as supertest from "supertest";
import { config } from "../../src/core/config";
import { log } from "../../src/core/helpers";
import { DatasetsRouter } from "../../src/resources/datasets";
import { FormDataRouter } from "../../src/resources/form-data";
import { AuthRouter } from "../../src/resources/auth";
import { SessionsRouter } from "../../src/resources/sessions";
import { init } from "./00_init.test";

describe("Sessions", () => {
    let sandbox: sinon.SinonSandbox;
    // Create clean express instance
    const app = express();
    let authRouter: AuthRouter;
    let datasetsRouter: any; // :any for access to private properties
    let formDataRouter: any; // :any for access to private properties
    let sessionsRouter: any; // :any for access to private properties

    before(async () => {
        // mainly database initialization
        await init();

        authRouter = new AuthRouter();
        datasetsRouter = new DatasetsRouter();
        formDataRouter = new FormDataRouter();
        sessionsRouter = new SessionsRouter();

        app.use(express.json());
        app.use(express.urlencoded({ extended: true }));
        // Mount the tested router to the express instance
        app.use("/auth", authRouter.router);
        app.use("/datasets", datasetsRouter.router);
        app.use("/form-data", formDataRouter.router);
        app.use("/sessions", sessionsRouter.router);
        app.use(`/form-data`, (err: any, req: Request, res: Response, next: NextFunction) => {
            res.redirect(config.frontend_url + `?e=${encodeURIComponent(JSON.stringify(err.toObject ? err.toObject() : err))}`);
        });
        app.use((err: any, req: Request, res: Response, next: NextFunction) => {
            // hadle body parser errors
            if (err instanceof SyntaxError) {
                err = new CustomError(err.message, true, "App", (err as any)?.status || 500, err.message);
            }

            const warnCodes = [400, 401, 403, 404, 422];
            const errObject: ICustomErrorObject = HTTPErrorHandler.handle(
                err,
                log,
                warnCodes.includes(err.code) ? "warn" : "error"
            );
            log.silly("Error caught by the router error handler.");
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(errObject.error_status || 500).send(errObject);
        });
    });

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });

    describe("user role", () => {
        let accessToken: string;
        let datasetId: string;

        before((done) => {
            // get access token
            supertest(app)
                .post("/auth/login")
                .send({
                    email: "test@golemio.cz",
                    password: "pass",
                })
                .end((err: Error | CustomError, res: supertest.Response) => {
                    if (err || res.statusCode !== 200) {
                        done(err);
                    }
                    accessToken = res.body.accessToken;
                    done();
                });
        });

        before((done) => {
            // get dataset id
            supertest(app)
                .post("/datasets")
                .send({
                    organizationId: 1,
                })
                .set("Authorization", `Bearer ${accessToken}`)
                .end((err: Error | CustomError, res: supertest.Response) => {
                    if (err || res.statusCode !== 201) {
                        done(err);
                    }
                    datasetId = res.body.id;
                    done();
                });
        });

        it("should creates new Session at POST /sessions", (done) => {
            const url = "/sessions";
            supertest(app)
                .post(url)
                .send({
                    datasetId,
                })
                .set("Authorization", `Bearer ${accessToken}`)
                .expect(201)
                .end((err: Error | CustomError, res: supertest.Response) => {
                    if (err) {
                        done(err);
                    }
                    chai.expect(res.body).to.haveOwnProperty("createdAt").to.be.a("string");
                    chai.expect(res.body).to.haveOwnProperty("id").to.be.a("string");
                    chai.expect(res.body).to.haveOwnProperty("datasetId").to.be.equal(datasetId);
                    chai.expect(res.body).to.haveOwnProperty("userId").to.be.equal(2);
                    chai.expect(res.body).to.haveOwnProperty("expiresIn").to.be.equal(3600);
                    chai.expect(err).to.be.null;
                    done();
                });
        });

        it("should throws error if bad data was send at POST /sessions", (done) => {
            const url = "/sessions";
            supertest(app)
                .post(url)
                .send({
                    dataset: datasetId,
                })
                .set("Authorization", `Bearer ${accessToken}`)
                .expect(400, done);
        });

        it("should throws error if bad id was send at POST /sessions", (done) => {
            const url = "/sessions";
            supertest(app)
                .post(url)
                .send({
                    datasetId: "b3bef705-e1ac-4ecd-aed2-fafa0ca6e2f6",
                })
                .set("Authorization", `Bearer ${accessToken}`)
                .expect(404, done);
        });

        it("should deletes old sessions", async () => {
            // TODO
        });
    });

    describe("admin role", () => {
        let accessToken: string;
        let datasetId: string;

        before((done) => {
            // get access token
            supertest(app)
                .post("/auth/login")
                .send({
                    email: "admin@golemio.cz",
                    password: "pass",
                })
                .end((err: Error | CustomError, res: supertest.Response) => {
                    if (err || res.statusCode !== 200) {
                        done(err);
                    }
                    accessToken = res.body.accessToken;
                    done();
                });
        });

        before((done) => {
            // get dataset id
            supertest(app)
                .post("/datasets")
                .send({
                    organizationId: 3,
                })
                .set("Authorization", `Bearer ${accessToken}`)
                .end((err: Error | CustomError, res: supertest.Response) => {
                    if (err || res.statusCode !== 201) {
                        done(err);
                    }
                    datasetId = res.body.id;
                    done();
                });
        });

        it("should creates new Session at POST /sessions", (done) => {
            const url = "/sessions";
            supertest(app)
                .post(url)
                .send({
                    datasetId,
                })
                .set("Authorization", `Bearer ${accessToken}`)
                .expect(201)
                .end((err: Error | CustomError, res: supertest.Response) => {
                    if (err) {
                        done(err);
                    }
                    chai.expect(res.body).to.haveOwnProperty("createdAt").to.be.a("string");
                    chai.expect(res.body).to.haveOwnProperty("id").to.be.a("string");
                    chai.expect(res.body).to.haveOwnProperty("datasetId").to.be.equal(datasetId);
                    chai.expect(res.body).to.haveOwnProperty("userId").to.be.equal(3);
                    chai.expect(res.body).to.haveOwnProperty("expiresIn").to.be.equal(3600);
                    chai.expect(err).to.be.null;
                    done();
                });
        });

        it("should throws error if bad data was send at POST /sessions", (done) => {
            const url = "/sessions";
            supertest(app)
                .post(url)
                .send({
                    dataset: datasetId,
                })
                .set("Authorization", `Bearer ${accessToken}`)
                .expect(400, done);
        });

        it("should throws error if bad id was send at POST /sessions", (done) => {
            const url = "/sessions";
            supertest(app)
                .post(url)
                .send({
                    datasetId: "b3bef705-e1ac-4ecd-aed2-fafa0ca6e2f6",
                })
                .set("Authorization", `Bearer ${accessToken}`)
                .expect(404, done);
        });
    });
});
