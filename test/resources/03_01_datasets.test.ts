import { CustomError, HTTPErrorHandler, ICustomErrorObject } from "@golemio/core/dist/shared/golemio-errors";
import * as chai from "chai";
import * as express from "express";
import { NextFunction, Request, Response } from "express";
import "mocha";
import * as sinon from "sinon";
import * as supertest from "supertest";
import { config } from "../../src/core/config";
import { postgresConnector } from "../../src/core/database";
import { log } from "../../src/core/helpers";
import CachedLookups from "../../src/core/helpers/LookupQuery/CachedLookups";
import { ConnectionStrategyFactory } from "../../src/core/strategies/ConnectionStrategyFactory";
import { DatasetModel, DatasetStatus } from "../../src/models/DatasetModel";
import { AuthRouter } from "../../src/resources/auth";
import { DatasetsRouter } from "../../src/resources/datasets";
import { init } from "./00_init.test";

describe("Datasets", () => {
    let sandbox: sinon.SinonSandbox;
    // Create clean express instance
    const app = express();
    let authRouter: AuthRouter;
    let datasetsRouter: any; // :any for access to private properties

    before(async () => {
        // mainly database initialization
        await init();

        authRouter = new AuthRouter();
        datasetsRouter = new DatasetsRouter();

        app.use(express.json());
        app.use(express.urlencoded({ extended: true }));
        // Mount the tested router to the express instance
        app.use("/auth", authRouter.router);
        app.use("/datasets", datasetsRouter.router);
        app.use((err: any, req: Request, res: Response, next: NextFunction) => {
            // handle body parser errors
            if (err instanceof SyntaxError) {
                err = new CustomError(err.message, true, "App", (err as any)?.status || 500, err.message);
            }

            const warnCodes = [400, 401, 403, 404, 422];
            const errObject: ICustomErrorObject = HTTPErrorHandler.handle(
                err,
                log,
                warnCodes.includes(err.code) ? "warn" : "error"
            );
            log.silly("Error caught by the router error handler.");
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(errObject.error_status || 500).send(errObject);
        });
    });

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        let connectionStrategy = ConnectionStrategyFactory.getConnectionStrategy();
        sandbox.stub(connectionStrategy, "uploadDataset").callsFake(() => Promise.resolve(true));
        sandbox.stub(connectionStrategy, "removeDatasetCascade").callsFake(() => Promise.resolve(true));
        sandbox.stub(CachedLookups.getInstance(), <any>"getAllowedValues").callsFake(() => Promise.resolve([]));
    });

    afterEach(() => {
        sandbox.restore();
    });

    describe("role user", () => {
        let accessToken: string;
        let newdataset: any;

        before((done) => {
            // get access token
            supertest(app)
                .post("/auth/login")
                .send({
                    email: "test@golemio.cz",
                    password: "pass",
                })
                .end((err: Error | CustomError, res: supertest.Response) => {
                    if (err || res.statusCode !== 200) {
                        done(err);
                    }
                    accessToken = res.body.accessToken;
                    done();
                });
        });

        it("should creates new Dataset at POST /datasets", (done) => {
            const url = "/datasets";
            supertest(app)
                .post(url)
                .send({
                    organizationId: 1,
                })
                .set("Authorization", `Bearer ${accessToken}`)
                .expect(201)
                .end((err: Error | CustomError, res: supertest.Response) => {
                    if (err) {
                        done(err);
                    }
                    newdataset = res.body;
                    chai.expect(res.body).to.haveOwnProperty("createdAt").to.be.a("string");
                    chai.expect(res.body).to.haveOwnProperty("id").to.be.a("string");
                    chai.expect(res.body).to.haveOwnProperty("organizationId").to.be.equal(1);
                    chai.expect(res.body).to.haveOwnProperty("status").to.be.equal("created");
                    chai.expect(res.body).to.haveOwnProperty("userId").to.be.equal(2);
                    chai.expect(res.body).to.haveOwnProperty("isReadOnly").to.be.equal(false);
                    chai.expect(err).to.be.null;
                    done();
                });
        });

        it("should not create new Dataset for foreign organization at POST /datasets", (done) => {
            const url = "/datasets";
            supertest(app)
                .post(url)
                .send({
                    organizationId: 2,
                })
                .set("Authorization", `Bearer ${accessToken}`)
                .expect(403, done);
        });

        it("should throws error if bad data was send at POST /datasets", (done) => {
            const url = "/datasets";
            supertest(app)
                .post(url)
                .send({
                    organization: 1,
                })
                .set("Authorization", `Bearer ${accessToken}`)
                .expect(400, done);
        });

        it("should throws error if user has no permissions to organization at POST /datasets", (done) => {
            const url = "/datasets";
            supertest(app)
                .post(url)
                .send({
                    organizationId: 2,
                })
                .set("Authorization", `Bearer ${accessToken}`)
                .expect(403, done);
        });

        it("should return all datasets at GET /datasets", (done) => {
            const url = "/datasets";
            supertest(app)
                .get(url)
                .set("Authorization", `Bearer ${accessToken}`)
                .expect(200)
                .end((err: Error | CustomError, res: supertest.Response) => {
                    if (err) {
                        done(err);
                    }
                    chai.expect(res.body).to.be.deep.equal([{ name: null, keywords: null, ...newdataset }]);
                    chai.expect(err).to.be.null;
                    done();
                });
        });

        it("should return filtered datasets at GET /datasets (organizationId=1)", (done) => {
            const url = "/datasets";
            supertest(app)
                .get(url)
                .query("organizationId=1")
                .set("Authorization", `Bearer ${accessToken}`)
                .expect(200)
                .end((err: Error | CustomError, res: supertest.Response) => {
                    if (err) {
                        done(err);
                    }
                    chai.expect(res.body).to.be.deep.equal([{ name: null, keywords: null, ...newdataset }]);
                    chai.expect(err).to.be.null;
                    done();
                });
        });

        it("should return filtered datasets at GET /datasets (organizationId=2)", (done) => {
            const url = "/datasets";
            supertest(app)
                .get(url)
                .query("organizationId=2")
                .set("Authorization", `Bearer ${accessToken}`)
                .expect(200)
                .end((err: Error | CustomError, res: supertest.Response) => {
                    if (err) {
                        done(err);
                    }
                    chai.expect(res.body).to.be.deep.equal([]);
                    chai.expect(err).to.be.null;
                    done();
                });
        });

        it("should return validation error datasets at GET /datasets (status=unknown)", (done) => {
            const url = "/datasets";
            supertest(app)
                .get(url)
                .query("status=unknown")
                .set("Authorization", `Bearer ${accessToken}`)
                .expect(400)
                .end((err: Error | CustomError, res: supertest.Response) => {
                    if (err) {
                        done(err);
                    }
                    chai.expect(res.body.error_info).contains("Invalid status value.");
                    done();
                });
        });

        it("should return filtered datasets at GET /datasets (status=created)", (done) => {
            const url = "/datasets";
            supertest(app)
                .get(url)
                .query("status=created")
                .set("Authorization", `Bearer ${accessToken}`)
                .expect(200)
                .end((err: Error | CustomError, res: supertest.Response) => {
                    if (err) {
                        done(err);
                    }
                    chai.expect(res.body).to.be.deep.equal([{ name: null, keywords: null, ...newdataset }]);
                    chai.expect(err).to.be.null;
                    done();
                });
        });

        it("should return filtered datasets at GET /datasets (status[]=published&status[]=created)", (done) => {
            const url = "/datasets";
            supertest(app)
                .get(url)
                .query("status=published&status=created")
                .set("Authorization", `Bearer ${accessToken}`)
                .expect(200)
                .end((err: Error | CustomError, res: supertest.Response) => {
                    if (err) {
                        done(err);
                    }
                    chai.expect(res.body).to.deep.equal([{ name: null, keywords: null, ...newdataset }]);
                    chai.expect(err).to.be.null;
                    done();
                });
        });

        it("should return validation error datasets at GET /datasets (publisherIri=222)", (done) => {
            const url = "/datasets";
            supertest(app)
                .get(url)
                .query("publisherIri=222")
                .set("Authorization", `Bearer ${accessToken}`)
                .expect(400)
                .end((err: Error | CustomError, res: supertest.Response) => {
                    if (err) {
                        done(err);
                    }
                    chai.expect(res.body.error_info).contains("Invalid publisher value.");
                    done();
                });
        });

        it("should return validation error datasets at GET /datasets (formatIris[]=222)", (done) => {
            const url = "/datasets";
            supertest(app)
                .get(url)
                .query("formatIris[]=222")
                .set("Authorization", `Bearer ${accessToken}`)
                .expect(400)
                .end((err: Error | CustomError, res: supertest.Response) => {
                    if (err) {
                        done(err);
                    }
                    chai.expect(res.body.error_info).contains("Invalid format iri value.");
                    done();
                });
        });

        it("should return validation error datasets at GET /datasets (themeIris[]=222)", (done) => {
            const url = "/datasets";
            supertest(app)
                .get(url)
                .query("themeIris[]=222")
                .set("Authorization", `Bearer ${accessToken}`)
                .expect(400)
                .end((err: Error | CustomError, res: supertest.Response) => {
                    if (err) {
                        done(err);
                    }
                    chai.expect(res.body.error_info).contains("Invalid theme iri value.");
                    done();
                });
        });

        it("should return validation error datasets at GET /datasets (keywords[]=222)", (done) => {
            const url = "/datasets";
            supertest(app)
                .get(url)
                .query("keywords[]=222")
                .set("Authorization", `Bearer ${accessToken}`)
                .expect(400)
                .end((err: Error | CustomError, res: supertest.Response) => {
                    if (err) {
                        done(err);
                    }
                    chai.expect(res.body.error_info).contains("Invalid keyword value.");
                    done();
                });
        });

        it("should return validation error datasets at GET /datasets (formatIris=222)", (done) => {
            const url = "/datasets";
            supertest(app)
                .get(url)
                .query("formatIris=222")
                .set("Authorization", `Bearer ${accessToken}`)
                .expect(400)
                .end((err: Error | CustomError, res: supertest.Response) => {
                    if (err) {
                        done(err);
                    }
                    chai.expect(res.body.error_info).contains("Invalid format iri value.");
                    done();
                });
        });

        it("should return validation error datasets at GET /datasets (themeIris=222)", (done) => {
            const url = "/datasets";
            supertest(app)
                .get(url)
                .query("themeIris=222")
                .set("Authorization", `Bearer ${accessToken}`)
                .expect(400)
                .end((err: Error | CustomError, res: supertest.Response) => {
                    if (err) {
                        done(err);
                    }
                    chai.expect(res.body.error_info).contains("Invalid theme iri value.");
                    done();
                });
        });

        it("should return validation error datasets at GET /datasets (keywords=222)", (done) => {
            const url = "/datasets";
            supertest(app)
                .get(url)
                .query("keywords=222")
                .set("Authorization", `Bearer ${accessToken}`)
                .expect(400)
                .end((err: Error | CustomError, res: supertest.Response) => {
                    if (err) {
                        done(err);
                    }
                    chai.expect(res.body.error_info).contains("Invalid keyword value.");
                    done();
                });
        });

        it("should return datasets at GET /datasets (keywords=222)", (done) => {
            (CachedLookups.getInstance()["getAllowedValues"] as any).restore();
            sandbox.stub(CachedLookups.getInstance(), <any>"getAllowedValues").callsFake(() => Promise.resolve(["222"]));
            const url = "/datasets";
            supertest(app)
                .get(url)
                .query("keywords=222")
                .set("Authorization", `Bearer ${accessToken}`)
                .expect(200)
                .end((err: Error | CustomError, res: supertest.Response) => {
                    if (err) {
                        done(err);
                    }
                    chai.expect(res.body).deep.equal([]);
                    chai.expect(err).to.be.null;
                    done();
                });
        });

        it("should return datasets at GET /datasets (limit=1 offset=0)", (done) => {
            const url = "/datasets";
            supertest(app)
                .get(url)
                .query("limit=1")
                .query("offset=0")
                .set("Authorization", `Bearer ${accessToken}`)
                .expect(200)
                .end((err: Error | CustomError, res: supertest.Response) => {
                    if (err) {
                        done(err);
                    }
                    chai.expect(res.body).to.be.an("array").lengthOf(1);
                    chai.expect(res.headers["x-total-count"]).to.be.equal("1");
                    chai.expect(err).to.be.null;
                    done();
                });
        });

        it("should return datasets at GET /datasets (limit=1 offset=1)", (done) => {
            const url = "/datasets";
            supertest(app)
                .get(url)
                .query("limit=1")
                .query("offset=1")
                .set("Authorization", `Bearer ${accessToken}`)
                .expect(200)
                .end((err: Error | CustomError, res: supertest.Response) => {
                    if (err) {
                        done(err);
                    }
                    chai.expect(res.body).to.be.an("array").lengthOf(0);
                    chai.expect(res.headers["x-total-count"]).to.be.equal("1");
                    chai.expect(err).to.be.null;
                    done();
                });
        });

        it("should return dataset at GET /datasets/{id}", (done) => {
            const url = `/datasets/${newdataset.id}`;
            supertest(app)
                .get(url)
                .set("Authorization", `Bearer ${accessToken}`)
                .expect(200)
                .end((err: Error | CustomError, res: supertest.Response) => {
                    if (err) {
                        done(err);
                    }
                    chai.expect(res.body).to.be.deep.equal({ name: null, keywords: null, ...newdataset });
                    chai.expect(err).to.be.null;
                    done();
                });
        });

        it("should throws error if dataset was not found at GET /datasets/{id}", (done) => {
            const url = `/datasets/b3bef705-e1ac-4ecd-aed2-fafa0ca6e2f6`;
            supertest(app).get(url).set("Authorization", `Bearer ${accessToken}`).expect(404, done);
        });

        it("should throws error 409 if Dataset cannot be changed at PATCH /datasets/{id}", (done) => {
            const url = `/datasets/${newdataset.id}`;
            supertest(app)
                .patch(url)
                .send({
                    status: "published",
                })
                .set("Authorization", `Bearer ${accessToken}`)
                .expect(409, done);
        });

        it("should change Dataset status at PATCH /datasets/{id}", (done) => {
            postgresConnector
                .getConnection()
                .query(`UPDATE "dataset" SET "status"='saved' WHERE "id"='${newdataset.id}'`)
                .then(() => {
                    const url = `/datasets/${newdataset.id}`;
                    supertest(app)
                        .patch(url)
                        .send({
                            status: "published",
                        })
                        .set("Authorization", `Bearer ${accessToken}`)
                        .expect(200)
                        .end((err: Error | CustomError, res: supertest.Response) => {
                            if (err) {
                                done(err);
                            }
                            chai.expect(res.body).to.haveOwnProperty("createdAt").to.be.equal(newdataset.createdAt);
                            chai.expect(res.body).to.haveOwnProperty("id").to.be.equal(newdataset.id);
                            chai.expect(res.body).to.haveOwnProperty("organizationId").to.be.equal(newdataset.organizationId);
                            chai.expect(res.body).to.haveOwnProperty("status").to.be.equal("published");
                            chai.expect(res.body).to.haveOwnProperty("userId").to.be.equal(newdataset.userId);
                            chai.expect(res.body).to.haveOwnProperty("updatedAt").to.be.a("string");
                            chai.expect(err).to.be.null;
                            done();
                        });
                });
        });

        it("should not change Dataset userId at PATCH /datasets/{id}", (done) => {
            const url = `/datasets/${newdataset.id}`;
            supertest(app)
                .patch(url)
                .send({
                    status: "published",
                    userId: 2,
                })
                .set("Authorization", `Bearer ${accessToken}`)
                .expect(403, done);
        });

        it("should change Dataset status at PATCH /datasets/{id} (unpublished)", (done) => {
            const url = `/datasets/${newdataset.id}`;
            supertest(app)
                .patch(url)
                .send({
                    status: "unpublished",
                })
                .set("Authorization", `Bearer ${accessToken}`)
                .expect(200)
                .end((err: Error | CustomError, res: supertest.Response) => {
                    if (err) {
                        done(err);
                    }
                    chai.expect(res.body).to.haveOwnProperty("createdAt").to.be.equal(newdataset.createdAt);
                    chai.expect(res.body).to.haveOwnProperty("id").to.be.equal(newdataset.id);
                    chai.expect(res.body).to.haveOwnProperty("organizationId").to.be.equal(newdataset.organizationId);
                    chai.expect(res.body).to.haveOwnProperty("status").to.be.equal("unpublished");
                    chai.expect(res.body).to.haveOwnProperty("userId").to.be.equal(newdataset.userId);
                    chai.expect(res.body).to.haveOwnProperty("updatedAt").to.be.a("string");
                    chai.expect(err).to.be.null;
                    done();
                });
        });

        it("should throws error on bad input data at PATCH /datasets/{id}", (done) => {
            const url = `/datasets/${newdataset.id}`;
            supertest(app)
                .patch(url)
                .send({
                    status: "saved",
                })
                .set("Authorization", `Bearer ${accessToken}`)
                .expect(400, done);
        });

        it("should delete Dataset at DELETE /datasets/{id}", (done) => {
            postgresConnector
                .getConnection()
                .query(
                    `` +
                        `INSERT INTO "dataset" (id, "userId", status, "createdAt", "updatedAt", data, "organizationId")` +
                        ` VALUES` +
                        `    ('f9511776-72d8-484a-bb0a-8167f4cba1d4', 2, 'published', NOW(), NOW(), ` +
                        `        '{"any":"data","iri":` +
                        `"${config.backend_url}${config.pathPrefix}/lod/catalog/f9511776-72d8-484a-bb0a-8167f4cba1d4"}'` +
                        `::json, 1);`
                )
                .then(() => {
                    const url = `/datasets/f9511776-72d8-484a-bb0a-8167f4cba1d4`;
                    return supertest(app).delete(url).set("Authorization", `Bearer ${accessToken}`).expect(204, Promise.resolve);
                })
                .then(() => {
                    return DatasetModel.findByPk("f9511776-72d8-484a-bb0a-8167f4cba1d4");
                })
                .then((res) => {
                    chai.expect(res?.status).to.be.equal(DatasetStatus.deleted);
                    done();
                })
                .catch((err) => {
                    done(err);
                });
        });

        it("should throws error if dataset was not found at DELETE /datasets/{id}", (done) => {
            const url = `/datasets/f9511776-72d8-484a-bb0a-8167f4cba1d4`;
            supertest(app).delete(url).set("Authorization", `Bearer ${accessToken}`).expect(404, done);
        });
    });

    describe("role admin", () => {
        let accessToken: string;
        let newdataset: any;

        before((done) => {
            // get access token
            supertest(app)
                .post("/auth/login")
                .send({
                    email: "admin@golemio.cz",
                    password: "pass",
                })
                .end((err: Error | CustomError, res: supertest.Response) => {
                    if (err || res.statusCode !== 200) {
                        done(err);
                    }
                    accessToken = res.body.accessToken;
                    done();
                });
        });

        it("should creates new Dataset at POST /datasets", (done) => {
            const url = "/datasets";
            supertest(app)
                .post(url)
                .send({
                    organizationId: 3,
                })
                .set("Authorization", `Bearer ${accessToken}`)
                .expect(201)
                .end((err: Error | CustomError, res: supertest.Response) => {
                    if (err) {
                        done(err);
                    }
                    newdataset = res.body;
                    chai.expect(res.body).to.haveOwnProperty("createdAt").to.be.a("string");
                    chai.expect(res.body).to.haveOwnProperty("id").to.be.a("string");
                    chai.expect(res.body).to.haveOwnProperty("organizationId").to.be.equal(3);
                    chai.expect(res.body).to.haveOwnProperty("status").to.be.equal("created");
                    chai.expect(res.body).to.haveOwnProperty("userId").to.be.equal(3);
                    chai.expect(res.body).to.haveOwnProperty("isReadOnly").to.be.equal(true);
                    chai.expect(err).to.be.null;
                    done();
                });
        });

        it("should not create new Dataset for foreign organization at POST /datasets", (done) => {
            const url = "/datasets";
            supertest(app)
                .post(url)
                .send({
                    organizationId: 2,
                })
                .set("Authorization", `Bearer ${accessToken}`)
                .expect(403, done);
        });

        it("should throws error if bad data was send at POST /datasets", (done) => {
            const url = "/datasets";
            supertest(app)
                .post(url)
                .send({
                    organization: 2,
                })
                .set("Authorization", `Bearer ${accessToken}`)
                .expect(400, done);
        });

        it("should throws error if user has no permissions to organization at POST /datasets", (done) => {
            const url = "/datasets";
            supertest(app)
                .post(url)
                .send({
                    organizationId: 4,
                })
                .set("Authorization", `Bearer ${accessToken}`)
                .expect(403, done);
        });

        it("should return all datasets at GET /datasets", (done) => {
            const url = "/datasets";

            supertest(app)
                .get(url)
                .set("Authorization", `Bearer ${accessToken}`)
                .expect(200)
                .end((err: Error | CustomError, res: supertest.Response) => {
                    if (err) {
                        done(err);
                    }
                    chai.expect(res.body[1]).to.be.deep.equal({ name: null, keywords: null, ...newdataset });
                    chai.expect(err).to.be.null;
                    done();
                });
        });

        it("should return filtered datasets at GET /datasets (organizationId=1)", (done) => {
            const url = "/datasets";
            supertest(app)
                .get(url)
                .query("organizationId=1")
                .set("Authorization", `Bearer ${accessToken}`)
                .expect(200)
                .end((err: Error | CustomError, res: supertest.Response) => {
                    if (err) {
                        done(err);
                    }
                    chai.expect(res.body).to.be.deep.equal([]);
                    chai.expect(err).to.be.null;
                    done();
                });
        });

        it("should return filtered datasets at GET /datasets (organizationId=2)", (done) => {
            const url = "/datasets";
            supertest(app)
                .get(url)
                .query("organizationId=2")
                .set("Authorization", `Bearer ${accessToken}`)
                .expect(200)
                .end((err: Error | CustomError, res: supertest.Response) => {
                    if (err) {
                        done(err);
                    }
                    chai.expect(res.body).to.be.deep.equal([]);
                    chai.expect(err).to.be.null;
                    done();
                });
        });

        it("should return filtered datasets at GET /datasets (organizationId=3)", (done) => {
            const url = "/datasets";
            supertest(app)
                .get(url)
                .query("organizationId=3")
                .set("Authorization", `Bearer ${accessToken}`)
                .expect(200)
                .end((err: Error | CustomError, res: supertest.Response) => {
                    if (err) {
                        done(err);
                    }
                    chai.expect(
                        (res.body as any[]).filter((el) => el.id != "032046b6-0a23-4e66-9100-5948b04f9e5c")
                    ).to.be.deep.equal([{ name: null, keywords: null, ...newdataset }]);
                    chai.expect(err).to.be.null;
                    done();
                });
        });

        it("should return filtered datasets at GET /datasets (organizationId=4)", (done) => {
            const url = "/datasets";
            supertest(app)
                .get(url)
                .query("organizationId=4")
                .set("Authorization", `Bearer ${accessToken}`)
                .expect(200)
                .end((err: Error | CustomError, res: supertest.Response) => {
                    if (err) {
                        done(err);
                    }
                    chai.expect(res.body).to.be.deep.equal([]);
                    chai.expect(err).to.be.null;
                    done();
                });
        });

        it("should return dataset at GET /datasets/{id}", (done) => {
            const url = `/datasets/${newdataset.id}`;
            supertest(app)
                .get(url)
                .set("Authorization", `Bearer ${accessToken}`)
                .expect(200)
                .end((err: Error | CustomError, res: supertest.Response) => {
                    if (err) {
                        done(err);
                    }
                    chai.expect(res.body).to.be.deep.equal({ name: null, keywords: null, ...newdataset });
                    chai.expect(err).to.be.null;
                    done();
                });
        });

        it("should throws error if dataset was not found at GET /datasets/{id}", (done) => {
            const url = `/datasets/b3bef705-e1ac-4ecd-aed2-fafa0ca6e2f6`;
            supertest(app).get(url).set("Authorization", `Bearer ${accessToken}`).expect(404, done);
        });

        it("should throws error 409 if Dataset cannot be changed at PATCH /datasets/{id}", (done) => {
            const url = `/datasets/${newdataset.id}`;
            supertest(app)
                .patch(url)
                .send({
                    status: "published",
                })
                .set("Authorization", `Bearer ${accessToken}`)
                .expect(409, done);
        });

        it("should change Dataset status at PATCH /datasets/{id} (published)", (done) => {
            postgresConnector
                .getConnection()
                .query(`UPDATE "dataset" SET "status"='saved' WHERE "id"='${newdataset.id}'`)
                .then(() => {
                    const url = `/datasets/${newdataset.id}`;
                    supertest(app)
                        .patch(url)
                        .send({
                            status: "published",
                        })
                        .set("Authorization", `Bearer ${accessToken}`)
                        .expect(200)
                        .end((err: Error | CustomError, res: supertest.Response) => {
                            if (err) {
                                done(err);
                            }
                            chai.expect(res.body).to.haveOwnProperty("createdAt").to.be.equal(newdataset.createdAt);
                            chai.expect(res.body).to.haveOwnProperty("id").to.be.equal(newdataset.id);
                            chai.expect(res.body).to.haveOwnProperty("organizationId").to.be.equal(newdataset.organizationId);
                            chai.expect(res.body).to.haveOwnProperty("status").to.be.equal("published");
                            chai.expect(res.body).to.haveOwnProperty("userId").to.be.equal(newdataset.userId);
                            chai.expect(res.body).to.haveOwnProperty("updatedAt").to.be.a("string");
                            chai.expect(err).to.be.null;
                            done();
                        });
                });
        });

        it("should change Dataset status at PATCH /datasets/{id} (unpublished)", (done) => {
            const url = `/datasets/${newdataset.id}`;
            supertest(app)
                .patch(url)
                .send({
                    status: "unpublished",
                })
                .set("Authorization", `Bearer ${accessToken}`)
                .expect(200)
                .end((err: Error | CustomError, res: supertest.Response) => {
                    if (err) {
                        done(err);
                    }
                    chai.expect(res.body).to.haveOwnProperty("createdAt").to.be.equal(newdataset.createdAt);
                    chai.expect(res.body).to.haveOwnProperty("id").to.be.equal(newdataset.id);
                    chai.expect(res.body).to.haveOwnProperty("organizationId").to.be.equal(newdataset.organizationId);
                    chai.expect(res.body).to.haveOwnProperty("status").to.be.equal("unpublished");
                    chai.expect(res.body).to.haveOwnProperty("userId").to.be.equal(newdataset.userId);
                    chai.expect(res.body).to.haveOwnProperty("updatedAt").to.be.a("string");
                    chai.expect(err).to.be.null;
                    done();
                });
        });

        it("should not change Dataset userId at PATCH /datasets/{id}", (done) => {
            const url = `/datasets/${newdataset.id}`;
            supertest(app)
                .patch(url)
                .send({
                    status: "published",
                    userId: 2,
                })
                .set("Authorization", `Bearer ${accessToken}`)
                .expect(403, done);
        });

        it("should throws error on bad input data at PATCH /datasets/{id}", (done) => {
            const url = `/datasets/${newdataset.id}`;
            supertest(app)
                .patch(url)
                .send({
                    status: "saved",
                })
                .set("Authorization", `Bearer ${accessToken}`)
                .expect(400, done);
        });

        it("should delete Dataset at DELETE /datasets/{id}", (done) => {
            postgresConnector
                .getConnection()
                .query(
                    `` +
                        `INSERT INTO "dataset" (id, "userId", status, "createdAt", "updatedAt", data, "organizationId")` +
                        ` VALUES` +
                        `    ('f9511776-72d8-484a-bb0a-8167f4cba1d5', 1, 'published', NOW(), NOW(), ` +
                        `        '{"any":"data","iri":` +
                        `"${config.backend_url}${config.pathPrefix}/lod/catalog/f9511776-72d8-484a-bb0a-8167f4cba1d5"}'` +
                        `::json, 3);`
                )
                .then(() => {
                    const url = `/datasets/f9511776-72d8-484a-bb0a-8167f4cba1d5`;
                    return supertest(app).delete(url).set("Authorization", `Bearer ${accessToken}`).expect(204, Promise.resolve);
                })
                .then(() => {
                    return DatasetModel.findByPk("f9511776-72d8-484a-bb0a-8167f4cba1d5");
                })
                .then((res) => {
                    chai.expect(res?.status).to.be.equal(DatasetStatus.deleted);
                    done();
                })
                .catch((err) => {
                    done(err);
                });
        });

        it("should throws error if dataset was not found at DELETE /datasets/{id}", (done) => {
            const url = `/datasets/f9511776-72d8-484a-bb0a-8167f4cba1d5`;
            supertest(app).delete(url).set("Authorization", `Bearer ${accessToken}`).expect(404, done);
        });
    });

    describe("role superadmin", () => {
        let accessToken: string;
        let newdataset: any;

        before((done) => {
            // get access token
            supertest(app)
                .post("/auth/login")
                .send({
                    email: "superadmin@golemio.cz",
                    password: "pass",
                })
                .end((err: Error | CustomError, res: supertest.Response) => {
                    if (err || res.statusCode !== 200) {
                        done(err);
                    }
                    accessToken = res.body.accessToken;
                    done();
                });
        });

        it("should creates new Dataset at POST /datasets", (done) => {
            const url = "/datasets";
            supertest(app)
                .post(url)
                .send({
                    organizationId: 3,
                })
                .set("Authorization", `Bearer ${accessToken}`)
                .expect(201)
                .end((err: Error | CustomError, res: supertest.Response) => {
                    if (err) {
                        done(err);
                    }
                    newdataset = res.body;
                    chai.expect(res.body).to.haveOwnProperty("createdAt").to.be.a("string");
                    chai.expect(res.body).to.haveOwnProperty("id").to.be.a("string");
                    chai.expect(res.body).to.haveOwnProperty("organizationId").to.be.equal(3);
                    chai.expect(res.body).to.haveOwnProperty("status").to.be.equal("created");
                    chai.expect(res.body).to.haveOwnProperty("userId").to.be.equal(5);
                    chai.expect(res.body).to.haveOwnProperty("isReadOnly").to.be.equal(true);
                    chai.expect(err).to.be.null;
                    done();
                });
        });

        it("should change Dataset userId at PATCH /datasets/{id}", (done) => {
            const url = `/datasets/${newdataset.id}`;
            postgresConnector
                .getConnection()
                .query(`UPDATE "dataset" SET "status"='saved' WHERE "id"='${newdataset.id}'`)
                .then(() => {
                    supertest(app)
                        .patch(url)
                        .send({
                            status: "published",
                            userId: 2,
                        })
                        .set("Authorization", `Bearer ${accessToken}`)
                        .expect(200)
                        .end((err, res) => {
                            if (err) {
                                done(err);
                                return;
                            }
                            chai.expect(res.body).to.haveOwnProperty("createdAt").to.be.a("string");
                            chai.expect(res.body).to.haveOwnProperty("id").to.be.a("string");
                            chai.expect(res.body).to.haveOwnProperty("organizationId").to.be.equal(3);
                            chai.expect(res.body).to.haveOwnProperty("status").to.be.equal("published");
                            chai.expect(res.body).to.haveOwnProperty("userId").to.be.equal(2);
                            chai.expect(res.body).to.haveOwnProperty("isReadOnly").to.be.equal(true);
                            chai.expect(err).to.be.null;
                            done();
                        });
                });
        });
    });
});
