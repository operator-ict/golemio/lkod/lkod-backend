import { CustomError, HTTPErrorHandler, ICustomErrorObject } from "@golemio/core/dist/shared/golemio-errors";
import * as chai from "chai";
import * as express from "express";
import { NextFunction, Request, Response } from "express";
import "mocha";
import * as sinon from "sinon";
import * as supertest from "supertest";
import { config, IConfig } from "../../src/core/config";
import { postgresConnector } from "../../src/core/database";
import { log } from "../../src/core/helpers";
import { LODRouter } from "../../src/resources/lod";
import { init } from "./00_init.test";

describe("LOD", () => {
    let sandbox: sinon.SinonSandbox;
    // Create clean express instance
    const app = express();
    let lodRouter: any; // :any for access to private properties
    const datasetId = "f9511776-72d8-484a-bb0a-8167f4cba1d4";
    let linkedDataConf: IConfig["linked_data"];

    before(async () => {
        // mainly database initialization
        await init();

        lodRouter = new LODRouter();

        app.use(express.json());
        app.use(express.urlencoded({ extended: true }));
        // Mount the tested router to the express instance
        app.use("/lod/catalog", lodRouter.router);
        app.use((err: any, req: Request, res: Response, next: NextFunction) => {
            // hadle body parser errors
            if (err instanceof SyntaxError) {
                err = new CustomError(err.message, true, "App", (err as any)?.status || 500, err.message);
            }

            const warnCodes = [400, 401, 403, 404, 422];
            const errObject: ICustomErrorObject = HTTPErrorHandler.handle(
                err,
                log,
                warnCodes.includes(err.code) ? "warn" : "error"
            );
            log.silly("Error caught by the router error handler.");
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(errObject.error_status || 500).send(errObject);
        });

        // add test data
        await postgresConnector
            .getConnection()
            .query(
                `INSERT INTO "dataset" (id, "userId", status, "createdAt", "updatedAt", data, "organizationId") VALUES` +
                    `('f9511776-72d8-484a-bb0a-8167f4cba1d4', 1, 'published', NOW(), NOW(), ` +
                    `'{"any":"data","iri":"${config.backend_url}${config.pathPrefix}/lod/catalog/` +
                    `f9511776-72d8-484a-bb0a-8167f4cba1d4"}'::json, 1);`
            );

        // set config
        linkedDataConf = { ...config.linked_data };
        config.linked_data = {
            ...config.linked_data,
            catalog_publisher_id: "02795281",
            catalog_name: "Katalog otevřených dat Golemio",
            catalog_description: "",
            catalog_homepage: "https://opendata.praha.eu",
        };
    });

    after(() => {
        // reset conf
        config.linked_data = { ...linkedDataConf };
    });

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should return catalog definition at GET /lod/catalog", (done) => {
        const url = "/lod/catalog";
        supertest(app)
            .get(url)
            .expect(200)
            .end((err: Error | CustomError, res: supertest.Response) => {
                if (err) {
                    done(err);
                }
                chai.expect(res.body).to.be.deep.equal({
                    "@context": config.linked_data.catalog_context,
                    iri: `${config.backend_url}${config.pathPrefix}/lod/catalog`,
                    poskytovatel: `${config.linked_data.poskytovatel_url_prefix}02795281`,
                    typ: "Katalog",
                    název: {
                        cs: "Katalog otevřených dat Golemio",
                    },
                    popis: {
                        cs: "",
                    },
                    domovská_stránka: "https://opendata.praha.eu",
                    datová_sada: [`testIRI`, `${config.backend_url}${config.pathPrefix}/lod/catalog/${datasetId}`],
                });
                chai.expect(err).to.be.null;
                done();
            });
    });

    it("should return lod dataset at GET /lod/catalog/{id}", (done) => {
        const url = `/lod/catalog/${datasetId}`;
        supertest(app)
            .get(url)
            .expect(200)
            .end((err: Error | CustomError, res: supertest.Response) => {
                if (err) {
                    done(err);
                }
                chai.expect(res.body).to.be.deep.equal({
                    any: "data",
                    iri: `${config.backend_url}${config.pathPrefix}/lod/catalog/${datasetId}`,
                });
                chai.expect(err).to.be.null;
                done();
            });
    });

    it("should return lod dataset at GET /lod/catalog/?publishers[]", (done) => {
        const url = `/lod/catalog/?publishers[]=test-org`;
        supertest(app)
            .get(url)
            .expect(200)
            .end((err: Error | CustomError, res: supertest.Response) => {
                if (err) {
                    done(err);
                }
                chai.expect(res.body).to.be.deep.equal({
                    "@context": config.linked_data.catalog_context,
                    iri: `${config.backend_url}${config.pathPrefix}/lod/catalog/?publishers[]=test-org`,
                    poskytovatel: `${config.linked_data.poskytovatel_url_prefix}02795281`,
                    typ: "Katalog",
                    název: {
                        cs: "test org - Katalog otevřených dat Golemio",
                    },
                    popis: {
                        cs: "Lorem ipsum dolor sit amet.",
                    },
                    domovská_stránka: "https://opendata.praha.eu",
                    datová_sada: [`${config.backend_url}${config.pathPrefix}/lod/catalog/${datasetId}`],
                });
                chai.expect(err).to.be.null;
                done();
            });
    });

    it("should return lod dataset at GET /lod/catalog/?publishers[]=xyz&publishers[]=test-org", (done) => {
        const url = `/lod/catalog/?publishers[]=xyz&publishers[]=test-org`;
        supertest(app)
            .get(url)
            .expect(200)
            .end((err: Error | CustomError, res: supertest.Response) => {
                if (err) {
                    done(err);
                }
                chai.expect(res.body).to.be.deep.equal({
                    "@context": config.linked_data.catalog_context,
                    iri: `${config.backend_url}${config.pathPrefix}/lod/catalog/?publishers[]=xyz&publishers[]=test-org`,
                    poskytovatel: `${config.linked_data.poskytovatel_url_prefix}02795281`,
                    typ: "Katalog",
                    název: {
                        cs: "Sdružený katalog otevřených dat Golemio",
                    },
                    popis: {
                        cs: "Katalog obsahuje datové sady organizací test org",
                    },
                    domovská_stránka: "https://opendata.praha.eu",
                    datová_sada: [`${config.backend_url}${config.pathPrefix}/lod/catalog/${datasetId}`],
                });
                chai.expect(err).to.be.null;
                done();
            });
    });

    it("should throws error if dataset was not found at GET /lod/catalog/{id}", (done) => {
        const url = `/lod/catalog/b3bef705-e1ac-4ecd-aed2-fafa0ca6e2f6`;
        supertest(app).get(url).expect(404, done);
    });
});
