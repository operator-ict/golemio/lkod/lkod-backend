import { config } from "../../src/core/config";
import { postgresConnector, redisConnector } from "../../src/core/database";
import { initModels } from "../../src/models";

const DBMigrate = require("db-migrate");
const dbmigrate = DBMigrate.getInstance(true, {
    cmdOptions: {
        env: "test",
        "migrations-dir": "db/migrations",
    },
});

export const init = async () => {
    // set postgres db schema to `test`
    config.database.schema = "test";

    // connect to postgres db
    const connection = await postgresConnector.connect();

    // create `test` schema in postgres db
    await connection.query(`DROP SCHEMA IF EXISTS ${config.database.schema} CASCADE;`);
    await connection.query(`CREATE SCHEMA ${config.database.schema};`);
    await connection.query(`SET search_path TO ${config.database.schema};`);

    // run migrations
    await dbmigrate.up();

    // init models
    initModels();

    // add test data
    await connection.query(
        `` +
            `INSERT INTO "user" ("email", "password", "name", "role", "hasDefaultPassword") VALUES` +
            `    ('test@golemio.cz','$2b$10$PCAwdjhktHpwOBJBW7RIFeGD8IYXiSMIohRdG83hl8j.lerlWOwP.','test user','user','false');` +
            `INSERT INTO "organization" ("name", "identificationNumber", "slug", "logo", "description") VALUES` +
            `    ('test org', '02795281', 'test-org', 'https://placehold.co/100x100', ` +
            `       'Lorem ipsum dolor sit amet.');` +
            `INSERT INTO "user_organization" ("userId", "organizationId") VALUES` +
            `    ('2','1');` +
            `INSERT INTO "user" ("email", "password", "name", "role", "hasDefaultPassword") VALUES` +
            `    ('admin@golemio.cz','$2b$10$Wsvnq8mxPWuV4XpWlMijWOX7m6Dy3kPRwW.qgL9x95NWvHdL0JEoC','test admin'` +
            `    ,'admin','false');` +
            `INSERT INTO "organization" ("name", "identificationNumber", "slug") VALUES` +
            `    ('second test org', '02795281', 'second-test-org');` +
            `INSERT INTO "organization" ("name", "identificationNumber", "slug", "arcGisFeed") VALUES` +
            `    ('arcgis test org', '02795282', 'arcgis-test-org', 'https://test-data-brno-cz-mestobrno.hub.arcgis.com/api/feed/dcat-us/1.1.json');` +
            `INSERT INTO "user_organization" ("userId", "organizationId") VALUES` +
            `    ('3','3');` +
            `INSERT INTO "user_organization" ("userId", "organizationId") VALUES` +
            `    ('2','3');` +
            `INSERT INTO "user" ("email", "password", "name", "role", "hasDefaultPassword") VALUES` +
            `    ('new@golemio.cz','$2b$10$PCAwdjhktHpwOBJBW7RIFeGD8IYXiSMIohRdG83hl8j.lerlWOwP.','new user','user','true');` +
            `INSERT INTO "user" ("email", "password", "name", "role", "hasDefaultPassword") VALUES` +
            `    ('superadmin@golemio.cz','$2b$10$Wsvnq8mxPWuV4XpWlMijWOX7m6Dy3kPRwW.qgL9x95NWvHdL0JEoC','test superadmin'` +
            `    ,'superadmin','false');` +
            `INSERT INTO "user" ("email", "password", "name", "role", "hasDefaultPassword") VALUES` +
            `    ('superadmin2@golemio.cz','$2b$10$Wsvnq8mxPWuV4XpWlMijWOX7m6Dy3kPRwW.qgL9x95NWvHdL0JEoC','test superadmin 2'` +
            `    ,'superadmin','false');` +
            `INSERT INTO "user" ("id", "email", "password", "name", "role", "hasDefaultPassword") VALUES` +
            `    ('666', 'deleteme@golemio.cz','$2b$10$Wsvnq8mxPWuV4XpWlMijWOX7m6Dy3kPRwW.qgL9x95NWvHdL0JEoC','delete me'` +
            `    ,'user','false');` +
            // eslint-disable-next-line max-len
            `INSERT INTO dataset (id,"userId",status,"createdAt","updatedAt","data","organizationId","validationResult",arcgisid) VALUES` +
            `('032046b6-0a23-4e66-9100-5948b04f9e5c',666,'published','2024-01-11 07:45:03.594','2024-09-16 08:45:04.999','{"iri": "testIRI","typ": "Datová sada","popis": { "cs": "TEST" },"téma": ["http://publications.europa.eu/resource/authority/data-theme/TRAN"],"název": { "cs": "Zákaz stání" },"@context": "https://ofn.gov.cz/rozhraní-katalogů-otevřených-dat/2021-01-11/kontexty/rozhraní-katalogů-otevřených-dat.jsonld","distribuce": [{"iri": "testIri","typ": "Distribuce","název": { "cs": "Dataset" },"formát": "http://publications.europa.eu/resource/authority/file-type/HTML","typ_média": "http://www.iana.org/assignments/media-types/text/html","podmínky_užití": {"typ": "Specifikace podmínek užití","autor": { "cs": "Test author" },"osobní_údaje": "https://data.gov.cz/podmínky-užití/neobsahuje-osobní-údaje/","autorské_dílo": "https://creativecommons.org/licenses/by/4.0/","databáze_jako_autorské_dílo": "https://data.gov.cz/podmínky-užití/není-autorskoprávně-chráněnou-databází/","databáze_chráněná_zvláštními_právy": "https://data.gov.cz/podmínky-užití/není-chráněna-zvláštním-právem-pořizovatele-databáze/"},"přístupové_url": "testUrl","soubor_ke_stažení": "testSoubor"},{"iri": "testIri","typ": "Distribuce","název": { "cs": "testDistribuce" },"formát": "http://publications.europa.eu/resource/authority/file-type/JSON","typ_média": "http://www.iana.org/assignments/media-types/application/json","podmínky_užití": {"typ": "Specifikace podmínek užití","autor": { "cs": "test" },"osobní_údaje": "https://data.gov.cz/podmínky-užití/neobsahuje-osobní-údaje/","autorské_dílo": "https://creativecommons.org/licenses/by/4.0/","databáze_jako_autorské_dílo": "https://data.gov.cz/podmínky-užití/není-autorskoprávně-chráněnou-databází/","databáze_chráněná_zvláštními_právy": "https://data.gov.cz/podmínky-užití/není-chráněna-zvláštním-právem-pořizovatele-databáze/"},"přístupové_url": "testUrl","soubor_ke_stažení": "testSoubor"}],"dokumentace": "testDokumentace","poskytovatel": "testPoskytovatel","prvek_rúian": ["https://linked.cuzk.cz/resource/ruian/region-soudrznosti/19","https://linked.cuzk.cz/resource/ruian/stat/1"],"kontaktní_bod": {"typ": "testOrganizace","e-mail": "testEmail","jméno": { "cs": "testJmeno" }},"klíčové_slovo": { "cs": ["Silniční doprava", "Doprava/Transportation"] },"geografické_území": ["https://publications.europa.eu/resource/authority/place/CZE_PRG", "https://sws.geonames.org/3067695/"],"periodicita_aktualizace": "http://publications.europa.eu/resource/authority/frequency/WEEKLY"}',3,NULL,'81871d51d93247e2bf3d2a2afae7f33e_0');`
    );

    // connect to redis
    await redisConnector.connect();
};
