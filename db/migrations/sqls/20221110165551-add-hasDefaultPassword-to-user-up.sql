ALTER TABLE "user"
    ADD COLUMN "hasDefaultPassword" boolean NOT NULL DEFAULT 'true';
ALTER TABLE "user"
    ADD COLUMN "createdAt" timestamp with time zone NOT NULL DEFAULT NOW();
ALTER TABLE "user"
    ADD COLUMN "updatedAt" timestamp with time zone;

UPDATE "user"
    SET "hasDefaultPassword"='false'
    WHERE password is not null;
