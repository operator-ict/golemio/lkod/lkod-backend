UPDATE "user"
SET "role" = 'admin'
WHERE "role" = 'superadmin';

-- The only way to remove an enum value is via a temporary enum
CREATE TYPE "enum_user_role_tmp"
    AS ENUM ('user', 'admin');

ALTER TABLE "user"
    ALTER COLUMN "role" DROP DEFAULT;
ALTER TABLE "user"
    ALTER COLUMN "role" TYPE "enum_user_role_tmp"
        USING ("role"::text::"enum_user_role_tmp");

DROP TYPE "enum_user_role";

ALTER TYPE "enum_user_role_tmp"
    RENAME TO "enum_user_role";
ALTER TABLE "user"
    ALTER COLUMN "role" SET DEFAULT 'user'::"enum_user_role";
