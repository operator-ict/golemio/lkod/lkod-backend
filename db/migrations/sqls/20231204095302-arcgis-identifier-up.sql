ALTER TABLE dataset ADD arcgisId varchar(50) NULL;
ALTER TABLE dataset ADD CONSTRAINT dataset_arcgisid_un UNIQUE (arcgisId);

drop view v_organization_with_arcgis_feed;
CREATE VIEW v_organization_with_arcgis_feed
AS SELECT organization.id,
    organization.name,
    organization."arcGisFeed",
    organization.slug
   FROM organization
  WHERE organization."arcGisFeed" IS NOT NULL;
