ALTER TABLE "dataset" ALTER COLUMN status TYPE VARCHAR(255);

DROP TYPE IF EXISTS enum_dataset_status;
CREATE TYPE enum_dataset_status AS ENUM
    ('created', 'saved', 'published', 'unpublished', 'deleted');

ALTER TABLE "dataset" ALTER COLUMN status TYPE enum_dataset_status USING (status::enum_dataset_status);
