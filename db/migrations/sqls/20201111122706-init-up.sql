
CREATE TYPE enum_dataset_status AS ENUM
    ('created', 'saved', 'published', 'unpublished');

CREATE TABLE "dataset"
(
    id uuid NOT NULL,
    "userId" bigint NOT NULL,
    status enum_dataset_status NOT NULL,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone,
    data json,
    "organizationId" bigint NOT NULL,
    CONSTRAINT dataset_pkey PRIMARY KEY (id)
);

CREATE TABLE "session"
(
    id uuid NOT NULL,
    "userId" bigint NOT NULL,
    "datasetId" uuid NOT NULL,
    "createdAt" timestamp with time zone NOT NULL,
    "expiresIn" integer NOT NULL,
    CONSTRAINT session_pkey PRIMARY KEY (id)
);

CREATE SEQUENCE organization_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

CREATE TABLE "organization"
(
    id bigint NOT NULL DEFAULT nextval('organization_id_seq'::regclass),
    name character varying(255) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT organization_pkey PRIMARY KEY (id)
);

CREATE TYPE enum_user_role AS ENUM
    ('user', 'admin');

CREATE SEQUENCE user_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

CREATE TABLE "user"
(
    id bigint NOT NULL DEFAULT nextval('user_id_seq'::regclass),
    email character varying(255) COLLATE pg_catalog."default" NOT NULL,
    password character varying(255) COLLATE pg_catalog."default",
    name character varying(255) COLLATE pg_catalog."default" NOT NULL,
    role enum_user_role NOT NULL DEFAULT 'user'::enum_user_role,
    CONSTRAINT user_pkey PRIMARY KEY (id)
);

CREATE TABLE user_organization
(
    "userId" bigint NOT NULL,
    "organizationId" bigint NOT NULL,
    CONSTRAINT user_organization_pkey PRIMARY KEY ("userId", "organizationId")

);


ALTER TABLE "dataset" ADD CONSTRAINT dataset_organizationid_fkey FOREIGN KEY ("organizationId")
    REFERENCES organization (id) MATCH SIMPLE
    ON UPDATE CASCADE
    ON DELETE CASCADE;
ALTER TABLE "dataset" ADD CONSTRAINT dataset_userid_fkey FOREIGN KEY ("userId")
    REFERENCES "user" (id) MATCH SIMPLE
    ON UPDATE CASCADE
    ON DELETE CASCADE;

ALTER TABLE "session" ADD CONSTRAINT session_datasetid_fkey FOREIGN KEY ("datasetId")
    REFERENCES dataset (id) MATCH SIMPLE
    ON UPDATE CASCADE
    ON DELETE CASCADE;

ALTER TABLE "user_organization" ADD CONSTRAINT user_organization_organizationid_fkey FOREIGN KEY ("organizationId")
    REFERENCES organization (id) MATCH SIMPLE
    ON UPDATE CASCADE
    ON DELETE CASCADE;
ALTER TABLE "user_organization" ADD CONSTRAINT user_organization_userid_fkey FOREIGN KEY ("userId")
    REFERENCES "user" (id) MATCH SIMPLE
    ON UPDATE CASCADE
    ON DELETE CASCADE;
