drop view v_dataset_basic_info;
create VIEW v_dataset_basic_info AS
select
	d.id,
	d.data ->> 'iri'::text as iri,
	(d.data -> 'název'::text) ->> 'cs'::text as title,
	(d.data -> 'popis'::text) ->> 'cs'::text as description,
	o.name as publisher,
	d.data -> 'téma'::text as theme_iris,
	(
	select
		array_agg(label)
	from
		themes
	where
		(d.data -> 'téma'::text) ? iri) as themes,
	f.formats as format_iris,
	(
	select
		array_agg(label)
	from
		file_types
	where
		f.formats ? iri) as formats,
	o.slug as publisher_slug,
	((d.data -> 'klíčové_slovo'::text) ->> 'cs'::text)::jsonb as keywords
from
	dataset d
join organization o on
	o.id = d."organizationId"
left join (
	select
		dataset.id,
		jsonb_agg(element.value ->> 'formát'::text) filter (
		where (element.value ->> 'formát'::text) is not null) as formats
	from
		dataset,
		lateral jsonb_array_elements(dataset.data -> 'distribuce'::text) element(value)
	where
		(dataset.data -> 'distribuce'::text) is not null
	group by
		dataset.id) f on
	f.id = d.id
where d.status = 'published';
