ALTER TABLE "organization"
    ADD COLUMN "slug" character varying(255) COLLATE pg_catalog."default" NOT NULL DEFAULT 'placeholder';

UPDATE "organization"
    SET "slug"=regexp_replace("name", '(\W+)', '-', 'g');

ALTER TABLE "organization" ALTER COLUMN "slug" DROP DEFAULT;
