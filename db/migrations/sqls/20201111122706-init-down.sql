
DROP TABLE user_organization;

DROP TABLE "session";

DROP TABLE dataset;
DROP TYPE enum_dataset_status;

DROP TABLE organization;
DROP SEQUENCE organization_id_seq;

DROP TABLE "user";
DROP SEQUENCE user_id_seq;
DROP TYPE enum_user_role
