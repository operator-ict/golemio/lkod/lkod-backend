ALTER TABLE organization ADD CONSTRAINT organization_slug_uniq UNIQUE (slug);

UPDATE dataset d
SET "data" = jsonb_set("data", '{poskytovatel}', to_jsonb(concat(left(d."data"->>'poskytovatel', -length(o."identificationNumber")), o.slug)))
FROM organization o
WHERE d."organizationId" = o.id
    AND d."data"->'poskytovatel' IS NOT NULL;
