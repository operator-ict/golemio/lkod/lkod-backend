UPDATE dataset d
SET "data" = jsonb_set("data", '{poskytovatel}', to_jsonb(concat(left(d."data"->>'poskytovatel', -length(o.slug)), o."identificationNumber")))
FROM organization o
WHERE d."organizationId" = o.id
    AND d."data"->'poskytovatel' IS NOT NULL;

ALTER TABLE organization DROP CONSTRAINT organization_slug_uniq;
