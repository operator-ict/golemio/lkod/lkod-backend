ALTER TABLE dataset DROP CONSTRAINT dataset_arcgisId_un;
ALTER TABLE dataset DROP COLUMN arcgisId;

drop view v_organization_with_arcgis_feed;
CREATE VIEW v_organization_with_arcgis_feed
AS SELECT organization.id,
    organization.name,
    organization."arcGisFeed"
   FROM organization
  WHERE organization."arcGisFeed" IS NOT NULL;