ALTER TABLE "organization"
    ADD COLUMN "logo" character varying(255) COLLATE pg_catalog."default";
ALTER TABLE "organization"
    ADD COLUMN "description" TEXT;
