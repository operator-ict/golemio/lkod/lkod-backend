alter table "organization" drop column "arcGisFeed";
drop view "v_organization_with_arcgis_feed";

-- importer user
delete from "user" where "email" = 'importer@lkod';
drop function get_importer_user_id();
