ALTER TABLE "organization"
    ADD COLUMN "identificationNumber" character varying(255) COLLATE pg_catalog."default" NOT NULL DEFAULT '02795281';
ALTER TABLE "organization"
    ALTER COLUMN "identificationNumber" DROP DEFAULT;
