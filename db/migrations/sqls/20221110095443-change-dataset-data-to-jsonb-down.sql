DROP INDEX dataset_data_gin;

ALTER TABLE dataset ALTER COLUMN "data" TYPE json USING "data"::json;
