ALTER TABLE "user"
    DROP COLUMN "hasDefaultPassword";
ALTER TABLE "user"
    DROP COLUMN "createdAt";
ALTER TABLE "user"
    DROP COLUMN "updatedAt";
