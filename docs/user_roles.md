# User roles

- superadmin
- admin
- user

## Superadmin

- Users with the role `superadmin` can manage users, organizations, user memberships in organizations.
- They can manage datasets in all organizations.

## Admin

- Users with the role `admin` can manage all datasets in their organization.
- They have access to datasets that are only in their organization.

## User

- Users with the role `user` can manage only datasets owned by them.
- They have access to datasets that are only in their organization.
