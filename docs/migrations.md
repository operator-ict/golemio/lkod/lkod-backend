# New Migration script

- main documentation of `db-migrate` https://db-migrate.readthedocs.io/en/latest/

## PostgreSQL migrations

- creating the new migration (creates js file and sql up and down files)
  - files for migration scripts are saved in directory `db/migrations/sqls/`
```
npx db-migrate create new-migration-name --env default --migrations-dir db/migrations --sql-file
```
- checking done/waitning migrations
```
npx db-migrate check --env default --migrations-dir db/migrations
```
- running all waiting migrations (up)
```
npx db-migrate up --env default --migrations-dir db/migrations
```
- undoing last migration (down)
```
npx db-migrate down --env default --migrations-dir db/migrations
```
