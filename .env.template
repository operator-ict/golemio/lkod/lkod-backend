# Port the app will listen on
PORT="3000"
# Node environment
NODE_ENV="development"
# Logging settings for "Debug" logging module: https://www.npmjs.com/package/debug
DEBUG="golemio-lkod-backend:*"
# Log level settings - silly>debug>verbose>info>warn>error, defaults to INFO if not set
LOG_LEVEL="INFO"
# Activity logging to PostgreSQL
ACTIVITY_LOG_ENABLED=true

# Connection to PostgreSQL database
DB_HOSTNAME="localhost"
DB_PORT="5432"
DB_DATABASE="fads"
DB_USERNAME="afd"
DB_PASSWORD="fadsa"
DB_SCHEMA="fdas"
DB_SSL=false

# Connection to Redis database
REDIS_CONN=redis://localhost/1

# Base URL of this app. Used for dataset iri
BACKEND_URL="https://rabin.golemio.cz"

# URL to redirect after form data processing
FRONTEND_URL="http://localhost/webdev/lkod/lkod-frontend"

# URL used to create links to the catalog
CATALOG_FRONTEND_URL="http://localhost:3000/"

# (model) Session validity in seconds, default 1 hour
SESSION_EXPIRES_IN=3600

# Login accessToken validity in seconds, default 12 hours
TOKEN_EXPIRES_IN=43200

# url path prefix, e.g. for usage in combination with traefik (traefik.frontend.rule: PathPrefix:/lkod-api)
PATH_PREFIX="/lkod-api"

# Sparql enabled, default true
SPARQL_ENABLED=true
# Sparql query endpoint for getting lookup data
SPARQL_QUERY_URL="http://localhost/lkod/query"
# Sparql data endpoint for uploading rdf data
SPARQL_DATA_URL="https://localhost/lkod/data"
# Sparql upload endpoint for deleting rdf data
SPARQL_UPDATE_URL="https://localhost/lkod/upload"
# Authorization header content, e.g. "Basic xyz...". Keep it empty if the sparql endpoint has no authentication
SPARQL_AUTH=""
# Sparql vocabularies upload/refresh cron definition (config/golemio-static-data.json, skos-vocabularies.json)
# At 02:00 on Sunday (with seconds).
SPARQL_VOCABULARY_CRONTIME="0 0 2 * * 0"
# At minute 30.
SPARQL_ORGANIZATIONS_CRONTIME="0 30 * * * *"

# Storage type: NONE | S3 | FTP
STORAGE_TYPE=FTP
# Storage file size limit in kilobytes, default 20MB
STORAGE_FILE_MAX_SIZE_IN_KB=20480
# FTP storage
FTP_HOST=
FTP_PORT=
FTP_USER=
FTP_PASSWORD=
FTP_PUBLIC_DOWNLOAD_PREFIX="https://storage.dev.cz/lkod-rabin"
## S3 storage
S3_ENDPOINT=
S3_ACCESS_KEY_ID=
S3_SECRET_ACCESS_KEY=
S3_BUCKET_NAME=

# Mailer transport type: DUMMY | SMTP, default DUMMY
MAILER_TYPE=DUMMY
MAILER_FROM="Golemio LKOD <lkod@golemio.cz>"
# SMTP mailer transport
SMTP_MAILER_PASS=
SMTP_MAILER_USER=
SMTP_MAILER_HOST=
SMTP_MAILER_PORT=
SMTP_MAILER_SECURE=
SMTP_MAILER_TLS_REJECT_UNAUTHORIZED=

# Linked data
# RDF publisher iri prefix, publisher iri is organization unique identifier
POSKYTOVATEL_URL_PREFIX="https://api-lkod.rabin.golemio.cz/organization/"
# Context of catalog returned by LODController, LKOD JSON-LD endpoints (`/lod/catalog`)
CATALOG_CONTEXT="https://pod-test.mvcr.gov.cz/otevřené-formální-normy/rozhraní-katalogů-otevřených-dat/draft/kontexty/rozhraní-katalogů-otevřených-dat.jsonld"
# Catalog publisher id (for publishe iri), LKOD JSON-LD endpoints (`/lod/catalog`)
CATALOG_PUBLISHER_ID="02795281"
# Catalog name, LKOD JSON-LD endpoints (`/lod/catalog`)
CATALOG_NAME="Katalog otevřených dat Golemio"
# Catalog description, LKOD JSON-LD endpoints (`/lod/catalog`)
CATALOG_DESCRIPTION=""
# Catalog homepage, LKOD JSON-LD endpoints (`/lod/catalog`)
CATALOG_HOMEPAGE="https://opendata.praha.eu"

# Custom modules
ARCGIS_ENABLED=false
ARCGIS_CRON_TIME="0 55 6 * * *"
