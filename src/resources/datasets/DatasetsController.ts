import { CustomError } from "@golemio/core/dist/shared/golemio-errors";
import { FindOptions, ForeignKeyConstraintError, literal, Op, ValidationError } from "sequelize";
import { v4 } from "uuid";
import { dataValidator, logActivity } from "../../core/helpers";
import { IConnectionStrategy } from "../../core/strategies/IConnectionStrategy";
import { DatasetModel, DatasetStatus, IDatasetAttributes } from "../../models/DatasetModel";
import { OrganizationModel } from "../../models/OrganizationModel";
import { SessionModel } from "../../models/SessionModel";
import { UserModel, UserRole } from "../../models/UserModel";
import QueryHelper from "../helpers/QueryHelper";
import { IDatasetFilter } from "./interfaces/IDatasetQueryParams";
import { findCurrentUserOrThrow } from "../../core/helpers/SessionUserHelper";
import { ConnectionStrategyFactory } from "../../core/strategies/ConnectionStrategyFactory";

/**
 * Dataset Controller class
 */
export class DatasetsController {
    private connectionStrategy: IConnectionStrategy;
    /**
     * Deletes old "created" datasets
     */
    constructor() {
        this.connectionStrategy = ConnectionStrategyFactory.getConnectionStrategy();
    }

    public static async deleteOldCreatedDatasets(): Promise<void> {
        const activeSessions = await SessionModel.findAll({
            attributes: ["datasetId"],
        });

        await DatasetModel.destroy({
            where: {
                status: {
                    [Op.eq]: DatasetStatus.created,
                },
                createdAt: {
                    [Op.lt]: literal(`now() - INTERVAL '2 days'`),
                },
                id: {
                    [Op.notIn]: activeSessions.map((session) => session.datasetId),
                },
            },
        });
    }

    /**
     * Returns all user's datasets
     *
     * @param {number} userId
     * @param {IDatasetFilter} params Params to specify the DB query
     * @returns {Promise<IDatasetAttributes[]>}
     */
    public async getAllDatasets(
        userId: number,
        params?: IDatasetFilter
    ): Promise<{ datasets: IDatasetAttributes[]; totalCount: number }> {
        try {
            const user = await findCurrentUserOrThrow(userId, {
                include: [UserModel.associations.organizations],
            });

            const attributes: FindOptions<IDatasetAttributes>["attributes"] = {
                include: [
                    [literal("data -> 'název' ->> 'cs'"), "name"],
                    [literal("data -> 'klíčové_slovo' -> 'cs'"), "keywords"],
                ],
            };

            const concatedWhere = QueryHelper.buildWherePart(params, user);

            const totalCount = await DatasetModel.count({
                include: [DatasetModel.associations.organization],
                where: concatedWhere,
            });

            const datasets = (await DatasetModel.findAll({
                attributes,
                include: [DatasetModel.associations.organization],
                where: concatedWhere,
                order: [[literal("name"), "ASC"]],
                limit: params?.limit,
                offset: params?.offset,
            })) as IDatasetAttributes[];

            return {
                datasets,
                totalCount,
            };
        } catch (err) {
            throw new CustomError("Error while getting all Datasets", true, this.constructor.name, 500, err);
        }
    }

    /**
     * Returns dataset by id. User id is used for authorization
     *
     * @param {string} datasetId UUID
     * @param {number} userId
     * @returns {IDatasetAttributes}
     */
    public async getDatasetById(datasetId: string, userId: number): Promise<IDatasetAttributes> {
        try {
            const user = await findCurrentUserOrThrow(userId, {
                include: [UserModel.associations.organizations],
            });

            const attributes: FindOptions<IDatasetAttributes>["attributes"] = {
                include: [
                    [literal("data -> 'název' ->> 'cs'"), "name"],
                    [literal("data -> 'klíčové_slovo' -> 'cs'"), "keywords"],
                ],
            };

            let where: FindOptions<IDatasetAttributes>["where"] = {
                id: datasetId,
                status: {
                    [Op.ne]: DatasetStatus.deleted,
                },
            };

            if (user?.role !== UserRole.superadmin) {
                where = {
                    ...where,
                    organizationId: {
                        [Op.in]: user?.organizations?.map((o) => o.id),
                    },
                };

                if (user?.role === UserRole.user) {
                    where = {
                        ...where,
                        userId: user?.id,
                    };
                }
            }

            const dataset = await DatasetModel.findOne({
                attributes,
                include: [DatasetModel.associations.organization],
                where,
            });

            if (!dataset) {
                throw new CustomError(`Dataset with id ${datasetId} was not found`, true, this.constructor.name, 404);
            }

            return (dataset.toJSON ? dataset.toJSON() : dataset) as IDatasetAttributes;
        } catch (err) {
            if (err instanceof CustomError) {
                throw err;
            } else {
                throw new CustomError("Error while Dataset by ID", true, this.constructor.name, 500, err);
            }
        }
    }

    /**
     * Creates new dataset for user
     *
     * @param {number} userId
     * @param {number} organizationId
     * @returns {IDatasetAttributes} New dataset
     */
    public async createNewDataset(userId: number, organizationId: number): Promise<IDatasetAttributes> {
        try {
            const user = await UserModel.findOne({
                include: [
                    {
                        association: UserModel.associations.organizations,
                    },
                ],
                where: {
                    id: userId,
                    [Op.or]: [
                        {
                            role: UserRole.superadmin,
                        },
                        {
                            "$organizations.id$": organizationId,
                        },
                    ],
                },
            });
            const org = await OrganizationModel.findByPk(organizationId);

            if (!user || !org) {
                throw new CustomError(
                    "User has no permission to create Dataset for this Organization",
                    true,
                    this.constructor.name,
                    403
                );
            }

            const newdataset = await DatasetModel.create({
                id: v4(),
                organizationId,
                status: DatasetStatus.created,
                userId,
            } as IDatasetAttributes);

            const dataset = await DatasetModel.findOne({
                include: [DatasetModel.associations.organization],
                where: {
                    id: newdataset.id,
                },
            });

            await logActivity({
                scope: "Dataset",
                action: "create",
                userId,
                meta: {
                    datasetId: newdataset.id,
                },
            });

            return (dataset?.toJSON ? dataset.toJSON() : dataset) as IDatasetAttributes;
        } catch (err) {
            if (err instanceof CustomError) {
                throw err;
            } else {
                throw new CustomError("Error while creating new Dataset", true, this.constructor.name, 500, err);
            }
        }
    }

    /**
     * Updates dataset. In this case is enabled only status update. User id is used for authorization
     *
     * @param {string} datasetId UUID
     * @param {"published" | "unpublished"} status
     * @param {number | undefined} userId
     * @param {number} currentUserId
     * @returns {IDatasetAttributes}
     */
    public async updateDataset(
        datasetId: string,
        status: "published" | "unpublished",
        userId: number | undefined,
        currentUserId: number
    ): Promise<IDatasetAttributes> {
        try {
            const user = await findCurrentUserOrThrow(currentUserId, {
                include: [UserModel.associations.organizations],
            });

            const attributes: FindOptions<IDatasetAttributes>["attributes"] = {
                include: [
                    [literal("data -> 'název' ->> 'cs'"), "name"],
                    [literal("data -> 'klíčové_slovo' -> 'cs'"), "keywords"],
                ],
            };

            let where: FindOptions<IDatasetAttributes>["where"] = {
                id: datasetId,
                status: {
                    [Op.ne]: DatasetStatus.deleted,
                },
            };

            if (user?.role !== UserRole.superadmin) {
                if (userId !== undefined) {
                    throw new CustomError(`Only superadmin can update userId`, true, this.constructor.name, 403);
                }

                where = {
                    ...where,
                    organizationId: {
                        [Op.in]: user?.organizations?.map((o) => o.id),
                    },
                };

                if (user?.role === UserRole.user) {
                    where = {
                        ...where,
                        userId: user?.id,
                    };
                }
            }

            const dataset = await DatasetModel.findOne({
                attributes,
                include: [DatasetModel.associations.organization],
                where,
            });

            if (!dataset) {
                throw new CustomError(`Dataset with id ${datasetId} was not found`, true, this.constructor.name, 404);
            }

            if (dataset.status === DatasetStatus.created) {
                throw new CustomError(`Dataset status cannot be changed`, true, this.constructor.name, 409);
            }

            switch (status) {
                case DatasetStatus.published:
                    if (dataset.status !== DatasetStatus.published) {
                        await this.connectionStrategy.uploadDataset(dataset?.data);
                    }
                    break;
                case DatasetStatus.unpublished:
                    if (dataset.status !== DatasetStatus.unpublished) {
                        await this.connectionStrategy.removeDatasetCascade(dataset?.data?.iri || "");
                    }
                    break;
                default:
                    break;
            }

            // data validation
            // only for easiest way to update validationResult for existing datasets
            if (!dataset.validationResult && dataset.data) {
                dataset.validationResult = dataValidator.validate(dataset.data);
                dataset.changed("validationResult", true); // force update column validationResult
            }
            // /data validation

            const previousStatus = dataset.status;
            dataset.status = DatasetStatus[status];
            if (userId !== undefined) {
                dataset.userId = userId;
            }
            dataset.updatedAt = new Date();
            await dataset.save();

            await logActivity({
                scope: "Dataset",
                action: "update",
                userId: currentUserId,
                meta: {
                    datasetId,
                    previousStatus,
                    currentStatus: dataset.status,
                    userId: userId,
                },
            });

            return (dataset.toJSON ? dataset.toJSON() : dataset) as IDatasetAttributes;
        } catch (err) {
            if (err instanceof CustomError) {
                throw err;
            } else if (err instanceof ForeignKeyConstraintError) {
                throw new CustomError("The provided userId does not exist", true, this.constructor.name, 400, err);
            } else {
                throw new CustomError("Error while updating Dataset", true, this.constructor.name, 500, err);
            }
        }
    }

    /**
     * Deletes dataset. Soft delete is used (dataset status is set to `deleted`). User id is used for authorization
     *
     * @param {string} datasetId UUID
     * @param {number} userId
     */
    public async deleteDataset(datasetId: string, userId: number): Promise<void> {
        try {
            const user = await findCurrentUserOrThrow(userId, {
                include: [UserModel.associations.organizations],
            });

            let where: FindOptions<IDatasetAttributes>["where"] = {
                id: datasetId,
                status: {
                    [Op.ne]: DatasetStatus.deleted,
                },
            };

            if (user?.role !== UserRole.superadmin) {
                where = {
                    ...where,
                    organizationId: {
                        [Op.in]: user?.organizations?.map((o) => o.id),
                    },
                };

                if (user?.role === UserRole.user) {
                    where = {
                        ...where,
                        userId: user?.id,
                    };
                }
            }

            const dataset = await DatasetModel.findOne({
                where,
            });

            if (!dataset) {
                throw new CustomError(`Dataset with id ${datasetId} was not found`, true, this.constructor.name, 404);
            }

            if (dataset.status === DatasetStatus.published) {
                await this.connectionStrategy.removeDatasetCascade(dataset?.data?.iri || "");
            }

            dataset.status = DatasetStatus.deleted;
            dataset.updatedAt = new Date();
            await dataset.save();

            await logActivity({
                scope: "Dataset",
                action: "delete",
                userId,
                meta: {
                    datasetId,
                },
            });
        } catch (err) {
            if (err instanceof CustomError) {
                throw err;
            } else {
                throw new CustomError("Error while Dataset by ID", true, this.constructor.name, 500, err);
            }
        }
    }
}
