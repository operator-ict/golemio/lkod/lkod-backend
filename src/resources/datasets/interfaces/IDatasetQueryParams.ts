export interface IDatasetFilter {
    filter?: {
        organizationId?: number;
        searchString?: string;
        publisherIri?: string;
        themeIris?: string[];
        keywords?: string[];
        formatIris?: string[];
        status?: string[];
    };
    limit?: number;
    offset?: number;
}

export interface IDatasetQueryParams {
    organizationId?: string;
    searchString?: string;
    publisherIri?: string;
    themeIris?: string | string[];
    keywords?: string | string[];
    formatIris?: string | string[];
    status?: string;
    limit?: string;
    offset?: string;
}
