import { CustomError } from "@golemio/core/dist/shared/golemio-errors";
import { IFile, Storage } from "../../core/storage/Storage";
import { DatasetModel } from "../../models/DatasetModel";
import { logActivity } from "../../core/helpers";

/**
 * Dataset Files Controller class
 */
export class DatasetFilesController {
    private storage: Storage;

    constructor() {
        this.storage = new Storage();
    }

    /**
     * List all files
     *
     * @param {string} datasetId
     * @returns {Promise<string[]>}
     */
    public async listFiles(datasetId: string): Promise<string[]> {
        let path = datasetId;
        try {
            const dataset = await DatasetModel.findByPk(datasetId, {
                include: [DatasetModel.associations.organization],
            });
            if (!dataset) {
                throw new Error("Dataset was not found.");
            }
            path = `${dataset.organization.slug}/${datasetId}`;
            return await this.storage.list(path);
        } catch (err) {
            if (err instanceof CustomError) {
                throw err;
            } else {
                throw new CustomError(`Error while listing files in ${path}`, true, this.constructor.name, 503, err);
            }
        }
    }

    /**
     * Upload file
     *
     * @param {string} datasetId
     * @param {object} file
     * @param {string} [file.name] name of file
     * @param {Readable} [file.data] content data
     * @param {number} userId
     * @returns {Promise<void>}
     */
    public async uploadFile(datasetId: string, file: IFile, userId: number): Promise<string> {
        let path = datasetId;
        try {
            const dataset = await DatasetModel.findByPk(datasetId, {
                include: [DatasetModel.associations.organization],
            });
            if (!dataset) {
                throw new Error("Dataset was not found.");
            }

            path = `${dataset.organization.slug}/${datasetId}`;
            const result = await this.storage.upload(path, file);

            await logActivity({
                scope: "DatasetFiles",
                action: "upload",
                userId,
                meta: {
                    datasetId: dataset.id,
                    filename: file.name,
                },
            });

            return result;
        } catch (err) {
            if (err instanceof CustomError) {
                throw err;
            } else {
                throw new CustomError(`Error while uploading file to ${path}`, true, this.constructor.name, 500, err);
            }
        }
    }

    /**
     * Delete file
     *
     * @param {string} datasetId
     * @param {string} filename
     * @param {number} userId
     * @returns {Promise<void>}
     */
    public async deleteFile(datasetId: string, filename: string, userId: number): Promise<void> {
        let path = datasetId;
        try {
            const dataset = await DatasetModel.findByPk(datasetId, {
                include: [DatasetModel.associations.organization],
            });
            if (!dataset) {
                throw new Error("Dataset was not found.");
            }

            path = `${dataset.organization.slug}/${datasetId}`;
            const result = await this.storage.delete(path, filename);

            await logActivity({
                scope: "DatasetFiles",
                action: "delete",
                userId,
                meta: {
                    datasetId: dataset.id,
                    filename,
                },
            });

            return result;
        } catch (err) {
            if (err instanceof CustomError) {
                throw err;
            } else {
                throw new CustomError(`Error while deleting file from ${path}`, true, this.constructor.name, 500, err);
            }
        }
    }
}
