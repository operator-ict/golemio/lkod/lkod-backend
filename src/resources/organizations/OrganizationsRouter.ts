import { NextFunction, Response, Router } from "express";
import { AuthenticationMiddleware, IExtendedRequest } from "../../core/middlewares";
import { BaseRouter } from "../../core/routers/BaseRouter";
import { OrganizationsController } from "./";
import { body, param } from "express-validator";
import { CustomError } from "@golemio/core/dist/shared/golemio-errors";

/**
 * Organiations Router class
 */
export class OrganizationsRouter extends BaseRouter {
    // Assign router to the express.Router() instance
    public router: Router = Router();
    public controller: OrganizationsController;

    constructor() {
        super();
        this.controller = new OrganizationsController();
        this.initRoutes();
    }

    /**
     * Initiates all routes. Should respond with correct data to a HTTP requests to all routes.
     */
    private initRoutes = (): void => {
        this.router.get("/", new AuthenticationMiddleware().authenticate, this.getAllOrganizations);
        this.router.post(
            "/",
            [
                body("name").notEmpty(),
                body("identificationNumber").notEmpty().isNumeric(),
                body("slug").notEmpty().isAlphanumeric("en-US", { ignore: "-" }),
                body("logo").optional({ checkFalsy: true }).isURL(),
                body("arcGisFeed").optional({ checkFalsy: true }).isURL(),
            ],
            this.handleValidationError,
            new AuthenticationMiddleware().authenticate,
            this.createNewOrganization
        );
        this.router.patch(
            "/:organizationId",
            [
                param("organizationId").notEmpty().isNumeric(),
                body("name").optional().notEmpty(),
                body("identificationNumber").optional().isNumeric(),
                body("slug").optional().notEmpty().isAlphanumeric("en-US", { ignore: "-" }),
                body("logo").optional({ checkFalsy: true }).isURL(),
                body("arcGisFeed").optional({ checkFalsy: true }).isURL(),
            ],
            this.handleValidationError,
            new AuthenticationMiddleware().authenticate,
            this.updateOrganization
        );
        this.router.delete(
            "/:organizationId/",
            [param("organizationId").optional().notEmpty().isNumeric()],
            this.handleValidationError,
            new AuthenticationMiddleware().authenticate,
            this.deleteOrganization
        );
        this.router.get(
            "/:organizationId/members/",
            [param("organizationId").notEmpty().isNumeric()],
            this.handleValidationError,
            new AuthenticationMiddleware().authenticate,
            this.getUsersInOrganization
        );
        this.router.post(
            "/:organizationId/members/:userId",
            [param("organizationId").optional().notEmpty().isNumeric(), param("userId").optional().notEmpty().isNumeric()],
            this.handleValidationError,
            new AuthenticationMiddleware().authenticate,
            this.addUserToOrganization
        );
        this.router.delete(
            "/:organizationId/members/:userId",
            [param("organizationId").optional().notEmpty().isNumeric(), param("userId").optional().notEmpty().isNumeric()],
            this.handleValidationError,
            new AuthenticationMiddleware().authenticate,
            this.removeUserFromOrganization
        );
    };

    /**
     * Getting organizations
     *
     * @param {Request} req
     * @param {Response} res
     * @param {NextFunction} next
     * @returns {Promise<void>}
     */
    private getAllOrganizations = async (req: IExtendedRequest, res: Response, next: NextFunction): Promise<void> => {
        try {
            const organizations = await this.controller.getAllOrganizations(req.decoded?.id || 0);
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(200).json(organizations);
        } catch (err) {
            next(err);
        }
    };

    /**
     * Creating new organization
     *
     * @param {IExtendedRequest} req
     * @param {Response} res
     * @param {NextFunction} next
     * @returns {Promise<void>}
     */
    private createNewOrganization = async (req: IExtendedRequest, res: Response, next: NextFunction): Promise<void> => {
        try {
            const dataset = await this.controller.createNewOrganization(
                req.decoded?.id || 0,
                req.body.name,
                req.body.identificationNumber,
                req.body.slug,
                req.body.logo,
                req.body.description,
                req.body.arcGisFeed ? req.body.arcGisFeed : null
            );
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(201).json(dataset);
        } catch (err) {
            next(err);
        }
    };

    /**
     * Updating organization
     *
     * @param {IExtendedRequest} req
     * @param {Response} res
     * @param {NextFunction} next
     * @returns {Promise<void>}
     */
    private updateOrganization = async (req: IExtendedRequest, res: Response, next: NextFunction): Promise<void> => {
        try {
            const organizationId = +req.params.organizationId;
            if (!organizationId) {
                next(new CustomError("Invalid ID", true, this.constructor.name, 400));
                return;
            }

            const dataset = await this.controller.updateOrganization(
                +req.params.organizationId,
                req.body.name,
                req.body.identificationNumber,
                req.body.slug,
                req.body.logo,
                req.body.description,
                typeof req.body.arcGisFeed != undefined && req.body.arcGisFeed ? req.body.arcGisFeed : null,
                req.decoded?.id || 0
            );
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(200).json(dataset);
        } catch (err) {
            next(err);
        }
    };

    /**
     * Deleting organization
     *
     * @param {IExtendedRequest} req
     * @param {Response} res
     * @param {NextFunction} next
     * @returns {Promise<void>}
     */
    private deleteOrganization = async (req: IExtendedRequest, res: Response, next: NextFunction): Promise<void> => {
        try {
            await this.controller.deleteOrganization(req.params.organizationId, req.decoded?.id || 0);
            res.sendStatus(204);
        } catch (err) {
            next(err);
        }
    };

    /**
     * Adding user into an organization
     *
     * @param {IExtendedRequest} req
     * @param {Response} res
     * @param {NextFunction} next
     * @returns {Promise<void>}
     */
    private addUserToOrganization = async (req: IExtendedRequest, res: Response, next: NextFunction): Promise<void> => {
        try {
            const organizationId = +req.params.organizationId;
            const userId = +req.params.userId;
            const currentUserId = req.decoded?.id || 0;
            await this.controller.addUserToOrganization(organizationId, userId, currentUserId);
            res.status(204).send();
        } catch (err) {
            next(err);
        }
    };

    /**
     * Removing user from an organization
     *
     * @param {IExtendedRequest} req
     * @param {Response} res
     * @param {NextFunction} next
     * @returns {Promise<void>}
     */
    private removeUserFromOrganization = async (req: IExtendedRequest, res: Response, next: NextFunction): Promise<void> => {
        try {
            const organizationId = +req.params.organizationId;
            const userId = +req.params.userId;
            const currentUserId = req.decoded?.id || 0;
            await this.controller.removeUserFromOrganization(organizationId, userId, currentUserId);
            res.status(204).send();
        } catch (err) {
            next(err);
        }
    };

    /**
     * Returning organization members
     *
     * @param {IExtendedRequest} req
     * @param {Response} res
     * @param {NextFunction} next
     * @returns {Promise<void>}
     */
    private getUsersInOrganization = async (req: IExtendedRequest, res: Response, next: NextFunction): Promise<void> => {
        try {
            const organizationId = +req.params.organizationId;
            const userId = req.decoded?.id || 0;
            const members = await this.controller.getUsersInOrganization(organizationId, userId);
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(200).json(members);
        } catch (err) {
            next(err);
        }
    };
}
