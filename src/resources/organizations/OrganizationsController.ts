import { CustomError } from "@golemio/core/dist/shared/golemio-errors";
import { ForeignKeyConstraintError, literal, Op, ProjectionAlias, ValidationError } from "sequelize";
import { IOrganizationAttributes, OrganizationModel } from "../../models/OrganizationModel";
import { logActivity } from "../../core/helpers";
import { IUserAttributes, UserModel, UserRole } from "../../models/UserModel";
import { findCurrentUserOrThrow } from "../../core/helpers/SessionUserHelper";

/**
 * Organization Controller class
 */
export class OrganizationsController {
    constructor() {
        //
    }

    /**
     * Returns all user's organizations
     *
     * @param {number} userId
     * @returns {Promise<IOrganizationAttributes[]>}
     */
    public async getAllOrganizations(userId: number): Promise<IOrganizationAttributes[]> {
        try {
            const user = await findCurrentUserOrThrow(userId);
            const include: ProjectionAlias[] = [[literal('"arcGisFeed" IS NOT NULL'), "hasArcGisFeed"]];

            if (user?.role == UserRole.superadmin) {
                return await OrganizationModel.findAll({ attributes: { include } });
            }

            return await OrganizationModel.findAll({
                attributes: {
                    exclude: ["arcGisFeed"],
                    include: include,
                },
                include: [
                    {
                        association: OrganizationModel.associations.users,
                        attributes: [],
                        through: { attributes: [] },
                        where: {
                            id: userId,
                        },
                    },
                ],
            });
        } catch (err) {
            throw new CustomError("Error while getting all Organizations", true, this.constructor.name, 500, err);
        }
    }

    /**
     * Creates new organization. User id is used for authorization
     *
     * @param {number} userId
     * @param {number} name
     * @param {number} identificationNumber
     * @param {number} slug
     * @param {number} logo
     * @param {number} description
     * @param {number} arcGisFeed
     * @returns {Promise<IOrganizationAttributes[]>}
     */
    async createNewOrganization(
        userId: number,
        name: string,
        identificationNumber: string,
        slug: string,
        logo: string | null,
        description: string | null,
        arcGisFeed: string | null
    ): Promise<IOrganizationAttributes> {
        try {
            const user = await UserModel.findOne({
                where: {
                    id: userId,
                    role: UserRole.superadmin,
                },
            });
            if (!user) {
                throw new CustomError("User has no permission to create Organization", true, this.constructor.name, 403);
            }

            const neworganization = await OrganizationModel.create({
                name,
                identificationNumber,
                slug,
                logo,
                description,
                arcGisFeed,
            });
            await neworganization.addUser(user);

            await logActivity({
                scope: "Organization",
                action: "create",
                userId,
                meta: {
                    organizationId: neworganization.id,
                },
            });

            return (neworganization?.toJSON ? neworganization.toJSON() : neworganization) as IOrganizationAttributes;
        } catch (err) {
            if (err instanceof CustomError) {
                throw err;
            } else if (err instanceof ValidationError) {
                throw new CustomError("Duplicate slug while creating Organization", true, this.constructor.name, 409, err);
            } else {
                throw new CustomError("Error while creating new Organization", true, this.constructor.name, 500, err);
            }
        }
    }

    /**
     * Updates organization. User id is used for authorization
     *
     * @param {string} organizationId number
     * @param {number} name
     * @param {number} identificationNumber
     * @param {number} slug
     * @param {number} logo
     * @param {number} description
     * @param {number} arcGisFeed
     * @param {number} userId
     * @returns {IDatasetAttributes}
     */
    public async updateOrganization(
        organizationId: number,
        name: string | undefined,
        identificationNumber: string | undefined,
        slug: string | undefined,
        logo: string | null | undefined,
        description: string | null | undefined,
        arcGisFeed: string | null | undefined,
        userId: number
    ): Promise<IOrganizationAttributes> {
        try {
            const user = await UserModel.findOne({
                include: [
                    {
                        association: UserModel.associations.organizations,
                    },
                ],
                where: {
                    id: userId,
                    [Op.or]: [
                        {
                            role: UserRole.superadmin,
                        },
                        {
                            "$organizations.id$": organizationId,
                        },
                    ],
                },
            });
            if (!user) {
                throw new CustomError("User has no permission to update Organization", true, this.constructor.name, 403);
            }

            const values = { name, identificationNumber, slug, logo, description, arcGisFeed };
            const [_, [organization]] = await OrganizationModel.update(values, {
                where: { id: organizationId },
                returning: true,
            });

            if (!organization) {
                throw new CustomError(`Organization with id ${organizationId} was not found`, true, this.constructor.name, 404);
            }

            await logActivity({
                scope: "Organization",
                action: "update",
                userId,
                meta: {
                    ...values,
                    organizationId,
                },
            });

            return (organization.toJSON ? organization.toJSON() : organization) as IOrganizationAttributes;
        } catch (err) {
            if (err instanceof CustomError) {
                throw err;
            } else if (err instanceof ValidationError) {
                throw new CustomError("Duplicate slug while updating Organization", true, this.constructor.name, 409, err);
            } else {
                throw new CustomError("Error while updating Organization", true, this.constructor.name, 500, err);
            }
        }
    }

    /**
     * Deletes organization. User id is used for authorization
     *
     * @param {string} organizationId UUID
     * @param {number} userId
     */
    public async deleteOrganization(organizationId: string, userId: number): Promise<void> {
        try {
            const user = await UserModel.findOne({
                where: {
                    id: userId,
                    role: UserRole.superadmin,
                },
            });
            if (!user) {
                throw new CustomError("User has no permission to create Organization", true, this.constructor.name, 403);
            }

            const organization = await OrganizationModel.findByPk(organizationId);
            if (!organization) {
                throw new CustomError(`Organization with id ${organizationId} was not found`, true, this.constructor.name, 404);
            }

            await organization.destroy();

            await logActivity({
                scope: "Organization",
                action: "delete",
                userId,
                meta: {
                    organizationId,
                },
            });
        } catch (err) {
            if (err instanceof CustomError) {
                throw err;
            } else {
                throw new CustomError("Error while deleting Organization", true, this.constructor.name, 500, err);
            }
        }
    }

    /**
     * Adds user to organization. User id is used for authorization
     *
     * @param {number} organizationId
     * @param {number} userId
     * @param {number} currentUserId
     */
    async addUserToOrganization(organizationId: number, userId: number, currentUserId: number): Promise<void> {
        try {
            const organization = await this.findOrganizationForEditing(organizationId, currentUserId);
            if (organization.users?.some((u) => u.id === userId)) {
                throw new CustomError(
                    `User ${userId} is already a member of organization ${organizationId}`,
                    true,
                    this.constructor.name,
                    409
                );
            }
            await organization.addUser(userId);
        } catch (err) {
            if (err instanceof CustomError) {
                throw err;
            } else if (err instanceof ForeignKeyConstraintError) {
                throw new CustomError("User not found while adding Organization member", true, this.constructor.name, 404, err);
            } else {
                throw new CustomError("Error while adding user to Organization", true, this.constructor.name, 500, err);
            }
        }
    }

    /**
     * Adds user to organization. User id is used for authorization
     *
     * @param {number} organizationId
     * @param {number} userId
     * @param {number} currentUserId
     */
    async removeUserFromOrganization(organizationId: number, userId: number, currentUserId: number): Promise<void> {
        try {
            const organization = await this.findOrganizationForEditing(organizationId, currentUserId);
            if (!organization.users?.some((u) => u.id === userId)) {
                throw new CustomError(
                    `User ${userId} is not a member of organization ${organizationId}`,
                    true,
                    this.constructor.name,
                    404
                );
            }
            await organization.removeUser(userId);
        } catch (err) {
            if (err instanceof CustomError) {
                throw err;
            } else if (err instanceof ForeignKeyConstraintError) {
                throw new CustomError("User not found while adding Organization member", true, this.constructor.name, 404, err);
            } else {
                throw new CustomError("Error while adding user to Organization", true, this.constructor.name, 500, err);
            }
        }
    }

    private async findOrganizationForEditing(organizationId: number, currentUserId: number): Promise<OrganizationModel> {
        const user = await UserModel.findOne({
            where: {
                id: currentUserId,
                role: UserRole.superadmin,
            },
        });
        if (!user) {
            throw new CustomError("User has no permission to add user to Organization", true, this.constructor.name, 403);
        }

        const organization = await OrganizationModel.findByPk(organizationId, {
            include: [{ association: OrganizationModel.associations.users }],
        });

        if (!organization) {
            throw new CustomError(`Organization with id ${organizationId} was not found`, true, this.constructor.name, 404);
        }
        return organization;
    }

    /**
     * Returns users in organization. User id is used for authorization
     *
     * @param {number} organizationId
     * @param {number} userId
     */
    async getUsersInOrganization(organizationId: number, userId: number): Promise<IUserAttributes[]> {
        try {
            const user = await UserModel.findOne({
                where: {
                    id: userId,
                    role: UserRole.superadmin,
                },
            });
            if (!user) {
                throw new CustomError("User has no permission to get Organization members", true, this.constructor.name, 403);
            }

            const organization = await OrganizationModel.findByPk(organizationId, {
                include: [
                    {
                        association: OrganizationModel.associations.users,
                        attributes: { exclude: ["createdAt", "password", "updatedAt"] },
                    },
                ],
            });

            if (!organization) {
                throw new CustomError(`Organization with id ${organizationId} was not found`, true, this.constructor.name, 404);
            }

            return organization?.users ?? [];
        } catch (err) {
            if (err instanceof CustomError) {
                throw err;
            } else if (err instanceof ForeignKeyConstraintError) {
                throw new CustomError("User not found while adding Organization member", true, this.constructor.name, 404, err);
            } else {
                throw new CustomError("Error while adding user to Organization", true, this.constructor.name, 500, err);
            }
        }
    }
}
