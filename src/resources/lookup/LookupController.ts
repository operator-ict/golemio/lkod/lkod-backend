import { NextFunction, Response } from "express";
import { IDatasetQueryOptions } from "../../core/helpers/LookupQuery/interfaces/IDatasetQueryOptions";
import LookupFacade from "../../core/helpers/LookupQuery/LookupFacade";
import { IExtendedRequest } from "../../core/middlewares";
import QueryHelper from "../helpers/QueryHelper";

export interface IDatasetLookupQueryParams {
    organizationId?: number;
    status?: string;
    publisherIri?: string;
    themeIris?: string | string[];
    keywords?: string | string[];
    formatIris?: string | string[];
}

export class LookupController {
    private lookupHelper: LookupFacade;

    constructor() {
        this.lookupHelper = new LookupFacade();
    }

    public getDatasetThemes = async (
        req: IExtendedRequest<unknown, unknown, unknown, IDatasetLookupQueryParams>,
        res: Response,
        next: NextFunction
    ): Promise<void> => {
        try {
            const filter = this.getFilters(req);
            const dataset = await this.lookupHelper.getThemes(req.decoded?.id || 0, filter);

            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(200).json(dataset);
        } catch (err) {
            next(err);
        }
    };

    public getFormats = async (
        req: IExtendedRequest<unknown, unknown, unknown, IDatasetLookupQueryParams>,
        res: Response,
        next: NextFunction
    ): Promise<void> => {
        try {
            const filter = this.getFilters(req);
            const dataset = await this.lookupHelper.getFormats(req.decoded?.id || 0, filter);

            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(200).json(dataset);
        } catch (err) {
            next(err);
        }
    };

    public getKeywords = async (req: IExtendedRequest, res: Response, next: NextFunction): Promise<void> => {
        try {
            const filter = this.getFilters(req);
            const dataset = await this.lookupHelper.getKeywords(req.decoded?.id || 0, filter);

            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(200).json(dataset);
        } catch (err) {
            next(err);
        }
    };

    public getPublishers = async (req: IExtendedRequest, res: Response, next: NextFunction): Promise<void> => {
        try {
            const filter = this.getFilters(req);
            const dataset = await this.lookupHelper.getPublishers(req.decoded?.id || 0, filter);

            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(200).json(dataset);
        } catch (err) {
            next(err);
        }
    };

    public getStatuses = async (req: IExtendedRequest, res: Response, next: NextFunction): Promise<void> => {
        try {
            const filter = this.getFilters(req);
            const dataset = await this.lookupHelper.getStatuses(req.decoded?.id || 0, filter);

            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(200).json(dataset);
        } catch (err) {
            next(err);
        }
    };

    private getFilters(req: IExtendedRequest<unknown, unknown, unknown, IDatasetLookupQueryParams>): IDatasetQueryOptions {
        return {
            filter: {
                organizationId: req.query.organizationId ? +req.query.organizationId : req.query.organizationId,
                status: QueryHelper.parseParam(req.query.status),
                publisherIri: req.query.publisherIri,
                themeIris: QueryHelper.parseParam(req.query.themeIris),
                keywords: QueryHelper.parseParam(req.query.keywords),
                formatIris: QueryHelper.parseParam(req.query.formatIris),
            },
        };
    }
}
