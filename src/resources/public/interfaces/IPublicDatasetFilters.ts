export interface IPublicDatasetFilter {
    themeIris: string[];
    formatIris: string[];
    publisherSlug: string[];
    keywords: string[];
    searchText: string | null;
    limit: number;
    offset: number;
}
