import { NextFunction, Response, Router } from "express";
import { oneOf, param, query } from "express-validator";
import { IExtendedRequest } from "../../core/middlewares/AuthenticationMiddleware";
import { BaseRouter } from "../../core/routers/BaseRouter";
import QueryHelper from "../helpers/QueryHelper";
import { PublicController } from "./PublicController";
import { IPublicDatasetFilter } from "./interfaces/IPublicDatasetFilters";

export class PublicRouter extends BaseRouter {
    public router: Router = Router();
    public controller: PublicController;
    private readonly PAGINATION_LIMIT = 100;

    constructor() {
        super();
        this.controller = new PublicController();
        this.initRoutes();
    }

    private initRoutes = (): void => {
        this.router.get(
            "/themes",
            [query("limit").optional().isNumeric(), query("offset").optional().isNumeric()],
            this.handleValidationError,
            this.getThemes
        );
        this.router.get(
            "/organizations",
            [query("limit").optional().isNumeric(), query("offset").optional().isNumeric()],
            this.handleValidationError,
            this.getOrganizations
        );
        this.router.get(
            "/datasets",
            [
                query("publisher_slug").optional().isString().not().isArray(),
                this.isStringOrArrayOfStrings("theme_iris"),
                this.isStringOrArrayOfStrings("format_iris"),
                this.isStringOrArrayOfStrings("keywords"),
                query("search_text").optional().isString().not().isArray(),
                query("limit").optional().isNumeric().not().isArray(),
                query("offset").optional().isNumeric().not().isArray(),
            ],
            this.handleValidationError,
            this.getDatasets
        );
        this.router.get(
            "/datasets/:id",
            [param("id").isUUID(4).not().isEmpty().not().isArray()],
            this.handleValidationError,
            this.getDatasetDetail
        );
    };

    private getThemes = async (req: IExtendedRequest, res: Response, next: NextFunction): Promise<void> => {
        try {
            const filter = {
                limit: req.query.limit ? parseInt(req.query.limit as string, 10) : this.PAGINATION_LIMIT,
                offset: req.query.offset ? parseInt(req.query.offset as string, 10) : 0,
            };
            const organizations = await this.controller.getThemes(filter);

            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(200).json(organizations);
        } catch (err) {
            next(err);
        }
    };

    public getDatasets = async (req: IExtendedRequest, res: Response, next: NextFunction): Promise<void> => {
        try {
            const filter: IPublicDatasetFilter = {
                publisherSlug: req.query.publisher_slug
                    ? QueryHelper.parseParam(req.query.publisher_slug as string | string[])!
                    : [],
                themeIris: req.query.theme_iris ? QueryHelper.parseParam(req.query.theme_iris as string | string[])! : [],
                formatIris: req.query.format_iris ? QueryHelper.parseParam(req.query.format_iris as string | string[])! : [],
                keywords: req.query.keywords ? QueryHelper.parseParam(req.query.keywords as string | string[])! : [],
                limit: req.query.limit ? parseInt(req.query.limit as string, 10) : this.PAGINATION_LIMIT,
                offset: req.query.offset ? parseInt(req.query.offset as string, 10) : 0,
                searchText: req.query.search_text ? (req.query.search_text as string) : null,
            };

            const datasets = await this.controller.getDatasets(filter);
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(200).json(datasets);
        } catch (err) {
            next(err);
        }
    };

    public getDatasetDetail = async (req: IExtendedRequest, res: Response, next: NextFunction): Promise<void> => {
        try {
            const dataset = await this.controller.getDatasetDetail(req.params.id);
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(200).json(dataset);
        } catch (err) {
            next(err);
        }
    };

    public getOrganizations = async (req: IExtendedRequest, res: Response, next: NextFunction): Promise<void> => {
        try {
            const filter = {
                limit: req.query.limit ? parseInt(req.query.limit as string, 10) : this.PAGINATION_LIMIT,
                offset: req.query.offset ? parseInt(req.query.offset as string, 10) : 0,
            };
            const dataset = await this.controller.getOrganizations(filter);
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(200).json(dataset);
        } catch (err) {
            next(err);
        }
    };

    private isStringOrArrayOfStrings = (optionalQuery: string) => {
        return oneOf([
            query(optionalQuery).not().exists(),
            query(optionalQuery).isString().not().isArray(),
            [query(optionalQuery).isArray(), query(optionalQuery + ".*").isString()],
        ]);
    };
}
