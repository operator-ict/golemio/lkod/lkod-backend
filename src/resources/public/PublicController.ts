import { CustomError } from "@golemio/core/dist/shared/golemio-errors";
import { config } from "../../core/config";
import { postgresConnector } from "../../core/database/PostgresConnector";
import { IFrequencyInfo } from "../../core/datasources/interfaces/IFrequencyInfo";
import { IThemeInfo } from "../../core/datasources/interfaces/IThemeInfo";
import { log } from "../../core/helpers";
import { DatasetModel, DatasetStatus } from "../../models/DatasetModel";
import { DatasetRepository } from "../../models/DatasetRepository";
import { OrganizationModel } from "../../models/OrganizationModel";
import { FileTypeRepository } from "../../models/lookups/FileTypeRepository";
import { FrequencyRepository } from "../../models/lookups/FrequencyRepository";
import { ThemeRepository } from "../../models/lookups/ThemeRepository";
import { IPublicDatasetFilter } from "./interfaces/IPublicDatasetFilters";

export class PublicController {
    private datasetRepository: DatasetRepository;
    private fileTypesRepository: FileTypeRepository;
    private frequencyRepository: FrequencyRepository;
    private themeRepository: ThemeRepository;

    constructor() {
        this.datasetRepository = new DatasetRepository(postgresConnector, log);
        this.fileTypesRepository = new FileTypeRepository(postgresConnector, log);
        this.frequencyRepository = new FrequencyRepository(postgresConnector, log);
        this.themeRepository = new ThemeRepository(postgresConnector, log);
    }

    public async getOrganizations(filter: { limit: number; offset: number }) {
        try {
            const results = await this.datasetRepository.getOrganizationsWithCount(filter.limit, filter.offset);

            return results.map((element) => {
                return {
                    iri: `${config.linked_data.poskytovatel_url_prefix}${element.slug}`,
                    name: element.name,
                    logo: element.logo,
                    description: element.description,
                    slug: element.slug,
                    count: element.count,
                };
            });
        } catch (err) {
            if (err instanceof CustomError) {
                throw err;
            }
            throw new CustomError(
                "Error while getting organizations " + (err instanceof Error) ? (err as Error).message : "",
                true,
                this.constructor.name,
                500,
                err
            );
        }
    }

    public async getThemes(filter: { limit: number; offset: number }) {
        try {
            const results = await this.datasetRepository.getThemesWithCount(filter.limit, filter.offset);

            return results;
        } catch (err) {
            if (err instanceof CustomError) {
                throw err;
            }
            throw new CustomError(
                "Error while getting themes " + (err instanceof Error) ? (err as Error).message : "",
                true,
                this.constructor.name,
                500,
                err
            );
        }
    }

    public async getDatasets(params: IPublicDatasetFilter) {
        try {
            return this.datasetRepository.getDatasetsBasicInfo(
                params.publisherSlug,
                params.themeIris,
                params.keywords,
                params.formatIris,
                params.limit,
                params.offset,
                params.searchText
            );
        } catch (err) {
            if (err instanceof CustomError) {
                throw err;
            }
            throw new CustomError(
                "Error while getting datasets " + (err instanceof Error) ? (err as Error).message : "",
                true,
                this.constructor.name,
                500,
                err
            );
        }
    }

    public async getDatasetDetail(id: string) {
        try {
            const result = await DatasetModel.findByPk(id);

            if (result && result.status === DatasetStatus.published) {
                const slugRegex = /\/(?<slug>[\w-]+$)/;
                const slug = result.data?.poskytovatel ? slugRegex.exec(result.data?.poskytovatel)?.groups?.slug : null;
                const organization = slug ? await OrganizationModel.findOne({ where: { slug: slug }, raw: true }) : null;
                const themes = result.data?.téma
                    ? (await this.getThemesByIris(result.data?.téma)).map((el) => {
                          return {
                              iri: el.iri,
                              title: el.label,
                          };
                      })
                    : [];
                const accrualPeriodicity = result.data?.periodicita_aktualizace
                    ? await this.getFrequencyByIri(result.data?.periodicita_aktualizace)
                    : null;
                const filetypeMapper = await this.getFileTypeMap();

                const transformedResult = {
                    iri: result.data?.iri,
                    title: result.data?.název?.cs,
                    description: result.data?.popis?.cs,
                    publisher: organization
                        ? {
                              iri: result.data?.poskytovatel,
                              title: organization?.name,
                              logo: organization?.logo,
                              description: organization?.description,
                          }
                        : null,
                    documentation: result.data?.dokumentace,
                    specification: result.data?.specifikace,
                    accrual_periodicity: accrualPeriodicity,
                    temporal_from: result.data?.časové_pokrytí?.začátek,
                    temporal_to: result.data?.časové_pokrytí?.konec,
                    temporal_resolution: result.data?.časové_pokrytí?.typ,
                    spatial_resolution_in_meters: result.data?.prostorové_rozlišení_v_metrech,
                    contact_point: {
                        name: result.data?.kontaktní_bod?.jméno?.cs,
                        email: result.data?.kontaktní_bod["e-mail"],
                    },
                    keywords: result.data?.klíčové_slovo?.cs,
                    themes: themes,
                    eurovoc_themes: result.data?.koncept_euroVoc ? [result.data?.koncept_euroVoc] : null,
                    distributions: result.data?.distribuce.map((distribution: any) => {
                        return {
                            iri: distribution.iri,
                            title: distribution.název?.cs,
                            format: {
                                iri: distribution.formát,
                                title: filetypeMapper[distribution.formát],
                            },
                            media_type: distribution.typ_média,
                            download_url: distribution.soubor_ke_stažení,
                            access_url: distribution.přístupové_url,
                            compress_format: distribution.typ_média_komprese,
                            package_format: distribution.typ_média_balíčku,
                            conforms_to: distribution.schéma,
                            licence: {
                                type: distribution?.podmínky_užití?.typ,
                                author: distribution?.podmínky_užití?.autor?.cs,
                                personal_data: distribution?.podmínky_užití?.osobní_údaje,
                                copyright_licence: distribution?.podmínky_užití?.autorské_dílo,
                                database_copyright_licence: distribution?.podmínky_užití?.databáze_jako_autorské_dílo,
                                database_protected_by_law: distribution?.podmínky_užití?.databáze_chráněná_zvláštními_právy,
                            },
                            access_service: distribution?.přístupová_služba
                                ? {
                                      iri: distribution?.přístupová_služba?.iri,
                                      title: distribution?.přístupová_služba?.název?.cs,
                                      endpoint_url: distribution?.přístupová_služba?.přístupový_bod,
                                      endpoint_description: distribution?.přístupová_služba?.popis_přístupového_bodu,
                                  }
                                : null,
                        };
                    }),
                    spatial: result?.data?.prvek_rúian,
                };

                return transformedResult;
            } else {
                return null;
            }
        } catch (err) {
            if (err instanceof CustomError) {
                throw err;
            }
            throw new CustomError(
                "Error while getting dataset detail " + (err instanceof Error) ? (err as Error).message : "",
                true,
                this.constructor.name,
                500,
                err
            );
        }
    }

    //#region Private Helpers
    private async getFrequencyByIri(iri: string): Promise<IFrequencyInfo | undefined> {
        const result = await this.frequencyRepository.getAll();

        return result.find((element: IFrequencyInfo) => element.iri === iri);
    }

    private async getThemesByIris(iris: string[]): Promise<IThemeInfo[]> {
        const result = await this.themeRepository.getAll();
        const final = result.filter((element: IThemeInfo) => iris.includes(element.iri));

        return final;
    }

    private async getFileTypeMap() {
        const result: Record<string, string> = {};
        for (const element of await this.fileTypesRepository.getAll()) {
            result[element.iri] = element.label;
        }

        return result;
    }
    //#endregion
}
