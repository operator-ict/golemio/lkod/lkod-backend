import { CustomError } from "@golemio/core/dist/shared/golemio-errors";
import { IUserAttributes, UserModel, UserRole } from "../../models/UserModel";
import { logActivity, secretManager } from "../../core/helpers";
import { Op, ValidationError } from "sequelize";
import { AuthController } from "../auth";

const BOT_ACCOUNT_PASSWORD = "(none)";

/**
 * User Controller class
 */
export class UsersController {
    constructor() {
        //
    }

    /**
     * Returns all users
     *
     * @param {number} userId
     * @returns {Promise<IUserAttributes[]>}
     */
    public async getAllUsers(userId: number): Promise<IUserAttributes[]> {
        try {
            const user = await UserModel.findOne({
                where: {
                    id: userId,
                    role: UserRole.superadmin,
                },
            });
            if (!user) {
                throw new CustomError("User has no permission to get Users", true, this.constructor.name, 403);
            }

            return await UserModel.findAll({
                attributes: { exclude: ["createdAt", "password", "updatedAt"] },
                order: [
                    ["name", "ASC"],
                    ["email", "ASC"],
                ],
                where: { password: { [Op.not]: BOT_ACCOUNT_PASSWORD } },
            });
        } catch (err) {
            if (err instanceof CustomError) {
                throw err;
            } else {
                throw new CustomError("Error while getting all Users", true, this.constructor.name, 500, err);
            }
        }
    }

    /**
     * Returns the current user
     *
     * @param {number} userId
     * @returns {Promise<IUserAttributes>}
     */
    public async getCurrentUser(userId: number): Promise<IUserAttributes> {
        try {
            const user = await UserModel.findOne({
                attributes: { exclude: ["createdAt", "password", "updatedAt"] },
                where: {
                    id: userId,
                    role: UserRole.superadmin,
                },
            });
            if (!user) {
                throw new CustomError("User has no permission to get Users", true, this.constructor.name, 403);
            }
            return user;
        } catch (err) {
            if (err instanceof CustomError) {
                throw err;
            } else {
                throw new CustomError("Error while getting current User", true, this.constructor.name, 500, err);
            }
        }
    }

    /**
     * Add new user
     *
     * @param {string} email
     * @param {string} password
     * @param {string} name
     * @param {string} role
     * @param {number} currentUserId
     * @returns {Promise<IUserAttributes>}
     */
    public async createNewUser(
        email: string,
        password: string | undefined,
        name: string,
        role: UserRole,
        currentUserId: number
    ): Promise<IUserAttributes> {
        try {
            const currentUser = await UserModel.findOne({
                where: {
                    id: currentUserId,
                    role: UserRole.superadmin,
                },
            });
            if (!currentUser) {
                throw new CustomError("User has no permission to create User", true, this.constructor.name, 403);
            }

            if (password == BOT_ACCOUNT_PASSWORD) {
                throw new CustomError("This password is reserved", true, this.constructor.name, 400);
            }

            let hashedPassword;
            if (password !== undefined) {
                hashedPassword = await secretManager.hashSecret(password);
            } else {
                hashedPassword = "(unset)";
            }

            const newuser = await UserModel.create({
                email,
                password: hashedPassword,
                name,
                role,
            } as IUserAttributes);

            await logActivity({
                scope: "User",
                action: "create",
                userId: currentUserId,
                meta: {
                    userId: newuser.id,
                },
            });

            if (password === undefined) {
                await new AuthController().forgotPassword(email);
            }

            return (newuser?.toJSON ? newuser.toJSON() : newuser) as IUserAttributes;
        } catch (err) {
            if (err instanceof CustomError) {
                throw err;
            } else if (err instanceof ValidationError) {
                throw new CustomError("Duplicate email while creating User", true, this.constructor.name, 409, err);
            } else {
                throw new CustomError("Error while creating new User", true, this.constructor.name, 500, err);
            }
        }
    }

    /**
     * Updates user. User id is used for authorization
     *
     * @param {number} userId
     * @param {string} email
     * @param {string} name
     * @param {UserRole} role
     * @param {number} currentUserId
     * @returns {IUserAttributes}
     */
    public async updateUser(
        userId: number,
        email: string | undefined,
        name: string | undefined,
        role: UserRole | undefined,
        currentUserId: number
    ): Promise<IUserAttributes> {
        try {
            const currentSuperadminUser = await UserModel.findOne({
                where: {
                    id: currentUserId,
                    role: UserRole.superadmin,
                },
            });
            if (!currentSuperadminUser) {
                throw new CustomError("User has no permission to update User", true, this.constructor.name, 403);
            }

            if (userId === currentUserId) {
                // Prevents situations like the last superadmin removing their own permissions.
                throw new CustomError("User can not update themselves", true, this.constructor.name, 400);
            }

            const values = { email, name, role };
            const [_, [user]] = await UserModel.update(values, {
                where: { id: userId, password: { [Op.not]: BOT_ACCOUNT_PASSWORD } },
                returning: ["id", "email", "name", "role", "hasDefaultPassword"],
            });

            if (!user) {
                throw new CustomError(`User with id ${userId} was not found`, true, this.constructor.name, 404);
            }

            await logActivity({
                scope: "User",
                action: "update",
                userId: currentUserId,
                meta: {
                    ...values,
                    userId,
                },
            });

            return (user.toJSON ? user.toJSON() : user) as IUserAttributes;
        } catch (err) {
            if (err instanceof CustomError) {
                throw err;
            } else if (err instanceof ValidationError) {
                throw new CustomError("Duplicate email while updating User", true, this.constructor.name, 409, err);
            } else {
                throw new CustomError("Error while updating User", true, this.constructor.name, 500, err);
            }
        }
    }

    /**
     * Deletes user. User id is used for authorization
     *
     * @param {number} userId
     * @param {number} currentUserId
     */
    public async deleteUser(userId: number, currentUserId: number): Promise<void> {
        try {
            const currentSuperadminUser = await UserModel.findOne({
                where: {
                    id: currentUserId,
                    role: UserRole.superadmin,
                },
            });
            if (!currentSuperadminUser) {
                throw new CustomError("User has no permission to update User", true, this.constructor.name, 403);
            }

            if (userId === currentUserId) {
                throw new CustomError("User can not delete themselves", true, this.constructor.name, 400);
            }

            const user = await UserModel.findOne({ where: { id: userId, password: { [Op.not]: BOT_ACCOUNT_PASSWORD } } });
            if (!user) {
                throw new CustomError(`User with id ${userId} was not found`, true, this.constructor.name, 404);
            }

            await user.destroy();

            await logActivity({
                scope: "User",
                action: "delete",
                userId: currentUserId,
                meta: {
                    userId,
                },
            });
        } catch (err) {
            if (err instanceof CustomError) {
                throw err;
            } else {
                throw new CustomError("Error while deleting User by ID", true, this.constructor.name, 500, err);
            }
        }
    }
}
