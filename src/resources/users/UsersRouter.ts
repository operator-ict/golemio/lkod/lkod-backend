import { NextFunction, Response, Router } from "express";
import { AuthenticationMiddleware, IExtendedRequest } from "../../core/middlewares";
import { BaseRouter } from "../../core/routers/BaseRouter";
import { UsersController } from "./";
import { body, param } from "express-validator";
import { UserRole } from "../../models/UserModel";

/**
 * Users Router class
 */
export class UsersRouter extends BaseRouter {
    // Assign router to the express.Router() instance
    public router: Router = Router();
    public controller: UsersController;

    constructor() {
        super();
        this.controller = new UsersController();
        this.initRoutes();
    }

    /**
     * Initiates all routes. Should respond with correct data to a HTTP requests to all routes.
     */
    private initRoutes = (): void => {
        this.router.get("/", new AuthenticationMiddleware().authenticate, this.getAllUsers);
        this.router.get("/me", new AuthenticationMiddleware().authenticate, this.getCurrentUser);
        this.router.post(
            "/",
            new AuthenticationMiddleware().authenticate,
            [
                body("email").isEmail().normalizeEmail({
                    gmail_convert_googlemaildotcom: false,
                    gmail_remove_dots: false,
                    gmail_remove_subaddress: false,
                    icloud_remove_subaddress: false,
                    outlookdotcom_remove_subaddress: false,
                    yahoo_remove_subaddress: false,
                }),
                body("password").optional().notEmpty().isString(),
                body("name").notEmpty().isString(),
                body("role").notEmpty().isIn(Object.values(UserRole)),
            ],
            this.handleValidationError,
            this.handleValidationError,
            this.createUser
        );
        this.router.patch(
            "/:userId",
            [
                param("userId").notEmpty().isNumeric(),
                body("email").optional().isEmail().normalizeEmail({
                    gmail_convert_googlemaildotcom: false,
                    gmail_remove_dots: false,
                    gmail_remove_subaddress: false,
                    icloud_remove_subaddress: false,
                    outlookdotcom_remove_subaddress: false,
                    yahoo_remove_subaddress: false,
                }),
                body("name").optional().notEmpty().isString(),
                body("role").optional().notEmpty().isIn(Object.values(UserRole)),
            ],
            this.handleValidationError,
            new AuthenticationMiddleware().authenticate,
            this.updateUser
        );
        this.router.delete(
            "/:userId",
            [param("userId").notEmpty().isNumeric()],
            this.handleValidationError,
            new AuthenticationMiddleware().authenticate,
            this.deleteUser
        );
    };

    /**
     * Getting users
     *
     * @param {Request} req
     * @param {Response} res
     * @param {NextFunction} next
     * @returns {Promise<void>}
     */
    private getAllUsers = async (req: IExtendedRequest, res: Response, next: NextFunction): Promise<void> => {
        try {
            const users = await this.controller.getAllUsers(req.decoded?.id || 0);
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(200).json(users);
        } catch (err) {
            next(err);
        }
    };

    /**
     * Getting current user
     *
     * @param {Request} req
     * @param {Response} res
     * @param {NextFunction} next
     * @returns {Promise<void>}
     */
    private getCurrentUser = async (req: IExtendedRequest, res: Response, next: NextFunction): Promise<void> => {
        try {
            const user = await this.controller.getCurrentUser(req.decoded?.id || 0);
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(200).json(user);
        } catch (err) {
            next(err);
        }
    };

    /**
     * Adding user
     *
     * @param {Request} req
     * @param {Response} res
     * @param {NextFunction} next
     * @returns {Promise<void>}
     */
    private createUser = async (req: IExtendedRequest, res: Response, next: NextFunction): Promise<void> => {
        try {
            const currentUserId = req.decoded?.id || 0;
            const user = await this.controller.createNewUser(
                req.body.email,
                req.body.password,
                req.body.name,
                req.body.role,
                currentUserId
            );
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(201).json(user);
        } catch (err) {
            next(err);
        }
    };

    /**
     * Updating user
     *
     * @param {IExtendedRequest} req
     * @param {Response} res
     * @param {NextFunction} next
     * @returns {Promise<void>}
     */
    private updateUser = async (req: IExtendedRequest, res: Response, next: NextFunction): Promise<void> => {
        try {
            const user = await this.controller.updateUser(
                +req.params.userId,
                req.body.email,
                req.body.name,
                req.body.role,
                req.decoded?.id || 0
            );
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(200).json(user);
        } catch (err) {
            next(err);
        }
    };

    /**
     * Deleting user
     *
     * @param {IExtendedRequest} req
     * @param {Response} res
     * @param {NextFunction} next
     * @returns {Promise<void>}
     */
    private deleteUser = async (req: IExtendedRequest, res: Response, next: NextFunction): Promise<void> => {
        try {
            await this.controller.deleteUser(+req.params.userId, req.decoded?.id || 0);
            res.sendStatus(204);
        } catch (err) {
            next(err);
        }
    };
}
