import { NextFunction, Request, Response, Router } from "express";
import { body } from "express-validator";
import { AuthenticationMiddleware, IExtendedRequest } from "../../core/middlewares";
import { BaseRouter } from "../../core/routers/BaseRouter";
import { AuthController } from "./";

/**
 * Auth Router class
 */
export class AuthRouter extends BaseRouter {
    // Assign router to the express.Router() instance
    public router: Router = Router();
    public controller: AuthController;
    private authMiddleware: AuthenticationMiddleware;

    constructor() {
        super();
        this.controller = new AuthController();
        this.authMiddleware = new AuthenticationMiddleware();
        this.initRoutes();
    }

    /**
     * Initiates all routes. Should respond with correct data to a HTTP requests to all routes.
     */
    private initRoutes = (): void => {
        this.router.post(
            "/login",
            [
                body("email").isEmail().normalizeEmail({
                    gmail_convert_googlemaildotcom: false,
                    gmail_remove_dots: false,
                    gmail_remove_subaddress: false,
                    icloud_remove_subaddress: false,
                    outlookdotcom_remove_subaddress: false,
                    yahoo_remove_subaddress: false,
                }),
                body("password").notEmpty().isString(),
            ],
            this.handleValidationError,
            this.login
        );
        this.router.post("/logout", this.authMiddleware.authenticate, this.logout);
        this.router.post(
            "/change-password",
            [body("currentPassword").notEmpty().isString(), body("newPassword").notEmpty().isString()],
            this.handleValidationError,
            this.authMiddleware.authenticate,
            this.changePassword
        );
        this.router.post(
            "/forgot-password",
            [
                body("email").isEmail().normalizeEmail({
                    gmail_convert_googlemaildotcom: false,
                    gmail_remove_dots: false,
                    gmail_remove_subaddress: false,
                    icloud_remove_subaddress: false,
                    outlookdotcom_remove_subaddress: false,
                    yahoo_remove_subaddress: false,
                }),
            ],
            this.handleValidationError,
            this.forgotPassword
        );
        this.router.post(
            "/reset-password",
            [body("token").notEmpty().isString(), body("password").notEmpty().isString()],
            this.handleValidationError,
            this.resetPassword
        );
    };

    /**
     * Logging in
     *
     * @param {Request} req
     * @param {Response} res
     * @param {NextFunction} next
     * @returns {Promise<void>}
     */
    private login = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            const result = await this.controller.login(req.body.email, req.body.password);
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(200).json(result);
        } catch (err) {
            next(err);
        }
    };

    /**
     * Logging out
     *
     * @param {IExtendedRequest} req
     * @param {Response} res
     * @param {NextFunction} next
     * @returns {Promise<void>}
     */
    private logout = async (req: IExtendedRequest, res: Response, next: NextFunction): Promise<void> => {
        try {
            await this.controller.logout(req.decoded?.jti || "", req.decoded?.exp || undefined);
            res.sendStatus(204);
        } catch (err) {
            next(err);
        }
    };

    /**
     * Change password
     *
     * @param {IExtendedRequest} req
     * @param {Response} res
     * @param {NextFunction} next
     * @returns {Promise<void>}
     */
    private changePassword = async (req: IExtendedRequest, res: Response, next: NextFunction): Promise<void> => {
        try {
            await this.controller.changePassword(req.decoded?.id || 0, req.body.currentPassword, req.body.newPassword);
            res.sendStatus(204);
        } catch (err) {
            next(err);
        }
    };

    /**
     * Forgot password
     *
     * @param {Request} req
     * @param {Response} res
     * @param {NextFunction} next
     * @returns {Promise<void>}
     */
    private forgotPassword = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            await this.controller.forgotPassword(req.body.email);
            res.sendStatus(202);
        } catch (err) {
            next(err);
        }
    };

    /**
     * Reset password
     *
     * @param {Request} req
     * @param {Response} res
     * @param {NextFunction} next
     * @returns {Promise<void>}
     */
    private resetPassword = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            await this.controller.resetPassword(req.body.token, req.body.password);
            res.sendStatus(204);
        } catch (err) {
            next(err);
        }
    };
}
