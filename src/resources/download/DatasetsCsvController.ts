import { CustomError } from "@golemio/core/dist/shared/golemio-errors";
import { format } from "@fast-csv/format";
import { Response } from "express";
import { DownloadRepository } from "../../models/DownloadRepository";

export class DatasetsCsvController {
    private repository: DownloadRepository;

    constructor() {
        this.repository = new DownloadRepository();
    }

    public handleResponse = async (userId: number, response: Response): Promise<void> => {
        try {
            const result = await this.repository.getAllDatasets(userId);
            const csvStream = format({ headers: true, delimiter: ",", quote: '"', writeBOM: true });
            response.setHeader("Content-Type", "text/csv; charset=utf-8");
            csvStream.pipe(response);

            for (const line of result) {
                csvStream.write(line);
            }
            csvStream.end();
        } catch (err) {
            throw new CustomError("Unable to export datasets as csv", true, this.constructor.name, 500, err);
        }
    };
}
