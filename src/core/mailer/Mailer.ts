import * as nunjucks from "nunjucks";
import * as path from "path";
import { config } from "../config";
import { DummyMailService } from "./DummyMail.service";
import { SMTPMailService } from "./SMTPMail.service";

export enum MailerTansportType {
    DUMMY = "DUMMY",
    SMTP = "SMTP",
}

export interface IMailerSendOptions {
    from: string;
    to: string | string[];
    replyTo?: string | string[] | undefined;
    subject: string;
    text?: string | Buffer | undefined;
    html?: string | Buffer | undefined;
}

export interface IMailer {
    send(mailOpts: IMailerSendOptions): Promise<void>;
}

/**
 * Mailer class.
 */
export class Mailer {
    private mailer: IMailer;
    private templateRenderer: nunjucks.Environment;

    private static getMailer() {
        switch (config.mailerType) {
            case MailerTansportType.SMTP:
                return new SMTPMailService(config.smtp);
            case MailerTansportType.DUMMY:
                return new DummyMailService();
            default:
                throw new Error("Invalid mailer transport type");
        }
    }

    constructor() {
        this.mailer = Mailer.getMailer();
        this.templateRenderer = new nunjucks.Environment(
            new nunjucks.FileSystemLoader(path.resolve(`${__dirname}/../../../templates/email/`)),
            { autoescape: true }
        );
    }

    /**
     * Sets mail options with password reset code and processes email sending.
     */
    public async sendPasswordReset(email: string, urlWithCode: string): Promise<void> {
        const options = {
            urlWithCode,
        };

        return this.mailer.send({
            from: config.mailerFrom,
            html: this.templateRenderer.render("html/password_reset.html", { ...options }),
            subject: "Reset zapomenutého hesla",
            text: this.templateRenderer.render("plain/password_reset.txt", { ...options }),
            to: email,
        });
    }
}
