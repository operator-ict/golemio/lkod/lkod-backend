import { CustomError } from "@golemio/core/dist/shared/golemio-errors";
import { UserModel } from "../../models/UserModel";
import { Attributes, FindOptions } from "sequelize/types/model";

export const findCurrentUserOrThrow = async (
    userId: number,
    options?: Omit<FindOptions<Attributes<UserModel>>, "where">
): Promise<UserModel> => {
    const user = await UserModel.findByPk(userId ?? -1, options);
    if (!user) {
        throw new CustomError("User no longer exists", true, findCurrentUserOrThrow.name, 401);
    }
    return user;
};
