import { createLogger, createRequestLogger } from "@golemio/core/dist/helpers";
import { ActivityLogModel, IActivityLogAttributes, IActivityLogInput } from "../../models/ActivityLogModel";
import { config } from "../config";

const logger = createLogger({
    logLevel: config.log_level,
    nodeEnv: config.node_env as string,
    projectName: "golemio-lkod-backend",
});

const getRequestLogger = createRequestLogger(config.node_env as string, logger);

// Activity logger
const logActivity = async (input: IActivityLogInput): Promise<void> => {
    if (config.activity_log_enabled) {
        await ActivityLogModel.create(input as IActivityLogAttributes);
    }
};

export { logger as log, logActivity, getRequestLogger };
