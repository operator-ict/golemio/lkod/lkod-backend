import Ajv2019, { ValidateFunction } from "ajv/dist/2019.js";
import addFormats from "ajv-formats";
import { log } from "./Logger";

const addFormats2019 = require("ajv-formats-draft2019");
const localize = require("ajv-i18n");

export interface IValidationResult {
    status: "valid" | "invalid" | "hasWarnings";
    messages: string[] | null;
}

class DataValidator {
    private ajv: Ajv2019;
    private ajvValidate: ValidateFunction;

    constructor() {
        this.ajv = new Ajv2019({
            allErrors: true,
            unicodeRegExp: false,
            messages: false,
        });
        addFormats(this.ajv);
        addFormats2019(this.ajv);

        let schema = {};
        try {
            schema = require(`../../../config/dataset.schema.json`);
        } catch (err) {
            log.warn(`Error while loading data validation schema. Path /config/dataset.schema.json`);
        }

        this.ajvValidate = this.ajv.compile(schema);
    }

    public validate(data: any): IValidationResult {
        const valid = this.ajvValidate(data);
        const res: IValidationResult = {
            status: "valid",
            messages: null,
        };

        if (!valid) {
            res.status = "hasWarnings";

            for (const error of this.ajvValidate.errors || []) {
                if (error.instancePath === "" && error.keyword === "required") {
                    res.status = "invalid";
                    break;
                }
            }

            localize.cs(this.ajvValidate.errors);
            res.messages = this.ajvValidate.errors?.map((e) => `data${e.instancePath} ${e.message}`) || null;
        }
        return res;
    }
}

const dataValidator = new DataValidator();

export { dataValidator };
