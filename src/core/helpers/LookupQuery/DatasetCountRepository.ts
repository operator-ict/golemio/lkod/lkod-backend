import { FindOptions, fn, literal, WhereOptions } from "sequelize";
import { DatasetModel, IDatasetAttributes } from "../../../models/DatasetModel";
import { UserModel } from "../../../models/UserModel";
import QueryHelper from "../../../resources/helpers/QueryHelper";
import { findCurrentUserOrThrow } from "../SessionUserHelper";
import { IDatasetQueryOptions } from "./interfaces/IDatasetQueryOptions";
import { IListFormat } from "./interfaces/IListFormat";
import { IListKeyword } from "./interfaces/IListKeyword";
import { IListPublisher } from "./interfaces/IListPublisher";
import { IListStatus } from "./interfaces/IListStatus";
import { IListTheme } from "./interfaces/IListTheme";

export class DatasetCountRepository {
    private static instance: DatasetCountRepository;

    private constructor() {}

    public static getInstance = (): DatasetCountRepository => {
        if (!this.instance) {
            this.instance = new DatasetCountRepository();
        }

        return this.instance;
    };

    //#region Public methods
    public getPublishers = async (
        userId: number,
        options: IDatasetQueryOptions
    ): Promise<Array<Omit<IListPublisher, "label">>> => {
        const user = await findCurrentUserOrThrow(userId, {
            include: [UserModel.associations.organizations],
        });
        const attributes: FindOptions<IDatasetAttributes>["attributes"] = [
            [literal(`"data"->>'poskytovatel'`), "iri"],
            [fn("count", 1), "count"],
        ];
        const concatedWhere = QueryHelper.buildWherePart(options, user);
        const result = (await DatasetModel.findAll<any>({
            attributes,
            where: concatedWhere,
            group: "iri",
            raw: true,
            order: [[literal("count"), "DESC"]],
        })) as Array<Omit<IListPublisher, "label">>;

        return result;
    };

    public getThemes = async (userId: number, options: IDatasetQueryOptions): Promise<Array<Omit<IListTheme, "label">>> => {
        const user = await findCurrentUserOrThrow(userId, {
            include: [UserModel.associations.organizations],
        });
        const attributes: FindOptions<IDatasetAttributes>["attributes"] = [
            [literal(`jsonb_array_elements(("data"->>'téma')::jsonb) `), "iri"],
            [fn("count", 1), "count"],
        ];
        const concatedWhere = QueryHelper.buildWherePart(options, user);
        const result = (await DatasetModel.findAll<any>({
            attributes,
            where: concatedWhere,
            group: "iri",
            raw: true,
            order: [[literal("count"), "DESC"]],
        })) as Array<Omit<IListTheme, "label">>;

        return result;
    };

    public getFormats = async (userId: number, options: IDatasetQueryOptions): Promise<Array<Omit<IListFormat, "label">>> => {
        const user = await findCurrentUserOrThrow(userId, {
            include: [UserModel.associations.organizations],
        });
        const attributes: FindOptions<IDatasetAttributes>["attributes"] = [
            [literal(`jsonb_array_elements(("data"->>'distribuce')::jsonb)->>'formát'`), "iri"],
            [literal(`count(distinct id)`), "count"],
        ];
        const concatedWhere = QueryHelper.buildWherePart(options, user);
        const result = (await DatasetModel.findAll<any>({
            attributes,
            where: concatedWhere,
            group: "iri",
            raw: true,
            order: [[literal("count"), "DESC"]],
        })) as Array<Omit<IListFormat, "label">>;

        return result;
    };

    public getKeywords = async (userId: number, options: IDatasetQueryOptions): Promise<IListKeyword[]> => {
        const user = await findCurrentUserOrThrow(userId, {
            include: [UserModel.associations.organizations],
        });
        const concatedWhere = QueryHelper.buildWherePart(options, user);

        return await this.getKeywordsInternal(concatedWhere);
    };

    public getStatuses = async (userId: number, options: IDatasetQueryOptions): Promise<IListStatus[]> => {
        const user = await findCurrentUserOrThrow(userId, {
            include: [UserModel.associations.organizations],
        });
        const attributes: FindOptions<IDatasetAttributes>["attributes"] = [
            ["status", "label"],
            [fn("count", 1), "count"],
        ];
        const concatedWhere = QueryHelper.buildWherePart(options, user);
        const result = (await DatasetModel.findAll<any>({
            attributes,
            where: concatedWhere,
            group: "label",
            raw: true,
            order: [[literal("count"), "DESC"]],
        })) as IListStatus[];

        return result;
    };

    // For lookup purposes only
    public getAllKeywords = async (): Promise<IListKeyword[]> => {
        return this.getKeywordsInternal(undefined);
    };
    //#endregion

    //#region Private helpers
    private async getKeywordsInternal(concatedWhere: WhereOptions<IDatasetAttributes> | undefined): Promise<IListKeyword[]> {
        const attributes: FindOptions<IDatasetAttributes>["attributes"] = [
            [literal(`jsonb_array_elements((("data"->>'klíčové_slovo')::jsonb)->'cs')`), "label"],
            [fn("count", 1), "count"],
        ];

        const result = (await DatasetModel.findAll<any>({
            attributes,
            where: concatedWhere,
            group: "label",
            raw: true,
            order: [[literal("count"), "DESC"]],
        })) as IListKeyword[];
        return result;
    }
    //#endregion
}
