import { config } from "../../config";
import { DatasetCountRepository } from "./DatasetCountRepository";
import { LookupRepository } from "./LookupRepository";
import { SparqlLookupRepository } from "./SparqlLookupRepository";
import { IListFormat } from "./interfaces/IListFormat";
import { IListKeyword } from "./interfaces/IListKeyword";
import { IListPublisher } from "./interfaces/IListPublisher";
import { IListTheme } from "./interfaces/IListTheme";
import { ILookupRepository } from "./interfaces/ILookupRepository";

export enum LookupFilter {
    themes = "themes",
    formats = "formats",
    keywords = "keywords",
    publishers = "publishers",
}

interface ICacheItem {
    lastUpdated: Date;
    lookup: IListFormat[] | IListPublisher[] | IListKeyword[] | IListTheme[] | any[];
}

export default class CachedLookups {
    private static instance: CachedLookups;
    private lookupHelper: ILookupRepository;
    private pgLookupHelper: DatasetCountRepository;
    private cache: Map<LookupFilter, ICacheItem>;
    private cacheExpirationInMinutes: number;

    private constructor() {
        if (config.sparql.enabled) {
            this.lookupHelper = new SparqlLookupRepository();
        } else {
            this.lookupHelper = new LookupRepository();
        }
        this.pgLookupHelper = DatasetCountRepository.getInstance();
        this.cache = new Map();
        this.cacheExpirationInMinutes = config.datasetLookupCacheInMinutes;
    }

    public static getInstance = (): CachedLookups => {
        if (!this.instance) {
            this.instance = new CachedLookups();
        }

        return this.instance;
    };

    public getAllowedValues = async (filter: LookupFilter): Promise<string[]> => {
        await this.checkCache(filter);

        return this.cache.get(filter)!.lookup.map((el) => el.iri ?? el.label);
    };

    public getLookup = async (filter: LookupFilter) => {
        await this.checkCache(filter);

        return this.cache.get(filter)!.lookup;
    };

    private checkCache = async (filter: LookupFilter) => {
        if (
            !this.cache.has(filter) ||
            this.cache.get(filter)!.lastUpdated.valueOf() > this.cacheExpirationInMinutes * 60 * 1000
        ) {
            this.cache.set(filter, { lastUpdated: new Date(), lookup: await this.refreshCache(filter) });
        }
    };

    private refreshCache = async (filter: LookupFilter) => {
        let lookup: Array<Partial<IListFormat | IListPublisher | IListKeyword | IListTheme>>;

        switch (filter) {
            case LookupFilter.formats: {
                lookup = await this.lookupHelper.getFormats();
                break;
            }
            case LookupFilter.keywords: {
                lookup = await this.pgLookupHelper.getAllKeywords();

                break;
            }
            case LookupFilter.publishers: {
                lookup = await this.lookupHelper.getPublishers();
                break;
            }
            default: {
                lookup = await this.lookupHelper.getThemes();
                break;
            }
        }

        return lookup;
    };
}
