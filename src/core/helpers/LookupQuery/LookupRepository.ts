import { OrganizationModel } from "../../../models/OrganizationModel";
import { FileTypeModel } from "../../../models/lookups/FileTypeModel";
import { ThemeModel } from "../../../models/lookups/ThemeModel";
import { config } from "../../config";
import { IListFormat } from "./interfaces/IListFormat";
import { IListPublisher } from "./interfaces/IListPublisher";
import { IListTheme } from "./interfaces/IListTheme";
import { ILookupRepository } from "./interfaces/ILookupRepository";

export class LookupRepository implements ILookupRepository {
    public async getPublishers(): Promise<Array<Partial<IListPublisher>>> {
        const result = await OrganizationModel.findAll({ raw: true });

        return result.map((publisher) => {
            return {
                iri: `${config.linked_data.poskytovatel_url_prefix}${publisher.slug}`,
                label: publisher.name,
            };
        });
    }

    public async getThemes(): Promise<Array<Partial<IListTheme>>> {
        const result = await ThemeModel.findAll({ raw: true });

        return result.map((theme) => {
            return {
                iri: theme.iri,
                label: theme.label,
            };
        });
    }

    public async getFormats(): Promise<Array<Partial<IListFormat>>> {
        const result = await FileTypeModel.findAll({ raw: true });

        return result.map((format) => {
            return {
                iri: format.iri,
                label: format.label,
            };
        });
    }
}
