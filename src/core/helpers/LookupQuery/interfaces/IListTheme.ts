export interface IListTheme {
    iri: string;
    label: string;
    count: string;
}
