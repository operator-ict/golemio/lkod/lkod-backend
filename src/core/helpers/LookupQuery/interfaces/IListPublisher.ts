export interface IListPublisher {
    iri: string;
    label: string;
    count: string;
}
