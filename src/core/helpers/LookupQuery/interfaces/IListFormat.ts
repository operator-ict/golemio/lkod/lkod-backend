export interface IListFormat {
    iri: string;
    label: string;
    count: string;
}
