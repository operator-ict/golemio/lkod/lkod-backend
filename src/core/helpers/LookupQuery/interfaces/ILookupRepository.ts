import { IListFormat } from "./IListFormat";
import { IListKeyword } from "./IListKeyword";
import { IListPublisher } from "./IListPublisher";
import { IListTheme } from "./IListTheme";

export interface ILookupRepository {
    getPublishers(): Promise<Array<Partial<IListPublisher>>>;
    getThemes(): Promise<Array<Partial<IListTheme>>>;
    getFormats(): Promise<Array<Partial<IListFormat>>>;
}
