export interface IListStatus {
    label: string;
    count: string;
}
