import { CustomError } from "@golemio/core/dist/shared/golemio-errors";
import { IEuSparqlResponse } from "./interfaces/IEuSparqlResponse";
import axios from "axios";
import { JSONSchemaType } from "ajv";

export class EuSparqlDataSource<T> {
    private readonly url = "https://publications.europa.eu/webapi/rdf/sparql";
    private readonly format = "application/sparql-results+json";

    constructor() {}

    protected async query(query: string): Promise<IEuSparqlResponse<T>> {
        const url = new URL(this.url);
        url.searchParams.append("query", query);
        url.searchParams.append("format", this.format);

        const response = await axios(url.href, { responseType: "json" });

        if (!response.status.toString().startsWith("2")) {
            throw new CustomError(
                "Failed to fetch data from the EU SPARQL endpoint with status code: " + response.status,
                true,
                this.constructor.name
            );
        }

        return response.data;
    }

    protected getJsonSchemaEnvelope(resultsJsonSchema: JSONSchemaType<T>) {
        return {
            $schema: "http://json-schema.org/draft-04/schema#",
            type: "object",
            properties: {
                head: {
                    type: "object",
                    properties: {
                        link: { type: "array", items: {} },
                        vars: { type: "array", items: { type: "string" } },
                    },
                    required: ["vars"],
                },
                results: {
                    type: "object",
                    properties: {
                        distinct: { type: "boolean" },
                        ordered: { type: "boolean" },
                        bindings: {
                            type: "array",
                            items: resultsJsonSchema,
                        },
                    },
                    required: ["bindings"],
                },
            },
            required: ["head", "results"],
        };
    }
}
