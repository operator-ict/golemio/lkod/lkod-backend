import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { EuSparqlDataSource } from "./EuSparqlDataSource";
import { IThemeResponse } from "./interfaces/IEuSparqlResponse";
import { IThemeInfo } from "./interfaces/IThemeInfo";

export class ThemeDatasource extends EuSparqlDataSource<IThemeResponse> {
    private readonly command = `PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
        PREFIX dc: <http://purl.org/dc/elements/1.1/>
        
        SELECT ?iri ?id ?label 
        WHERE {
          ?iri dc:identifier ?id .
          ?iri skos:prefLabel ?label .  
          ?iri skos:topConceptOf <http://publications.europa.eu/resource/authority/data-theme> .
          FILTER ( LANG(?label) = 'cs' )
        }`;

    constructor() {
        super();
    }

    public async getThemes() {
        const data = await this.query(this.command);
        await new JSONSchemaValidator("EuThemesValidator", this.getJsonSchema()).Validate(data);

        return data.results.bindings.map<IThemeInfo>((item) => {
            return {
                iri: item.iri.value,
                id: item.id.value,
                label: item.label.value,
            };
        });
    }

    private getJsonSchema() {
        return this.getJsonSchemaEnvelope({
            type: "object",
            properties: {
                iri: {
                    type: "object",
                    properties: {
                        type: { type: "string", enum: ["uri"] },
                        value: { type: "string", format: "uri" },
                    },
                    required: ["type", "value"],
                },
                id: {
                    type: "object",
                    properties: {
                        type: { type: "string", enum: ["literal"] },
                        value: { type: "string" },
                    },
                    required: ["type", "value"],
                },
                label: {
                    type: "object",
                    properties: {
                        type: { type: "string", enum: ["literal"] },
                        value: { type: "string" },
                    },
                    additionalProperties: true,
                    required: ["type", "value"],
                },
            },
            required: ["iri", "id", "label"],
        });
    }
}
