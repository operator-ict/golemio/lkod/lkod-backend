import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { EuSparqlDataSource } from "./EuSparqlDataSource";
import { IFrequencyResponse } from "./interfaces/IEuSparqlResponse";
import { IFrequencyInfo } from "./interfaces/IFrequencyInfo";

export class FrequencyDatasource extends EuSparqlDataSource<IFrequencyResponse> {
    private readonly command = `PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
    PREFIX dc: <http://purl.org/dc/elements/1.1/>
    
    SELECT ?iri ?id ?label (GROUP_CONCAT(?altLabel; separator=", ") AS ?altLabels)
    WHERE {
      ?iri dc:identifier ?id .
      ?iri skos:prefLabel ?label .  
      ?iri skos:altLabel ?altLabel.
      ?iri skos:topConceptOf <http://publications.europa.eu/resource/authority/frequency> .
      FILTER ( LANG(?label) = 'cs' && LANG(?altLabel) = 'cs' ) 
    }
    GROUP BY ?iri ?id ?label`;

    constructor() {
        super();
    }

    public async getFrequencies() {
        const data = await this.query(this.command);

        await new JSONSchemaValidator("EuFrequenciesValidator", this.getJsonSchema()).Validate(data);

        return data.results.bindings.map<IFrequencyInfo>((item) => {
            return {
                iri: item.iri.value,
                id: item.id.value,
                label: item.label.value,
                altLabels: item.altLabels.value,
            };
        });
    }

    private getJsonSchema() {
        return this.getJsonSchemaEnvelope({
            type: "object",
            properties: {
                iri: {
                    type: "object",
                    properties: {
                        type: { type: "string", enum: ["uri"] },
                        value: { type: "string", format: "uri" },
                    },
                    required: ["type", "value"],
                },
                id: {
                    type: "object",
                    properties: {
                        type: { type: "string", enum: ["literal"] },
                        value: { type: "string" },
                    },
                    required: ["type", "value"],
                },
                label: {
                    type: "object",
                    properties: {
                        type: { type: "string", enum: ["literal"] },
                        value: { type: "string" },
                    },
                    required: ["type", "value"],
                    additionalProperties: true,
                },
                altLabels: {
                    type: "object",
                    properties: {
                        type: { type: "string", enum: ["literal"] },
                        value: { type: "string" },
                    },
                    required: ["type", "value"],
                },
            },
            required: ["iri", "id", "label"],
        });
    }
}
