export interface IFileTypeInfo {
    iri: string;
    id: string;
    label: string;
}
