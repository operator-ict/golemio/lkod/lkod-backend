export interface IEuSparqlResponse<T> {
    head: {
        link: any[];
        vars: string[];
    };
    results: {
        distinct: boolean;
        ordered: boolean;
        bindings: T[];
    };
}

export interface IThemeResponse {
    iri: {
        type: string;
        value: string;
    };
    id: {
        type: string;
        value: string;
    };
    label: {
        type: string;
        value: string;
    };
}

export interface IFileResponse {
    iri: {
        type: string;
        value: string;
    };
    id: {
        type: string;
        value: string;
    };
    label: {
        type: string;
        value: string;
    };
}

export interface IFrequencyResponse {
    iri: {
        type: string;
        value: string;
    };
    id: {
        type: string;
        value: string;
    };
    label: {
        type: string;
        value: string;
    };
    altLabels: {
        type: string;
        value: string;
    };
}
