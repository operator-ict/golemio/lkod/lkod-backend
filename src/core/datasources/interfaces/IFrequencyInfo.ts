export interface IFrequencyInfo {
    iri: string;
    id: string;
    label: string;
    altLabels: string; // separated by ,
}
