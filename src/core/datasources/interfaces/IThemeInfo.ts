export interface IThemeInfo {
    iri: string;
    id: string;
    label: string;
}
