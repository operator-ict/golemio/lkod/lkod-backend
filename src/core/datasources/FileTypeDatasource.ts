import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { EuSparqlDataSource } from "./EuSparqlDataSource";
import { IFileResponse } from "./interfaces/IEuSparqlResponse";
import { IFileTypeInfo } from "./interfaces/IFileTypeInfo";

export class FileTypeDatasource extends EuSparqlDataSource<IFileResponse> {
    private readonly command = `PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
    PREFIX dc: <http://purl.org/dc/elements/1.1/>
    PREFIX eu-voc: <http://publications.europa.eu/resource/authority/>
    
    SELECT ?iri ?id (COALESCE(?labelCS, ?labelEN) AS ?label)
    WHERE {
      ?iri skos:topConceptOf eu-voc:file-type .
      ?iri dc:identifier ?id .
      OPTIONAL { ?iri skos:prefLabel ?labelCS. FILTER(LANG(?labelCS) = "cs") }
      OPTIONAL { ?iri skos:prefLabel ?labelEN. FILTER(LANG(?labelEN) = "en") }
    }`;

    constructor() {
        super();
    }

    public async getFileTypes() {
        const data = await this.query(this.command);
        await new JSONSchemaValidator("EuFileTypesValidator", this.getJsonSchema()).Validate(data);

        return data.results.bindings.map<IFileTypeInfo>((item) => {
            return {
                iri: item.iri.value,
                id: item.id.value,
                label: item.label.value,
            } as IFileTypeInfo;
        });
    }

    private getJsonSchema() {
        return this.getJsonSchemaEnvelope({
            type: "object",
            properties: {
                iri: {
                    type: "object",
                    properties: {
                        type: { type: "string", enum: ["uri"] },
                        value: { type: "string", format: "uri" },
                    },
                    required: ["type", "value"],
                },
                id: {
                    type: "object",
                    properties: {
                        type: { type: "string", enum: ["literal"] },
                        value: { type: "string" },
                    },
                    required: ["type", "value"],
                },
                label: {
                    type: "object",
                    properties: {
                        type: { type: "string", enum: ["literal"] },
                        value: { type: "string" },
                    },
                    required: ["type", "value"],
                },
            },
            required: ["iri", "id", "label"],
        });
    }
}
