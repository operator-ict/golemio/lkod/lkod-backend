import * as pg from "pg";

// return numbers as the numbers instead of strings
pg.defaults.parseInt8 = true;

import { CustomError } from "@golemio/core/dist/shared/golemio-errors";
import { Sequelize } from "sequelize";
import { config } from "../config";
import { log } from "../helpers";

/**
 * Sequelize Connector class to handle connections to Postgres DB.
 * Uses sequelize package.
 */
export class SequelizeConnector {
    private connection: Sequelize | undefined;

    /**
     * Creates connection to Postgres DB
     *
     * @returns {Promise<Sequelize>}
     */
    public connect = async (): Promise<Sequelize> => {
        try {
            if (this.connection) {
                return this.connection;
            }

            if (!config.database.database || !config.database.username || !config.database.password) {
                throw new CustomError("The ENV variable POSTGRES_CONN cannot be undefined.", true, this.constructor.name, 500);
            }

            this.connection = new Sequelize(config.database.database, config.database.username, config.database.password, {
                define: {
                    freezeTableName: true,
                    omitNull: false,
                    timestamps: false,
                },
                dialect: "postgres",
                dialectOptions: {
                    schema: config.database.schema,
                    ssl: config.database.ssl,
                },
                host: config.database.host,
                port: config.database.port,
                logging: log.silly, // logging by Logger::silly
                pool: {
                    idle: 10000,
                    max: 9,
                    min: 0,
                },
                retry: {
                    match: [
                        /SequelizeConnectionError/,
                        /SequelizeConnectionRefusedError/,
                        /SequelizeHostNotFoundError/,
                        /SequelizeHostNotReachableError/,
                        /SequelizeInvalidConnectionError/,
                        /SequelizeConnectionTimedOutError/,
                        /TimeoutError/,
                    ],
                    max: 8,
                },
            });

            if (!this.connection) {
                throw new Error("Connection is undefined.");
            }

            await this.connection.authenticate();
            log.info("Connected to PostgreSQL!");
            return this.connection;
        } catch (err) {
            throw new CustomError("Error while connecting to PostgreSQL.", false, this.constructor.name, 500, err);
        }
    };

    /**
     * Returns connection to Postres DB
     *
     * @returns {Sequelize}
     */
    public getConnection = (): Sequelize => {
        if (!this.connection) {
            throw new CustomError(
                "Sequelize connection does not exist. First call connect() method.",
                false,
                this.constructor.name,
                500
            );
        }
        return this.connection;
    };
}

const postgresConnector = new SequelizeConnector();

export { postgresConnector };
