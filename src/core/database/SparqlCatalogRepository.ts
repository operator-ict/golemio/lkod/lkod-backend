import axios from "axios";

export default class SparqlCatalogRepository {
    private auth?: string;
    private query_url: string;

    constructor(config: { auth?: string; query_url: string }) {
        this.auth = config.auth;
        this.query_url = config.query_url;
    }

    public async query<T>(query: string): Promise<T[]> {
        const response = await this.rawQuery<any>(query);

        return response.data.results.bindings.map((doc: any) => {
            return Object.entries(doc).reduce((acc, [key, value]: [key: any, value: any]) => {
                acc[key] = value.value;
                return acc;
            }, {} as any);
        });
    }

    private rawQuery<T>(query: string) {
        return axios.get<T>(this.query_url, {
            params: { query },
            headers: { Accept: "application/json", ...(this.auth && { Authorization: this.auth }) },
        });
    }
}
