import { CustomError } from "@golemio/core/dist/shared/golemio-errors";
import { NextFunction, Request, Response } from "express";
import { tokenManager } from "../helpers";
import * as core from "express-serve-static-core";

export interface IDecodedToken {
    id: number;
    email: string;
    name: string;
    role: string;
    iat: number;
    exp?: number | null;
    iss: string;
    jti: string;
}

export interface IExtendedRequest<
    P = core.ParamsDictionary,
    ResBody = any,
    ReqBody = any,
    ReqQuery = core.Query,
    Locals extends Record<string, any> = Record<string, any>
> extends Request<P, ResBody, ReqBody, ReqQuery, Locals> {
    decoded?: IDecodedToken;
}

/**
 * Authentication middleware class
 */
export class AuthenticationMiddleware {
    /**
     * Middleware to verify and decode the authorization header
     */
    public authenticate = (req: IExtendedRequest, res: Response, next: NextFunction): void => {
        const token = req.headers.authorization ? req.headers.authorization.replace("Bearer ", "") : null;

        if (token) {
            // Checking if token was revoked
            tokenManager
                .isTokenBlacklisted(token)
                .then((isBlacklisted) => {
                    if (isBlacklisted) {
                        throw new CustomError("Access token was revoked.", true, this.constructor.name, 401);
                    }
                    return tokenManager.verifyAndDecodeToken(token);
                }) // Verifing token
                .then((decoded) => {
                    req.decoded = decoded as IDecodedToken;
                    return next();
                })
                .catch((err) => {
                    next(new CustomError("Error authenticating provided token.", true, this.constructor.name, 401, err));
                });
        } else {
            next(new CustomError("No authentication token provided.", true, this.constructor.name, 401));
        }
    };
}
