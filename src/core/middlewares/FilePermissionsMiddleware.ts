import { CustomError } from "@golemio/core/dist/shared/golemio-errors";
import { NextFunction, Response } from "express";
import { IExtendedRequest } from "./AuthenticationMiddleware";
import { DatasetModel } from "../../models/DatasetModel";
import { OrganizationModel } from "../../models/OrganizationModel";

export const fileAccessMiddleware = async (req: IExtendedRequest, res: Response, next: NextFunction): Promise<void> => {
    try {
        if (!req.decoded?.id) {
            return next(new CustomError("Access denied.", true, "fileAccessMiddleware", 401));
        }

        const dataset = await DatasetModel.findByPk(req.params.datasetId, {
            include: [
                {
                    association: DatasetModel.associations.organization,
                    required: true,
                    include: [
                        {
                            association: OrganizationModel.associations.users,
                            required: true,
                            where: { id: req.decoded?.id },
                        },
                    ],
                },
            ],
        });

        if (!dataset) {
            return next(new CustomError("Access denied.", true, "fileAccessMiddleware", 401));
        }
    } catch (err) {
        return next(new CustomError("Error checking file access.", true, "fileAccessMiddleware", 401));
    }

    return next();
};
