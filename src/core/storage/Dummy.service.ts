import { CustomError } from "@golemio/core/dist/shared/golemio-errors";
import { IStorage } from "./Storage";

export class DummyService implements IStorage {
    public async init() {
        return;
    }

    public async upload(): Promise<never> {
        throw new CustomError("Storage unavailable", true, this.constructor.name, 503);
    }

    public async delete(): Promise<never> {
        throw new CustomError("Storage unavailable", true, this.constructor.name, 503);
    }

    public async list(): Promise<never> {
        throw new CustomError("Storage unavailable", true, this.constructor.name, 503);
    }
}
