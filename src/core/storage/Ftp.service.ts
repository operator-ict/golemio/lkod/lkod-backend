import { CustomError } from "@golemio/core/dist/shared/golemio-errors";
import { Client } from "basic-ftp";
import { Readable } from "stream";
import { log } from "../helpers";
import { IFile, IStorage } from "./Storage";

export interface IFtpConfig {
    host: string;
    user: string;
    password: string;
    port: number;
    downloadPath: string;
}

export class FtpService implements IStorage {
    private client: Client;
    private readonly downloadPath: string;

    constructor(private config: IFtpConfig) {
        this.downloadPath = config.downloadPath;
        this.client = new Client();
        this.client.ftp.log = log.silly;
        this.client.ftp.verbose = true;
    }

    public async upload(path: string, file: IFile): Promise<string> {
        await this.client.access(this.config);
        await this.client.ensureDir(`${path}`);
        await this.client.uploadFrom(Readable.from(file.data), file.name);
        this.client.close();

        return this.formatOutputFileUrl(path, file.name);
    }

    public async delete(path: string, filename: string): Promise<void> {
        try {
            await this.client.access(this.config);
            await this.client.remove(`${path}/${filename}`);
            this.client.close();
        } catch (err: any) {
            if (err.code === 550) {
                throw new CustomError("File not found", true, this.constructor.name, 404, err);
            }
            throw err;
        }
    }

    public async list(path: string): Promise<string[]> {
        await this.client.access(this.config);
        const list = await this.client.list(path);
        const response = list.map((el) => this.formatOutputFileUrl(path, el.name));
        this.client.close();
        return response;
    }

    private formatOutputFileUrl = (datasetId: string, filename: string) => {
        return `${this.downloadPath}/${datasetId}/${filename}`;
    };
}
