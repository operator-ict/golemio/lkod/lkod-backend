import { CustomError } from "@golemio/core/dist/shared/golemio-errors";
import { S3 } from "aws-sdk";
import { DeleteObjectRequest, ListObjectsRequest, ManagedUpload, PutObjectRequest } from "aws-sdk/clients/s3";
import { IFile, IStorage } from "./Storage";

export interface IS3Config {
    accessKeyId: string;
    bucketName: string;
    endpoint: string;
    secretAccessKey: string;
}

export class S3Service implements IStorage {
    private client: S3;
    private readonly uploadProjectPath: string;

    constructor(private config: IS3Config) {
        this.client = new S3({
            accessKeyId: config.accessKeyId,
            secretAccessKey: config.secretAccessKey,
            endpoint: config.endpoint,
            s3ForcePathStyle: true,
            signatureVersion: "v4",
        });
        this.uploadProjectPath = `${this.config.bucketName}`;
    }

    public async upload(path: string, file: IFile): Promise<string> {
        const params: PutObjectRequest = {
            Bucket: this.uploadProjectPath,
            Key: `${path}/${file.name}`,
            Body: file.data,
        };
        const options: ManagedUpload.ManagedUploadOptions = {
            partSize: 1024 * 1024,
            queueSize: 5,
        };
        await this.client.upload(params, options).promise();

        return this.formatOutputFileUrl(`${path}/${file.name}`);
    }

    public async delete(path: string, filename: string) {
        const params: DeleteObjectRequest = {
            Bucket: this.uploadProjectPath,
            Key: `${path}/${filename}`,
        };
        try {
            await this.client.headObject(params).promise();
        } catch (err: any) {
            if (err.statusCode === 404) {
                throw new CustomError("File not found", true, this.constructor.name, 404, err);
            }
            throw err;
        }
        await this.client.deleteObject(params).promise();
    }

    public async list(path: string): Promise<string[]> {
        const params: ListObjectsRequest = {
            Bucket: this.uploadProjectPath,
            Prefix: path,
        };

        const list = await this.client.listObjects(params).promise();
        return list.Contents?.map((el) => this.formatOutputFileUrl(el.Key!)) || [];
    }

    private formatOutputFileUrl = (path: string) => {
        return `${this.config.endpoint}/${this.config.bucketName}/${path}`;
    };
}
