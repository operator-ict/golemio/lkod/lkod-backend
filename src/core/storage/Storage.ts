import { config } from "../config";
import { DummyService } from "./Dummy.service";
import { FtpService } from "./Ftp.service";
import { S3Service } from "./S3.service";

export enum StorageType {
    NONE = "NONE",
    FTP = "FTP",
    S3 = "S3",
}

export interface IFile {
    name: string;
    data: Buffer;
}

export interface IStorage {
    list: (path: string) => Promise<string[]>;
    upload: (path: string, file: IFile) => Promise<string>;
    delete: (path: string, filename: string) => Promise<void>;
}

export class Storage {
    private storage: IStorage;

    constructor() {
        this.storage = getStorage();
    }

    public isAvailable = () => {
        return config.storageType !== StorageType.NONE;
    };

    public list = async (path: string): Promise<string[]> => {
        return this.storage.list(path);
    };

    public upload = async (path: string, file: IFile): Promise<string> => {
        return this.storage.upload(path, file);
    };

    public delete = async (path: string, filename: string) => {
        return this.storage.delete(path, filename);
    };
}

export const getStorage = () => {
    switch (config.storageType) {
        case StorageType.FTP:
            return new FtpService(config.ftp);
        case StorageType.S3:
            return new S3Service(config.s3);
        case StorageType.NONE:
            return new DummyService();
        default:
            throw new Error("Invalid storage type");
    }
};
