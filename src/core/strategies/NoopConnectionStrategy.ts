import { IVocabulary } from "../config";
import { IConnectionStrategy } from "./IConnectionStrategy";

export class NoopConnectionStrategy implements IConnectionStrategy {
    public uploadDataset(dataset: any): Promise<boolean> {
        return Promise.resolve(true);
    }
    public removeDatasetCascade(datasetIri: string): Promise<boolean> {
        return Promise.resolve(true);
    }
    public uploadVocabularies(vocabularies: IVocabulary[]): Promise<void> {
        return Promise.resolve();
    }
}
