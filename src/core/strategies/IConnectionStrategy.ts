import { IVocabulary } from "../config/ConfigInterface";

export interface IConnectionStrategy {
    uploadDataset(dataset: any): Promise<boolean>;
    removeDatasetCascade(datasetIri: string): Promise<boolean>;
    uploadVocabularies(vocabularies: IVocabulary[]): Promise<void>;
}
