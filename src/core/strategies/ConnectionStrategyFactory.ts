import { config } from "../config";
import { IConnectionStrategy } from "./IConnectionStrategy";
import { NoopConnectionStrategy } from "./NoopConnectionStrategy";
import { SparqlConnectionStrategy } from "./SparqlConnectionStrategy";

export class ConnectionStrategyFactory {
    private static instance: IConnectionStrategy;

    public static initConnectionStrategy(): void {
        if (config.sparql.enabled) {
            this.instance = new SparqlConnectionStrategy();
        } else {
            this.instance = new NoopConnectionStrategy();
        }
    }

    public static getConnectionStrategy(): IConnectionStrategy {
        if (!this.instance) {
            this.initConnectionStrategy();
        }

        return this.instance;
    }
}
