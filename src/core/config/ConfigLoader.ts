import { MailerTansportType } from "../mailer/Mailer";
import { StorageType } from "../storage/Storage";
import { IConfig } from "./";

export const config: IConfig = {
    activity_log_enabled: process.env.ACTIVITY_LOG_ENABLED === "true",
    app_version: process.env.npm_package_version || "unknown",
    backend_url: process.env.BACKEND_URL || "localhost",
    database: {
        database: process.env.DB_DATABASE,
        host: process.env.DB_HOSTNAME || "localhost",
        port: parseInt(process.env.DB_PORT ?? "5432", 10),
        password: process.env.DB_PASSWORD,
        schema: process.env.DB_SCHEMA || "public",
        username: process.env.DB_USERNAME,
        ssl: process.env.DB_SSL === "true",
    },
    frontend_url: process.env.FRONTEND_URL || "localhost",
    catalog_frontend_url: process.env.CATALOG_FRONTEND_URL || "localhost",
    ftp: {
        host: process.env.FTP_HOST || "",
        port: process.env.FTP_PORT ? +process.env.FTP_PORT : 21,
        user: process.env.FTP_USER || "",
        password: process.env.FTP_PASSWORD || "",
        downloadPath: process.env.FTP_PUBLIC_DOWNLOAD_PREFIX || "",
    },
    linked_data: {
        catalog_context: process.env.CATALOG_CONTEXT || "",
        catalog_publisher_id: process.env.CATALOG_PUBLISHER_ID || "",
        catalog_name: process.env.CATALOG_NAME || "",
        catalog_description: process.env.CATALOG_DESCRIPTION || "",
        catalog_homepage: process.env.CATALOG_HOMEPAGE || "",
        poskytovatel_url_prefix: process.env.POSKYTOVATEL_URL_PREFIX || "",
    },
    log_level: process.env.LOG_LEVEL,
    mailerFrom: process.env.MAILER_FROM || "Golemio LKOD <lkod@golemio.cz>",
    mailerType: (process.env.MAILER_TYPE || "DUMMY") as MailerTansportType,
    node_env: process.env.NODE_ENV,
    pathPrefix: process.env.PATH_PREFIX || "",
    port: process.env.PORT,
    redis_connection: process.env.REDIS_CONN,
    s3: {
        accessKeyId: process.env.S3_ACCESS_KEY_ID || "",
        bucketName: process.env.S3_BUCKET_NAME || "",
        endpoint: process.env.S3_ENDPOINT || "",
        secretAccessKey: process.env.S3_SECRET_ACCESS_KEY || "",
    },
    session_expires_in: process.env.SESSION_EXPIRES_IN ? +process.env.SESSION_EXPIRES_IN : 3600,
    smtp: {
        pass: process.env.SMTP_MAILER_PASS || "",
        user: process.env.SMTP_MAILER_USER || "",
        host: process.env.SMTP_MAILER_HOST || "",
        port: process.env.SMTP_MAILER_PORT ? +process.env.SMTP_MAILER_PORT : 465,
        secure: process.env.SMTP_MAILER_SECURE === "true", // default false
        rejectUnauthorized: process.env.SMTP_MAILER_TLS_REJECT_UNAUTHORIZED !== "false", // default true
    },
    sparql: {
        enabled: process.env.SPARQL_ENABLED ? !(process.env.SPARQL_ENABLED === "false") : true, // default true
        auth: process.env.SPARQL_AUTH,
        query_url: process.env.SPARQL_QUERY_URL || "localhost",
        data_url: process.env.SPARQL_DATA_URL || "localhost",
        update_url: process.env.SPARQL_UPDATE_URL || "localhost",
        vocabulary: {
            crontime: process.env.SPARQL_VOCABULARY_CRONTIME || "0 0 2 * * 0",
            static_data: require(`../../../config/rdf-static-data.json`) ?? [],
            organizations_crontime: process.env.SPARQL_ORGANIZATIONS_CRONTIME || "0 30 * * * *",
        },
    },
    arcgis: {
        enabled: process.env.ARCGIS_ENABLED === "true",
        cronRefreshTime: process.env.ARCGIS_CRON_TIME || "0 55 6 * * *",
    },
    storageType: (process.env.STORAGE_TYPE || "FTP") as StorageType,
    storageFileMaxSizeInKB: process.env.STORAGE_FILE_MAX_SIZE_IN_KB ? +process.env.STORAGE_FILE_MAX_SIZE_IN_KB : 20480, // 20MB
    token_expires_in: process.env.TOKEN_EXPIRES_IN ? +process.env.TOKEN_EXPIRES_IN : 43200,
    datasetLookupCacheInMinutes: process.env.DATASET_LOOKUP_CACHE_IN_MINUTES ? +process.env.DATASET_LOOKUP_CACHE_IN_MINUTES : 30,
};
