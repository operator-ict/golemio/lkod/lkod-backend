import winston = require("winston");
const argv = require("minimist")(process.argv.slice(2));

const { printf } = winston.format;
const myFormat = printf(({ message }) => {
    return `${message}`;
});
const logger = winston.createLogger({
    format: myFormat,
    level: "info",
    transports: [new winston.transports.Console()],
});

(async () => {
    if (argv.help || argv.h) {
        logger.info(
            `\n` +
                `Example usage: \n\n` +
                ` > bin/lkod.js create-organization --name="test org" --identificationNumber="02795281" ` +
                `--slug="test-org" --arcGisFeed=https://xxxxx.arcgis.com/api/feed/dcat-us/1.1.json\n\n`
        );
        return;
    }

    if (!argv.name) {
        logger.error(`Name is required.`);
        return;
    }

    if (!argv.identificationNumber) {
        logger.error(`Identification Number is required.`);
        return;
    }

    if (!argv.slug) {
        logger.error(`Slug is required.`);
        return;
    }

    logger.info(
        `\n\n` +
            `-- -------------------------------------\n` +
            `INSERT INTO "organization" \n` +
            `("name", "identificationNumber", "slug"${argv.arcGisFeed ? `, "arcGisFeed"` : ""}) \n` +
            `VALUES \n` +
            `(` +
            `'${argv.name}',` +
            `'${argv.identificationNumber}',` +
            `'${argv.slug}'` +
            `${argv.arcGisFeed ? `,'${argv.arcGisFeed}'` : ""}` +
            `); \n` +
            `-- -------------------------------------\n`
    );
})();
