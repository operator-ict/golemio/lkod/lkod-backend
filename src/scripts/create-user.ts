import winston = require("winston");
import { secretManager } from "../core/helpers";
import { UserRole } from "../models/UserModel";
const argv = require("minimist")(process.argv.slice(2));

const { printf } = winston.format;
const myFormat = printf(({ message }) => {
    return `${message}`;
});
const logger = winston.createLogger({
    format: myFormat,
    level: "info",
    transports: [new winston.transports.Console()],
});

(async () => {
    if (argv.help || argv.h) {
        logger.info(
            `\nExample usage: \n\n > bin/lkod.js create-user --email="test@golemio.cz" --password="pass" ` +
                `--name="test user" --role="user" \n\n`
        );
        return;
    }

    if (!argv.email) {
        logger.error(`Email is required.`);
        return;
    }

    if (!argv.password) {
        logger.error(`Password is required.`);
        return;
    }

    if (!argv.name) {
        logger.error(`Name is required.`);
        return;
    }

    if (!argv.role || !Object.values(UserRole).includes(argv.role)) {
        logger.error(`User role is required and must be one of ${Object.values(UserRole).join(", ")}.`);
        return;
    }

    logger.info(
        `\n\n` +
            `-- -------------------------------------\n` +
            `INSERT INTO "user" \n` +
            `("email", "password", "name", "role", "hasDefaultPassword") \n` +
            `VALUES \n` +
            `(` +
            `'${(argv.email as string).toLocaleLowerCase()}',` +
            `'${await secretManager.hashSecret(argv.password)}',` +
            `'${argv.name}',` +
            `'${argv.role}',` +
            `'true'` +
            `); \n` +
            `-- -------------------------------------\n`
    );
})();
