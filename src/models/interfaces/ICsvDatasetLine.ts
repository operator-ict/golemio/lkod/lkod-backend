export interface ICsvDatasetLine {
    organizationName: string;
    datasetUrl: string;
    datasetName: string;
    distributionName: string;
    distributionFormat: string;
    contactEmail: string;
    contactName: string;
}
