export interface IJsonLDOrganization {
    "@id": string;
    "@type": "schema:Organization";
    alternateName: string;
    sameAs: string;
    legalName: string;
    name: {
        "@language": "cs";
        "@value": string;
    };
    description?: {
        "@language": "cs";
        "@value": string;
    };
    logo?: string;
}

export interface IJsonLDOrganizations {
    "@graph": IJsonLDOrganization[];
    "@context": object;
}
