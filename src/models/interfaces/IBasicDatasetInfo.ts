import { IIriTitleCount } from "./IIriTitleCount";

export interface IDatasetInfoWithCounts {
    dataset_basic_info: IDatasetInfo[];
    publisher_count: IIriTitleCount[];
    theme_count: IIriTitleCount[];
    format_count: IIriTitleCount[];
    keyword_count: Array<Omit<IIriTitleCount, "iri">>;
}

export interface IDatasetInfo {
    id: string;
    iri: string;
    title: string;
    description: string;
    publisher: string;
    theme_iris: string[];
    format_iris: string[];
    publisher_slug: string;
    keywords: string[];
}
