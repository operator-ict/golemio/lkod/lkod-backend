export interface IOrganizationWithCount {
    id: number;
    name: string;
    slug: string;
    logo: string;
    description: string;
    count: number;
}
