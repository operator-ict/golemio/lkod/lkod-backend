export interface IIriTitleCount {
    iri: string;
    label: string;
    count: number;
}
