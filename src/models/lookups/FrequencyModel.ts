import { JSONSchemaType } from "ajv";
import { DataTypes, Model, ModelAttributes } from "sequelize";
import { IFrequencyInfo } from "../../core/datasources/interfaces/IFrequencyInfo";

export class FrequencyModel extends Model<IFrequencyInfo> implements IFrequencyInfo {
    declare iri: string;
    declare id: string;
    declare label: string;
    declare altLabels: string;

    public static attributesModel: ModelAttributes<FrequencyModel> = {
        iri: {
            type: DataTypes.STRING,
            allowNull: false,
            primaryKey: true,
        },
        id: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        label: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        altLabels: {
            field: "alt_labels",
            type: DataTypes.STRING,
            allowNull: false,
        },
    };

    public static jsonSchema: JSONSchemaType<IFrequencyInfo[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                iri: { type: "string" },
                id: { type: "string" },
                label: { type: "string" },
                altLabels: { type: "string" },
            },
            required: ["iri", "id", "label"],
        },
    };
}
