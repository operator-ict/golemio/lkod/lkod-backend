import { ILogger } from "@golemio/core/dist/helpers/logger/LoggerProvider";
import { IValidator, JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { SequelizeConnector } from "../../core/database/PostgresConnector";
import { IFrequencyInfo } from "../../core/datasources/interfaces/IFrequencyInfo";
import { AbstractCachedRepository } from "./AbstractCachedRepository";
import { FrequencyModel } from "./FrequencyModel";

export class FrequencyRepository extends AbstractCachedRepository<IFrequencyInfo> {
    protected validator: IValidator;
    protected tableName: string = "frequencies";

    constructor(connector: SequelizeConnector, log: ILogger) {
        super(connector, log);
        this.validator = new JSONSchemaValidator("FrequencyRepositoryValidator", FrequencyModel.jsonSchema);

        FrequencyModel.init(FrequencyModel.attributesModel, {
            schema: this.schema,
            sequelize: connector.getConnection(),
            tableName: this.tableName,
            timestamps: true,
            createdAt: "created_at",
            updatedAt: "updated_at",
        });
    }

    public async refreshCache(): Promise<IFrequencyInfo[]> {
        return FrequencyModel.findAll({
            attributes: ["iri", "id", "label"],
            raw: true,
        });
    }

    public async saveAll(frequencies: IFrequencyInfo[]): Promise<void> {
        await this.validator.Validate(frequencies);

        await FrequencyModel.bulkCreate(frequencies, {
            updateOnDuplicate: ["id", "label", "updated_at" as keyof IFrequencyInfo],
            ignoreDuplicates: false,
        });
    }
}
