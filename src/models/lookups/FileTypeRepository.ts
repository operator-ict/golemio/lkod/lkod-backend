import { ILogger } from "@golemio/core/dist/helpers/logger/LoggerProvider";
import { IValidator, JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { SequelizeConnector } from "../../core/database/PostgresConnector";
import { IFileTypeInfo } from "../../core/datasources/interfaces/IFileTypeInfo";
import { AbstractCachedRepository } from "./AbstractCachedRepository";
import { FileTypeModel } from "./FileTypeModel";

export class FileTypeRepository extends AbstractCachedRepository<IFileTypeInfo> {
    protected tableName: string = "file_types";
    protected validator: IValidator;

    constructor(connector: SequelizeConnector, log: ILogger) {
        super(connector, log);
        this.validator = new JSONSchemaValidator("FileTypeRepositoryValidator", FileTypeModel.jsonSchema);

        FileTypeModel.init(FileTypeModel.attributesModel, {
            schema: this.schema,
            sequelize: this.connector.getConnection(),
            tableName: this.tableName,
            timestamps: true,
            createdAt: "created_at",
            updatedAt: "updated_at",
        });
    }

    protected async refreshCache(): Promise<IFileTypeInfo[]> {
        return await FileTypeModel.findAll({
            raw: true,
        });
    }

    public async saveAll(fileTypes: IFileTypeInfo[]): Promise<void> {
        await this.validator.Validate(fileTypes);

        await FileTypeModel.bulkCreate(fileTypes, {
            updateOnDuplicate: ["id", "label", "updated_at" as keyof IFileTypeInfo],
            ignoreDuplicates: false,
        });
    }
}
