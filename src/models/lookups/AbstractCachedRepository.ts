import { ILogger } from "@golemio/core/dist/helpers/logger/LoggerProvider";
import { IValidator } from "@golemio/core/dist/shared/golemio-validator";
import { config } from "../../core/config";
import { SequelizeConnector } from "../../core/database/PostgresConnector";

export abstract class AbstractCachedRepository<T> {
    protected abstract validator: IValidator;
    protected abstract tableName: string;

    protected schema: string;
    protected cache: T[] | null;
    protected cacheTimestamp: Date | null;
    protected readonly CACHE_EXPIRATION = 60 * 60 * 1000; // 1 hour

    constructor(protected connector: SequelizeConnector, protected log: ILogger) {
        this.schema = config.database.schema;
        this.cache = null;
        this.cacheTimestamp = null;
    }

    public async getAll(): Promise<T[]> {
        if (!this.cache || !this.cacheTimestamp || this.cacheTimestamp.valueOf() < new Date().valueOf() - this.CACHE_EXPIRATION) {
            this.cache = await this.refreshCache();
            this.cacheTimestamp = new Date();
        }

        return this.cache;
    }

    protected abstract refreshCache(): Promise<T[]>;
}
