import { ILogger } from "@golemio/core/dist/helpers/logger/LoggerProvider";
import { IValidator, JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { SequelizeConnector } from "../../core/database/PostgresConnector";
import { IThemeInfo } from "../../core/datasources/interfaces/IThemeInfo";
import { AbstractCachedRepository } from "./AbstractCachedRepository";
import { ThemeModel } from "./ThemeModel";

export class ThemeRepository extends AbstractCachedRepository<IThemeInfo> {
    protected validator: IValidator;
    protected tableName: string = "themes";

    constructor(connector: SequelizeConnector, log: ILogger) {
        super(connector, log);
        this.validator = new JSONSchemaValidator("ThemeRepositoryValidator", ThemeModel.jsonSchema);

        ThemeModel.init(ThemeModel.attributesModel, {
            schema: this.schema,
            sequelize: this.connector.getConnection(),
            tableName: this.tableName,
            timestamps: true,
            createdAt: "created_at",
            updatedAt: "updated_at",
        });
    }

    public async refreshCache(): Promise<IThemeInfo[]> {
        return ThemeModel.findAll({
            raw: true,
        });
    }

    public async saveAll(themes: IThemeInfo[]): Promise<void> {
        await this.validator.Validate(themes);

        await ThemeModel.bulkCreate(themes, {
            updateOnDuplicate: ["id", "label", "updated_at" as keyof IThemeInfo],
            ignoreDuplicates: false,
        });
    }
}
