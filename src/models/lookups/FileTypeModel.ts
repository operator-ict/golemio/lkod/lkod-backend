import { JSONSchemaType } from "ajv";
import { DataTypes, Model, ModelAttributes } from "sequelize";
import { IFileTypeInfo } from "../../core/datasources/interfaces/IFileTypeInfo";

export class FileTypeModel extends Model<IFileTypeInfo> implements IFileTypeInfo {
    declare iri: string;
    declare id: string;
    declare label: string;

    public static attributesModel: ModelAttributes<FileTypeModel> = {
        iri: {
            type: DataTypes.STRING,
            allowNull: false,
            primaryKey: true,
        },
        id: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        label: {
            type: DataTypes.STRING,
            allowNull: false,
        },
    };

    public static jsonSchema: JSONSchemaType<IFileTypeInfo[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                iri: { type: "string" },
                id: { type: "string" },
                label: { type: "string" },
            },
            required: ["iri", "id", "label"],
        },
    };
}
