import { JSONSchemaType } from "ajv";
import { DataTypes, Model, ModelAttributes } from "sequelize";
import { IThemeInfo } from "../../core/datasources/interfaces/IThemeInfo";

export class ThemeModel extends Model<IThemeInfo> implements IThemeInfo {
    declare iri: string;
    declare id: string;
    declare label: string;

    public static attributesModel: ModelAttributes<ThemeModel> = {
        iri: {
            type: DataTypes.STRING,
            allowNull: false,
            primaryKey: true,
        },
        id: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        label: {
            type: DataTypes.STRING,
            allowNull: false,
        },
    };

    public static jsonSchema: JSONSchemaType<IThemeInfo[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                iri: { type: "string" },
                id: { type: "string" },
                label: { type: "string" },
            },
            required: ["iri", "id", "label"],
        },
    };
}
