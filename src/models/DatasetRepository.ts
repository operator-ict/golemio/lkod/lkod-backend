import { ILogger } from "@golemio/core/dist/helpers/logger/LoggerProvider";
import { QueryTypes } from "sequelize";
import { config } from "../core/config/ConfigLoader";
import { SequelizeConnector } from "../core/database/PostgresConnector";
import { IDatasetInfoWithCounts } from "./interfaces/IBasicDatasetInfo";
import { IIriTitleCount } from "./interfaces/IIriTitleCount";
import { IOrganizationWithCount } from "./interfaces/IOrganizationWithCount";

export class DatasetRepository {
    private schema: string;
    private publisherPrefix: string;

    constructor(private connector: SequelizeConnector, private log: ILogger) {
        this.schema = config.database.schema;
        this.publisherPrefix = config.linked_data.poskytovatel_url_prefix;
    }

    /**
     * Note to jsonb array comparison:
     * source: https://www.postgresql.org/docs/9.5/functions-json.html
     * keywords::jsonb ?| $keywords Do any of these array strings exist as top-level keys?
     * keywords::jsonb ?& $keywords Do all of these array strings exist as top-level keys?
     */
    public async getDatasetsBasicInfo(
        organizations: string[] | null,
        themes: string[] | null,
        keywords: string[] | null,
        formats: string[] | null,
        limit: number,
        offset: number,
        searchText: string | null
    ) {
        const where = ["1=1"];

        if (organizations && organizations.length > 0) {
            where.push(`publisher_slug = ANY($publishers)`);
        }
        if (themes && themes.length > 0) {
            where.push(`theme_iris ?& $theme_iris`);
        }
        if (keywords && keywords.length > 0) {
            where.push(`keywords ?& $keywords`);
        }
        if (formats && formats.length > 0) {
            where.push(`format_iris ?& $formats`);
        }

        if (searchText) {
            where.push(`
                to_tsvector(${this.schema}.unaccent(title))
                || to_tsvector(${this.schema}.unaccent(description))
                || to_tsvector(${this.schema}.unaccent(keywords::text))
                @@ plainto_tsquery(${this.schema}.unaccent($searchText))
            `);
        }

        const result = await this.connector.getConnection().query<IDatasetInfoWithCounts>(
            `
			with basic_info as (
				select * 
				from ${this.schema}.v_dataset_basic_info
				where ${where.join(" and ")}
				order by title                               
			), publisher_count as (
				select 
                    '${this.publisherPrefix}' || publisher_slug as publisher_iri,
                    publisher, 
                    count(id) as count from basic_info
				group by publisher, publisher_slug
				order by count(id) desc
			), theme_count as (
                select 
                    theme_iri,
                    t.label as title,
                    count
                from (
                    select jsonb_array_elements_text(theme_iris) as theme_iri, count(id) as count
                    from basic_info
                    group by theme_iri
                    order by count(id) desc
                ) dbi
                inner join ${this.schema}.themes t on
                    t.iri = dbi.theme_iri                    
			), format_count as (
                select 
                    format_iri,
                    t.label as title,
                    count
                from (
                    select jsonb_array_elements_text(format_iris) as format_iri, count(id) as count 
                    from basic_info
                    group by format_iri
                    order by count(id) desc
                ) dbi
                inner join ${this.schema}.file_types t on
                    t.iri = dbi.format_iri
			), keywords_count as (
				select json_array_elements_text(keywords::json) as keyword, count(id) as count
				from basic_info
				group by keyword
				order by count(id) desc
			)
			select
				(SELECT json_agg(basic_info_page) FROM (
                    select * from basic_info
                    limit $limit offset $offset 
                ) basic_info_page) AS dataset_basic_info,
				(SELECT json_agg(json_build_object(
                    'iri',publisher_iri, 
                    'label',publisher, 
                    'count',count                    
                )) FROM publisher_count) AS publisher_count,
				(SELECT json_agg(json_build_object(
                    'iri',theme_iri, 
                    'label',title, 
                    'count',count
                )) FROM theme_count) AS theme_count,
				(SELECT json_agg(json_build_object(
                    'iri',format_iri, 
                    'label',title, 
                    'count',count
                )) FROM format_count) AS format_count,
				(SELECT json_agg(json_build_object(
                    'label',keyword, 
                    'count',count
                )) FROM keywords_count) AS keyword_count,
                (select count(*) from basic_info) as total_count
			`,
            {
                type: QueryTypes.SELECT,
                bind: {
                    publishers: organizations,
                    theme_iris: themes,
                    keywords: keywords,
                    formats: formats,
                    searchText: searchText,
                    limit: limit,
                    offset: offset,
                },
                raw: true,
                plain: true,
            }
        );

        return result;
    }

    public async getOrganizationsWithCount(limit: number, offset: number) {
        const result = await this.connector.getConnection().query<IOrganizationWithCount>(
            `select 
                id, name, slug, logo, description, count 
            from ${this.schema}.organization o
            inner join (select 
                "organizationId", 
                count(*) 
            from ${this.schema}.dataset
            where status='published'
            group by "organizationId") as c on
                c."organizationId" = o.id
            order by count desc
            limit $limit offset $offset
        `,
            {
                raw: true,
                type: QueryTypes.SELECT,
                bind: {
                    limit: limit,
                    offset: offset,
                },
            }
        );
        return result;
    }

    public async getThemesWithCount(limit: number, offset: number) {
        const result = await this.connector.getConnection().query<IIriTitleCount>(
            `select 
            theme_iri as iri,
            t.label as label,
            count
        from (
            select 
                jsonb_array_elements_text(theme_iris) as theme_iri, 	
                count(id) as count
            from ${this.schema}.v_dataset_basic_info 
            group by theme_iri
            order by count(id) desc            
        ) dbi
        inner join ${this.schema}.themes t on
            t.iri = dbi.theme_iri  
        limit $limit offset $offset      
        `,
            {
                raw: true,
                type: QueryTypes.SELECT,
                bind: {
                    limit: limit,
                    offset: offset,
                },
            }
        );
        return result;
    }
}
