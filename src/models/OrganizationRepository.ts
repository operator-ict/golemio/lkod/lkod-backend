import { CustomError } from "@golemio/core/dist/shared/golemio-errors";
import { config } from "../core/config";
import { IOrganizationAttributes, OrganizationModel } from "./OrganizationModel";
import { IJsonLDOrganization, IJsonLDOrganizations } from "./interfaces/IJsonLDOrganization";
import { Op } from "sequelize";

export class OrganizationRepository {
    public getOrganizationsAsJsonLD = async (): Promise<IJsonLDOrganizations> => {
        try {
            const organizations = await OrganizationModel.findAll();

            return this.createJsonLD(organizations);
        } catch (err) {
            throw new CustomError("Error while getting Organizations as JSON-LD", true, this.constructor.name, 500, err);
        }
    };

    public getArcGisOrgIds = async (): Promise<number[]> => {
        const organizations = await OrganizationModel.findAll({
            attributes: ["id"],
            where: {
                arcGisFeed: {
                    [Op.ne]: null,
                },
            },
            raw: true,
        });

        return organizations.map((org) => org.id);
    };

    private createJsonLD = (organizations: IOrganizationAttributes[]): IJsonLDOrganizations => {
        return {
            "@graph": organizations.map((org) => this.mapOrganizationToRdf(org)),
            "@context": {
                alternateName: "http://schema.org/alternateName",
                description: "http://schema.org/description",
                logo: {
                    "@id": "http://schema.org/logo",
                    "@type": "@id",
                },
                name: "http://xmlns.com/foaf/0.1/name",
                legalName: "http://schema.org/legalName",
                sameAs: {
                    "@id": "http://www.w3.org/2002/07/owl#sameAs",
                    "@type": "@id",
                },
                schema: "http://schema.org/",
                rdf: "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
                owl: "http://www.w3.org/2002/07/owl#",
                rdfs: "http://www.w3.org/2000/01/rdf-schema#",
                foaf: "http://xmlns.com/foaf/0.1/",
            },
        };
    };

    private mapOrganizationToRdf = (org: IOrganizationAttributes): IJsonLDOrganization => {
        return {
            "@id": `${config.linked_data.poskytovatel_url_prefix}${org.slug}`,
            "@type": "schema:Organization",
            alternateName: org.slug,
            sameAs: `https://linked.opendata.cz/zdroj/ekonomický-subjekt/${org.identificationNumber}`,
            legalName: org.name,
            name: {
                "@language": "cs",
                "@value": org.name,
            },
            ...(org.description
                ? {
                      description: {
                          "@language": "cs",
                          "@value": org.description,
                      },
                  }
                : {}),
            ...(org.logo ? { logo: org.logo } : {}),
        };
    };
}
