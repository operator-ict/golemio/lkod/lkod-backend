import { Association, DataTypes, Model, ModelAttributes } from "sequelize";
import { OrganizationModel } from "./OrganizationModel";
import { UserModel } from "./UserModel";

export interface IUserOrganizationAttributes {
    userId: number;
    organizationId: number;

    // associations
    user: UserModel;
    organization: OrganizationModel;
}

export const UserOrganizationSchema: ModelAttributes = {
    userId: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        allowNull: false,
    },
    organizationId: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        allowNull: false,
    },
};

/**
 * UserOrganization Model
 * table: user_organization
 */
export class UserOrganizationModel extends Model<IUserOrganizationAttributes> implements IUserOrganizationAttributes {
    public userId: number;
    public organizationId: number;

    public readonly user: UserModel;
    public readonly organization: OrganizationModel;

    public static associations: {
        user: Association<UserOrganizationModel, UserModel>;
        organization: Association<UserOrganizationModel, OrganizationModel>;
    };
}
