// Import everything from express and assign it to the express variable
import { CustomError, ErrorHandler, HTTPErrorHandler, ICustomErrorObject } from "@golemio/core/dist/shared/golemio-errors";
import * as express from "express";
import { NextFunction, Request, Response } from "express";
import * as fs from "fs";
import * as helmet from "helmet";
import * as http from "http";
import * as path from "path";
import * as swaggerUI from "swagger-ui-express";
import * as YAML from "yamljs";
import { config } from "./core/config";
import { postgresConnector, redisConnector } from "./core/database";
import { cronTasksManger, getRequestLogger, log } from "./core/helpers";
import { initModels } from "./models";
import { AuthRouter } from "./resources/auth";
import { DatasetsRouter } from "./resources/datasets";
import { DefaultRouter } from "./resources/default";
import { DatasetsCsvRouter } from "./resources/download/DatasetsCsvRouter";
import { FormDataRouter } from "./resources/form-data";
import { LODRouter } from "./resources/lod";
import { LookupRouter } from "./resources/lookup/LookupRouter";
import { OrganizationsRouter } from "./resources/organizations";
import { SessionsRouter } from "./resources/sessions";
import { cronTasksRegister } from "./registers";
import { UsersRouter } from "./resources/users";
import { PublicRouter } from "./resources/public/PublicRouter";

/**
 * Entry point of the application. Creates and configures an ExpressJS web server.
 */
export default class App {
    // Create a new express application instance
    public express: express.Application = express();
    // The port the express app will listen on
    public port: number = parseInt(config.port || "3000", 10);
    private server: http.Server;
    private commitSHA: string;
    private readonly apiDocument: object;

    /**
     * Runs configuration methods on the Express instance
     */
    constructor() {
        try {
            this.apiDocument = YAML.load(path.resolve(__dirname, "../docs/openapi.yaml"));
        } catch (err) {
            log.warn("API docs was not loaded");
            log.warn(err);
        }
    }

    // Starts the application and runs the server
    public start = async (): Promise<void> => {
        try {
            this.commitSHA = await this.loadCommitSHA();
            log.info(`Commit SHA: ${this.commitSHA}`);

            await this.database();
            this.express = express();
            this.middleware();

            await this.routes();

            cronTasksRegister.registerCronTasks();
            cronTasksManger.startAll();
            cronTasksManger.checkStatus();

            this.server = http.createServer(this.express);
            // Setup error handler hook on server error
            this.server.on("error", (err: Error) => {
                ErrorHandler.handle(new CustomError("Could not start a server", false, "App", 500, err), log);
            });
            // Serve the application at the given port
            this.server.listen(this.port, () => {
                // Success callback
                log.info(`Listening at http://localhost:${this.port}${config.pathPrefix}`);
            });
        } catch (err: any) {
            ErrorHandler.handle(err, log);
        }
    };

    public stop = async (): Promise<void> => {
        this.server.close();
    };

    private setHeaders = (req: Request, res: Response, next: NextFunction): void => {
        res.setHeader("Access-Control-Allow-Origin", "*");
        res.setHeader("Access-Control-Allow-Methods", "GET, OPTIONS, HEAD, POST, PATCH, DELETE");
        res.setHeader("Access-Control-Allow-Headers", "Authorization, Origin, X-Requested-With, Content-Type, Accept");
        next();
    };

    private database = async (): Promise<void> => {
        await postgresConnector.connect();
        initModels();
        await redisConnector.connect();
    };

    private middleware = (): void => {
        this.express.use(express.json({ limit: "10MB" }));
        this.express.use(express.urlencoded({ extended: true, limit: "10MB" }));
        this.express.use(helmet());
        this.express.use(getRequestLogger);
        this.express.use(this.setHeaders);
    };

    private routes = async (): Promise<void> => {
        // Create specific routes with their own router
        this.express.use(`${config.pathPrefix}/`, new DefaultRouter(this.commitSHA).router);

        // Swagger UI
        this.express.use(
            `${config.pathPrefix}/api-docs`,
            swaggerUI.serve,
            swaggerUI.setup(this.apiDocument, {
                customSiteTitle: "Golemio LKOD API Documentation",
            })
        );

        this.express.use(`${config.pathPrefix}/auth`, new AuthRouter().router);
        this.express.use(`${config.pathPrefix}/users`, new UsersRouter().router);
        this.express.use(`${config.pathPrefix}/datasets`, new DatasetsRouter().router);
        this.express.use(`${config.pathPrefix}/sessions`, new SessionsRouter().router);
        this.express.use(`${config.pathPrefix}/organizations`, new OrganizationsRouter().router);
        this.express.use(`${config.pathPrefix}/form-data`, new FormDataRouter().router);
        this.express.use(`${config.pathPrefix}/lod/catalog`, new LODRouter().router);
        this.express.use(`${config.pathPrefix}/lookup`, new LookupRouter().router);
        this.express.use(`${config.pathPrefix}/public`, new PublicRouter().router);
        this.express.use(`${config.pathPrefix}/datasets.csv`, new DatasetsCsvRouter().router);

        /** static files */
        this.express.use("/.well-known/security.txt", express.static("public/.well-known/security.txt"));

        /** ignore automatic favicon requests from some browsers */
        this.express.get("/favicon.ico", (_req, res) => res.sendStatus(204));

        // Not found error - no route was matched
        this.express.use((req: Request, res: Response, next: NextFunction) => {
            next(new CustomError("Not found", true, "App", 404));
        });

        // Error handler to catch all errors sent by routers (propagated through next(err))
        this.express.use(`${config.pathPrefix}/form-data`, (err: any, req: Request, res: Response, next: NextFunction) => {
            log.error(err);
            res.redirect(config.frontend_url + `?e=${encodeURIComponent(JSON.stringify(err.toObject ? err.toObject() : err))}`);
        });
        this.express.use((err: any, req: Request, res: Response, next: NextFunction) => {
            // hadle body parser errors
            if (err instanceof SyntaxError) {
                err = new CustomError(err.message, true, "App", (err as any)?.status || 500, err.message);
            }

            const warnCodes = [400, 401, 403, 404, 422];
            const errObject: ICustomErrorObject = HTTPErrorHandler.handle(
                err,
                log,
                warnCodes.includes(err.code) ? "warn" : "error"
            );
            log.silly("Error caught by the router error handler.");
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(errObject.error_status || 500).send(errObject);
        });
    };

    /**
     * Load the Commit SHA of the current build
     *
     * Only to be used at startup, not in runtime of the application.
     */
    private loadCommitSHA = async (): Promise<string> => {
        return new Promise<string>((resolve, reject) => {
            fs.readFile(path.join(__dirname, "..", "commitsha"), (err: NodeJS.ErrnoException | null, data: Buffer) => {
                if (err) {
                    return resolve(undefined as any);
                }
                return resolve(data.toString());
            });
        });
    };
}
