import { ILogger } from "@golemio/core/dist/helpers";
import { config } from "../../core/config";
import { ICronTask } from "../../core/helpers/CronTasksManager";
import { ConnectionStrategyFactory } from "../../core/strategies/ConnectionStrategyFactory";
import { IConnectionStrategy } from "../../core/strategies/IConnectionStrategy";
import { DatasetModel, DatasetStatus } from "../../models/DatasetModel";
import { OrganizationRepository } from "../../models/OrganizationRepository";

/**
 * Cron task to refresh arcgis related datasets in sparql
 */
export default class RefreshArcgisDatasetsTask implements ICronTask {
    public readonly cronTime = config.arcgis.cronRefreshTime;
    public readonly name = "refresh_arcgis_datasets";
    private connectionStrategy: IConnectionStrategy;

    constructor(private organizationRepository: OrganizationRepository, private log: ILogger) {
        this.connectionStrategy = ConnectionStrategyFactory.getConnectionStrategy();
    }

    public async process(): Promise<void> {
        const organizations = await this.organizationRepository.getArcGisOrgIds();

        for (const orgId of organizations) {
            const datasets = await DatasetModel.findAll({
                where: {
                    organizationId: orgId,
                    status: "saved",
                },
            });

            for (const dataset of datasets) {
                if (!!dataset.updatedAt) {
                    await this.connectionStrategy.removeDatasetCascade(dataset.data!.iri!);
                }
                await this.connectionStrategy.uploadDataset(dataset.data!);
                await DatasetModel.update(
                    {
                        status: DatasetStatus.published,
                    },
                    {
                        where: {
                            id: dataset.id,
                        },
                    }
                );
            }

            const deletedDatasets = await DatasetModel.findAll({
                where: {
                    organizationId: orgId,
                    status: "deleted",
                },
            });

            for (const dataset of deletedDatasets) {
                await this.connectionStrategy.removeDatasetCascade(dataset.data!.iri!);
                await DatasetModel.destroy({
                    where: {
                        id: dataset.id,
                    },
                });
            }
        }
        this.log.verbose(`Arcgis related datasets refreshed.`);
    }
}
