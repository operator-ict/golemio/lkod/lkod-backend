import { ILogger } from "@golemio/core/dist/helpers/logger/LoggerProvider";
import { CustomError } from "@golemio/core/dist/shared/golemio-errors";
import { config } from "../../core/config";
import { postgresConnector } from "../../core/database/PostgresConnector";
import { FileTypeDatasource } from "../../core/datasources/FileTypeDatasource";
import { FrequencyDatasource } from "../../core/datasources/FrequencyDatasource";
import { ThemeDatasource } from "../../core/datasources/ThemeDatasource";
import { ICronTask } from "../../core/helpers/CronTasksManager";
import { FileTypeRepository } from "../../models/lookups/FileTypeRepository";
import { FrequencyRepository } from "../../models/lookups/FrequencyRepository";
import { ThemeRepository } from "../../models/lookups/ThemeRepository";

/**
 * Cron task to refresh arcgis related datasets in sparql
 */
export default class RefreshVocabToPostgresTask implements ICronTask {
    public readonly cronTime = config.sparql.vocabulary.crontime;
    public readonly name = "refresh_vocab_to_postgres";

    private fileTypeDatasource: FileTypeDatasource;
    private themeDatasource: ThemeDatasource;
    private frequencyDatasource: FrequencyDatasource;

    private fileTypeRepository: FileTypeRepository;
    private themeRepository: ThemeRepository;
    private frequencyRepository: FrequencyRepository;

    constructor(private logger: ILogger) {
        this.fileTypeDatasource = new FileTypeDatasource();
        this.themeDatasource = new ThemeDatasource();
        this.frequencyDatasource = new FrequencyDatasource();

        this.fileTypeRepository = new FileTypeRepository(postgresConnector, this.logger);
        this.themeRepository = new ThemeRepository(postgresConnector, this.logger);
        this.frequencyRepository = new FrequencyRepository(postgresConnector, this.logger);
    }

    public async process(): Promise<void> {
        try {
            this.logger.verbose("Refreshing vocabularies to postgres");
            const fileTypes = await this.fileTypeDatasource.getFileTypes();
            await this.fileTypeRepository.saveAll(fileTypes);
            this.logger.verbose("File types refreshed");
        } catch (err) {
            if (err instanceof CustomError) {
                this.logger.error(err);
            }
            this.logger.error(new CustomError("Error while refreshing file types", true, this.constructor.name, 500, err));
        }

        try {
            this.logger.verbose("Refreshing themes to postgres");
            const themes = await this.themeDatasource.getThemes();
            await this.themeRepository.saveAll(themes);
            this.logger.verbose("Themes refreshed");
        } catch (err) {
            if (err instanceof CustomError) {
                this.logger.error(err);
            }
            this.logger.error(new CustomError("Error while refreshing themes", true, this.constructor.name, 500, err));
        }

        try {
            this.logger.verbose("Refreshing frequencies to postgres");
            const frequencies = await this.frequencyDatasource.getFrequencies();
            await this.frequencyRepository.saveAll(frequencies);
            this.logger.verbose("Frequencies refreshed");
        } catch (err) {
            if (err instanceof CustomError) {
                this.logger.error(err);
            }
            this.logger.error(new CustomError("Error while refreshing frequencies", true, this.constructor.name, 500, err));
        }
    }
}
