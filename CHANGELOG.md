# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.5.5] - 2025-02-18

### Fixed

-  datasets value or array validations ([lkod#140](https://gitlab.com/operator-ict/golemio/lkod/lkod-general/-/issues/140))

## [1.5.4] - 2025-01-07

### Fixed

-  dataset id validation ([lkod#136](https://gitlab.com/operator-ict/golemio/lkod/lkod-general/-/issues/136))

## [1.5.3] - 2024-09-23

### Fixed

-   keywords validation ([lkod-backend#36](https://gitlab.com/operator-ict/golemio/lkod/lkod-backend/-/issues/36))

### Changed

-   Update Node.js to 20.18.1 ([lkod#135](https://gitlab.com/operator-ict/golemio/lkod/lkod-general/-/issues/135))

## [1.5.2] - 2024-08-19

### Added

-   add backstage metadata files
-   add .gitattributes file

## [1.5.1] - 2024-08-13

### Changed

-   Update Node.js to 20.16.0 ([lkod#126](https://gitlab.com/operator-ict/golemio/lkod/lkod-general/-/issues/126))

## [1.5.0] - 2024-07-08

### Fixed

-   Fix behavior of roles so that `admin` can only manage organizations they belong to, and `user` can manage only their own datasets

### Added

-   Superadmin role
-   User & organization management

## [1.4.5] - 2024-05-31

### Fixed

-   fix public endpoints to show datasets without distribution
-   fix public dataset detail endpoint to show organization

## [1.4.4] - 2024-05-31

### Fixed

-   fix public endpoints to show only published datasets

## [1.4.3] - 2024-05-22

### Added

-   possibility to replace SPARQL endpoint with DB calls ([lkod-general#106](https://gitlab.com/operator-ict/golemio/lkod/lkod-general/-/issues/106))

## [1.4.2] - 2024-01-21

### Changed

-   Improve README.md

### Removed

-   Apiary links ([general#498](https://gitlab.com/operator-ict/golemio/code/general/-/issues/498))

## [1.4.1] - 2023-12-13

### Added

-   Cron for sending updated datasets to sparql ([lkod-general#114](https://gitlab.com/operator-ict/golemio/lkod/lkod-general/-/issues/114))

### Changed

-   ability to add ArcGIS feed to organization ([lkod-general#96](https://gitlab.com/operator-ict/golemio/lkod/lkod-general/-/issues/96))

## [1.4.0] - 2023-11-16

### Added

-   Add limit, offset and X-Total-Count to /datasets endpoint ([lkod-general#115](https://gitlab.com/operator-ict/golemio/lkod/lkod-general/-/issues/115))

### Changed

-   Refactor static files serving

## [1.3.1] - 2023-10-09

### Changed

-   CI/CD - Update Redis services to v7.2.1
-   flag dataset as readonly if the organization has an associated ArcGIS feed

### Added

-   ability to add ArcGIS feed to organization ([lkod-general#96](https://gitlab.com/operator-ict/golemio/lkod/lkod-general/-/issues/96))
-   `DB_PORT` env variable for configuration of the PostgreSQL database connection port

### Changed

-   flag dataset as readonly if the organization has an associated ArcGIS feed
-   Replace identificationNumber (IČO) with slug as a unique organization identifier ([lkod-backend#32](https://gitlab.com/operator-ict/golemio/lkod/lkod-backend/-/issues/32)). Note that SPARQL data for datasets and organizations needs to be reloaded (e.g. using [this](https://gitlab.com/operator-ict/golemio/lkod/lkod-general/-/tree/master/reload-sparql-data)).

### Fixed

-   Fix tests being dependent on a specific key pair
-   Fix tests being dependent on other tests

## [1.2.4] - 2023-09-01

### Changed

-   NodeJS 18.17.0
-   Change of parametrization of the /lod/catalog endpoint ([lkod-general#100](https://gitlab.com/operator-ict/golemio/lkod/lkod-general/-/issues/100))

## [1.2.3] - 2023-07-13

### Fixed

-   Update dataset json-schema

## [1.2.2] - 2023-06-21

### Fixed

-   Error when switching distribution type ([lkod-backend#29](https://gitlab.com/operator-ict/golemio/lkod/lkod-backend/-/issues/29))

## [1.2.1] - 2023-05-10

### Changed

-   upgrade node to v18
-   migrate to npm

## [1.2.0] - 2023-04-24

### Fixed

-   update documentation
-   parametrize lod catalog meta

### Added

-   add new columns logo and description to organization
-   upload organizations static data to sparql directly from database (not config file)

## [1.1.3] - 2023-03-16

### Added

-   csv download of datasets ([lkod-general#70](https://gitlab.com/operator-ict/golemio/lkod/lkod-general/-/issues/70))
-   extend search by keywords ([lkod-general#79](https://gitlab.com/operator-ict/golemio/lkod/lkod-general/-/issues/79))

## [1.1.2] - 2023-02-27

### Fixed

-   save dataset files with slugify name ([#30](https://gitlab.com/operator-ict/golemio/lkod/lkod-backend/-/issues/30))

## [1.1.1] - 2023-02-02

### Added

-   new view v_lkod_metadata (https://gitlab.com/operator-ict/golemio/lkod/lkod-general/-/issues/36)

## [1.1.0] - 2023-01-19

### Added

-   dataset validation ([lkod-general#34](https://gitlab.com/operator-ict/golemio/lkod/lkod-general/-/issues/34))

## [1.0.2] - 2022-12-20

### Added

-   possibility to return to original page on FormData Controller and Router ([#40](https://gitlab.com/operator-ict/golemio/lkod/lkod-general/-/issues/40))

## [1.0.1] - 2022-12-16

### Changed

-   Add publisher prefix to SparqlLookupHelper

## [1.0.0] - 2022-12-14

### Added

-   Add env STORAGE_FILE_MAX_SIZE_IN_KB and increase default limit to 20MB ([#19](https://gitlab.com/operator-ict/golemio/lkod/lkod-backend/-/issues/19))
-   Add filter by status to /dataset endpoint ([#16](https://gitlab.com/operator-ict/golemio/lkod/lkod-backend/-/issues/16))
-   Add deleting pending dataset in status `created` ([#14](https://gitlab.com/operator-ict/golemio/lkod/lkod-backend/-/issues/14))
-   Add change password, forgot password, reset password and mailer ([#20](https://gitlab.com/operator-ict/golemio/lkod/lkod-backend/-/issues/20))
-   Add `hasDefaultPassword` to user and /auth/login endpoint ([general#35](https://gitlab.com/operator-ict/golemio/lkod/lkod-general/-/issues/35))
-   Add filter by publisherIri, keywords, formatIris, themeiris to /dataset endpoint ([#17](https://gitlab.com/operator-ict/golemio/lkod/lkod-backend/-/issues/17))
-   Add lookup endpoints /publishers, /keywords, /formats, /themes /to /dataset endpoint ([#17](https://gitlab.com/operator-ict/golemio/lkod/lkod-backend/-/issues/17))
-   Add column `slug` to organization ([#22](https://gitlab.com/operator-ict/golemio/lkod/lkod-backend/-/issues/22))
-   Allow multiple status values on /dataset endpoint ([#23](https://gitlab.com/operator-ict/golemio/lkod/lkod-backend/-/issues/23))
-   Add db logging for dataset files
-   Add search filter to /dataset endpoint ([#5](https://gitlab.com/operator-ict/golemio/lkod/lkod-frontend/-/issues/5))
-   Add lookup endpoint /statuses ([#28](https://gitlab.com/operator-ict/golemio/lkod/lkod-backend/-/issues/28))

### Changed

-   Return link to upladed file in endpoint `POST /datasets/{datasetId}/files` ([#13](https://gitlab.com/operator-ict/golemio/lkod/lkod-backend/-/issues/13))
-   Replace tslint by eslint.
-   Move /login and /logout to /auth/login and /auth/logout ([#20](https://gitlab.com/operator-ict/golemio/lkod/lkod-backend/-/issues/20))
-   Add subfolder to path for uploading files ([#22](https://gitlab.com/operator-ict/golemio/lkod/lkod-backend/-/issues/22))
-   Add column `slug` to organization ([#22](https://gitlab.com/operator-ict/golemio/lkod/lkod-backend/-/issues/22))
-   Grant file access permissions for entire organization ([#24](https://gitlab.com/operator-ict/golemio/lkod/lkod-backend/-/issues/24))
-   Set datasets ordering by name ([#28](https://gitlab.com/operator-ict/golemio/lkod/lkod-backend/-/issues/28))

### Fixed

-   Fix /api-docs endpoint (yaml parsing).

